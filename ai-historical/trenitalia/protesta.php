<html>
<head>
  <title>Alcune delle cose che puoi fare...</title>
  <link rel="stylesheet" type="text/css" href="ti.css"/>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
  <meta http-equiv="Content-Language" content="it"/>
  <meta name="description" content="Citazione della compagnia ferroviaria italiana contro l'associazione Autistici Inventati. Raccolta atti giudiziari del processo per la chiusura di un sito di satira. Case Study."/>
  <meta name="keywords" content="Clienti Insoddisfatti, Sindacato ferrotranvieri, Censura contro la satira, diritto alla satira. sito censurato, guerra in iraq, trasporto armi, trasporto materiale bellico, liberta' di espressione, boicottare, pendolari, orario dei treni"/>
  <meta name="revisit" content="1 day"/>
  <meta name="robots" content="index,follow"/>
</head>


<body>

 <div id="banner">
  <img align="right" clear="none" src="materiali/banner_ai_half.png" alt="banner autistici/inventati sotto attacco"/>
  <h3>E' arrivato un vagone carico di...</h3>
  <small>Documentazione sulla citazione da parte di Trenitalia contro Autistici/Inventati</small>
 </div>



 <div id="sidebar">

  [<a href="index.php">italian version</a>] -  [<a href="index.en.php">english version</a>]<br/>

  <hr/>

  <ul>
   <li><a href="news.php">Aggiornamenti</a></li>
   <li><a href="mirrors.php">Mirrors</a></li>
   <li><a href="legaldocs.php">Documentazione legale</a></li>
   <li><a href="rassegna.php">Rassegna stampa/web</a></li>
   <li>Comunicati:
	<ul>
	<li><a href="comunicato.html">Primo Comunicato A/I</a></li>
	<li><a href="comunicato_ricorso.php">Secondo Comunicato A/I</a></li>
	<li><a href="comunicato_zenmai23.php">Comunicato Zenmai23</a></li>
	</ul></li>
<li><a href="link.php">Link su Trenitalia</a></li>
  </ul>

  <hr/>

  <ul>
   <li><a href="protesta.php">Cosa posso fare?</a></li>
	<ol>
	<li><a href="protesta.php#mail">Manda una mail a Trenitalia</a></li>
	<li><a href="protesta.php#banner">Pubblica il banner sul tuo sito</a></li>
	<li>Firma il <a href="http://www.autistici.org/ai/trenitalia/guest/guestbook.php">Guestbook</a></li>
	<li><a href="protesta.php#signature">Usa questa signature!</a></li>
	<li><a href="protesta.php#stampa">Stampa e diffondi il materiale</a></li>
	<li>Fai una <a href="http://autistici.org/it/donate.html">donazione!</a></li>
	</ol>
  </ul>

  <hr/>

  <a href="http://www.inventati.org">Home Page del progetto Autistici/Inventati</a><br/>

 </div>


 <div id="main">



<h1>
Alcune delle cose che puoi fare...
</h1>

</p>
<small>
[<a href="protesta.en.php">english version</a>]
</small>
<hr>

<p>
<h4>
Manda una mail di protesta a Trenitalia!
<a name="#mail"></a>
</h4>
Manda una, due, o un numero a tuo piacimento di e-mail 
alla direzione regionale di Trenitalia
che piu' ti sfizia tra quelle elencate su
</p>

<p>
<a href="http://www.regionale.trenitalia.it/direzioni_regionali.html">
http://www.regionale.trenitalia.it/direzioni_regionali.html
</a>
</p>
<p>
Copia il testo qui di seguito o, meglio,
 inventatene uno pieno di frasi ispirate, 
anche il subject e' affidato alla tua inventiva.
</p>


<p>
[testo da copia/incollare]
</p>
<p>
<strong>OGGETTO: Reclamo contro...
</strong>
<br>
All'Attenzione della Direzione Regionale Trenitalia
<br/>
<br/>
<i>

La liberta' non viaggia sui binari di Trenitalia
<br>
Con questa mail intendiamo protestare contro la rimozione
forzata del sito
<br>
http://autistici.org/zenmai23/trenitalia
<br>
e contro l'azione legale intrapresa da Trenitalia s.p.a.
ai danni dell'associazione Investici, che ospitava lo
spazio sul proprio server.
</i>
L'ennesimo cliente insoddisfatto.
<br/>
[fine testo]
</p>

<hr>

<p>
<a name="banner"></a>
<h4>
Banner sul tuo sito
</h4>

Se hai un sito apponi uno dei banner scaricabili da 
<ul>
<li>
<a href="http://autistici.org/ai/trenitalia/materiali/trenitalia.gif">http://autistici.org/ai/trenitalia/materiali/trenitalia.gif</a> (468x60 animato)
</li>
<li>
<a href="http://autistici.org/ai/trenitalia/materiali/banner_ai.png">http://autistici.org/ai/trenitalia/materiali/banner_ai.png</a> (468x60)
</li>
<li>
<a href="http://autistici.org/ai/trenitalia/materiali/banner_ai_half.png">http://autistici.org/ai/trenitalia/materiali/banner_ai_half.png</a> (234x60)
</li>
<li>
<a href="http://autistici.org/ai/trenitalia/materiali/banner_ai_square.png">http://autistici.org/ai/trenitalia/materiali/banner_ai_square.png</a> (160x160)
</li>
<li>
<a href="http://autistici.org/ai/trenitalia/materiali/banner_ai_button.png">http://autistici.org/ai/trenitalia/materiali/banner_ai_button.png</a> (88x31)
</li>
</ul>
<p>
Ed aggiungi queste poche righe di html, cambiando nome_banner
a seconda del banner che hai scelto. 
<pre>
&lt;!-- Inizio Banner Autistici vs Trenitalia --&gt;
&lt;a
href="http://autistici.org/ai/trenitalia/" target="_blank"&gt;&lt;img
src="nome_banner" alt="E' arrivato un vagone carico di ...
Sostegno ad AUTISTICI INVENTATI attaccati da Trenitalia"</a>
&lt;!-- Fine Banner Autistici vs Trenitalia --&gt;
</pre>
</p>

<hr>

<p>
<h4>Firma il Guestbook</h4>

<a name="#guest"></a>
Segnala sul nostro <a href="http://autistici.org/ai/trenitalia/guest">guestbook</a> che aderisci
alla campagna in sostegno ad Autistici/Inventati

<hr>

<p>
<a name="#stampa"></a>
<h4>
Stampa e diffondi!
</h4>

Stampa il volantino prelevabile da
<a href="http://autistici.org/ai/trenitalia/materiali/trenitalia-01092004.pdf">
http://autistici.org/ai/trenitalia/materiali/trenitalia-01092004.pdf
</a>
e diffondilo il piu' possibile. Il volantino verra' aggiornato periodicamente con gli sviluppi della situazione giudiziaria. Quindi ogni tanto butta un ochhio su queste pagine.
<!--
<p>
Abbiamo anche un bellissimo flyer sulla vicenda: fotocopia e diffondi.
<br>
<a href="http://autistici.org/ai/trenitalia/flyer.jpg">
http://autistici.org/ai/trenitalia/flyer.jpg</a>
</p>
-->

<hr>

<p>
<a name="signature"></a>
<h4>
Signature
</h4>

Aggiungi questo robo in ascii alla signature delle tue email
</p>
</small>

<pre>
-.          .-----.          .-----.        ----------         .--.
#|   o======|#####|   o======|#####|          |#  #|     .--.  |##|
---.   .------------.   .------------.   |----+####+-----+##+--+##|
o)o ) ( (o)o(o)o(o)o ) ( (o)o(o)o(o)o )  |###aAAb###aAAb###aAAb###|
&lt;http://www.autistici.org/ai/trenitalia&gt;-|--(doob)-(doob)-(doob)--#
 o ))  ( o ))   ( o ))  ( o ))   ( o ))      `uu'   `uu'   `uu'
</pre>

<hr>
<a name="donazioni"></a>
<h4>Donazioni</h4>
<p>
<small>
Come sempre un supporto finanziario e' sempre gradito e, ora
in particolare, necessario.
<br/>
Ricordiamo a tutti che il nostro progetto vive ESCLUSIVAMENTE
di sottoscrizioni volontarie!
</small>
</p>
<a href="http://autistici.org/it/donate.html">Sottoscrivi ORA!</a>
<br/>



  <br/><br/><br/><br/>
  <hr/>
  <p align="center">
   <small><em>
    Pagina modificata il 
    13/01/2007   </em></small>
  </p>


 </div>

</body>
</html>
