var TxInicial = 12;
var textoHtml;
var numCol = 1;
function zoom(Factor)
{
	tx = document.getElementById("TextoNoticia");
	TxInicial = TxInicial + Factor;
	tx.style.fontSize = TxInicial;
	if (numCol > 1)
	{
		tb = document.getElementById("tablaColumnas");
		tb.style.fontSize = TxInicial;
	}
}
function CogerTexto()
{
	texto = document.getElementById("TextoNoticia");
	textoHtml = texto.innerHTML;
	zoom(1);
	
}
function columnas() {
	var ini, fin
	var StrNumCol = prompt("numero de columnas", 1);
	numCol = parseInt(StrNumCol);
	if (!isNaN(numCol) && numCol>1){

	texto = document.getElementById("TextoNoticia");
	 texto.innerHTML = "";
	if (document.all)
	{ 
		var newTable = document.createElement("<table id='tablaColumnas' class='noticia' cellpadding='10' border='0' width='95%'></table>")
		var newRow = newTable.insertRow();
	}else{
	 	var newTable = document.createElement("table");
	 	newTable.setAttribute("border", "0");
		newTable.setAttribute("width", "95%");
		newTable.setAttribute("cellpadding", "10");
		newTable.setAttribute("cellspacing", "20");
		newTable.setAttribute("class", "noticia");
		newTable.setAttribute("id", "tablaColumnas");
		var newRow = document.createElement("tr"); 
		newTable.appendChild(newRow);
	}
	 
	fin = -1;
	var ancho = Math.floor(100 / numCol) + "%";
	for(i=1; i<=numCol; i++) {
	 	if (document.all)
		{
			var newCell = newRow.insertCell();
			newCell.width =  ancho;
			newCell.vAlign = "top";
		}else{
			var newCell = document.createElement("td"); 
			newRow.appendChild(newCell);			
			newCell.setAttribute("width", ancho);
			newCell.setAttribute("vAlign", "top");
			var celdaVacia = document.createElement("td"); 
		}

		// DIVIDIR TEXTO
		ini = fin + 1;
		fin += Math.floor(textoHtml.length / numCol);
		if (fin  > textoHtml.length || i == numCol)
		   fin = textoHtml.length;
		// NO CORTAR TAGS
		var j;
		for(j=ini+fin-1; j>=ini && textoHtml.charAt(j) != ">" && textoHtml.charAt(j) != "<"; j--);
		if (textoHtml.charAt(j) == "<")
		   for(; fin<textoHtml.length && textoHtml.charAt(fin) != ">"; fin++);
		// ACABAR EN BLANCO O PUNTO
		if (textoHtml.charAt(fin) != " " && textoHtml.charAt(fin) != ".")
		   for(; fin<textoHtml.length && textoHtml.charAt(fin) != " " && textoHtml.charAt(fin) != "."; fin++);

		newCell.innerHTML = textoHtml.substring(ini, fin);
	 }
	 texto.appendChild(newTable)

   }
   else
   {
	   texto.innerHTML = textoHtml;
	   numCol = 1;
   }
}