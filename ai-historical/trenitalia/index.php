<html>
<head>
  <title>E' arrivato un vagone carico di...</title>
  <link rel="stylesheet" type="text/css" href="ti.css"/>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
  <meta http-equiv="Content-Language" content="it"/>
  <meta name="description" content="Citazione della compagnia ferroviaria italiana contro l'associazione Autistici Inventati. Raccolta atti giudiziari del processo per la chiusura di un sito di satira. Case Study."/>
  <meta name="keywords" content="Clienti Insoddisfatti, Sindacato ferrotranvieri, Censura contro la satira, diritto alla satira. sito censurato, guerra in iraq, trasporto armi, trasporto materiale bellico, liberta' di espressione, boicottare, pendolari, orario dei treni"/>
  <meta name="revisit" content="1 day"/>
  <meta name="robots" content="index,follow"/>
</head>


<body>

 <div id="banner">
  <img align="right" clear="none" src="materiali/banner_ai_half.png" alt="banner autistici/inventati sotto attacco"/>
  <h3>E' arrivato un vagone carico di...</h3>
  <small>Documentazione sulla citazione da parte di Trenitalia contro Autistici/Inventati</small>
 </div>



 <div id="sidebar">

  [<a href="index.php">italian version</a>] -  [<a href="index.en.php">english version</a>]<br/>

  <hr/>

  <ul>
   <li><a href="news.php">Aggiornamenti</a></li>
   <li><a href="mirrors.php">Mirrors</a></li>
   <li><a href="legaldocs.php">Documentazione legale</a></li>
   <li><a href="rassegna.php">Rassegna stampa/web</a></li>
   <li>Comunicati:
	<ul>
	<li><a href="comunicato.html">Primo Comunicato A/I</a></li>
	<li><a href="comunicato_ricorso.php">Secondo Comunicato A/I</a></li>
	<li><a href="comunicato_zenmai23.php">Comunicato Zenmai23</a></li>
	</ul></li>
<li><a href="link.php">Link su Trenitalia</a></li>
  </ul>

  <hr/>

  <ul>
   <li><a href="protesta.php">Cosa posso fare?</a></li>
	<ol>
	<li><a href="protesta.php#mail">Manda una mail a Trenitalia</a></li>
	<li><a href="protesta.php#banner">Pubblica il banner sul tuo sito</a></li>
	<li>Firma il <a href="http://www.autistici.org/ai/trenitalia/guest/guestbook.php">Guestbook</a></li>
	<li><a href="protesta.php#signature">Usa questa signature!</a></li>
	<li><a href="protesta.php#stampa">Stampa e diffondi il materiale</a></li>
	<li>Fai una <a href="http://autistici.org/it/donate.html">donazione!</a></li>
	</ol>
  </ul>

  <hr/>

  <a href="http://www.inventati.org">Home Page del progetto Autistici/Inventati</a><br/>

 </div>


 <div id="main">




<h2>
Censura ad alta velocita'...
</h2>

<ul style="font-size:120%;">
<li>
  <a href="news.php">Aggiornamenti</a>
</li>
<li>
  <small><a href="materiali/trenitalia-01092004.pdf">Stampa e Diffondi</a></small>
</li>
</ul>
<br>
<br>
<h2><b>L'ultima notizia</b></h2>
<p>
Oggi, 14 Settembre 2004, abbiamo ricevuto copia del dispositivo emesso dai
giudici cui era stato sottoposto il nostro ricorso contro il
provvedimento d'urgenza.<br>
Il collegio giudicante ha accettato il nostro reclamo, cosi' abbiamo
riacquistato il diritto di pubblicare liberamente la pagina web:
<br>
<a href="http://www.autistici.org/zenmai23/trenitalia"
target="_blank">http://www.autistici.org/zenmai23/trenitalia</a><br>
Le spese legali saranno pagate da Trenitalia :-)
</p>
<br>
<br>
<br>
<h2>Nei giorni precedenti</h2>


<p>
Intorno a meta' luglio 2004 abbiamo ricevuto da <a href="link.php">Trenitalia</a> una citazione. Prima di
andare in vacanza gli avvocati della compagnia ferroviaria di bandiera ci hanno
comunicato che il sito http://autistici.org/zenmai23/trenitalia
offendeva orribilmente l'azienda che rappresentano e per questo con
una citazione presso il tribunale di Milano chiedevano
</p>
<p>
<ul>
<li>di rimuovere immediatamente il sito</li>
<li>di pubblicare a spese nostre su due quotidiani nazionali l'avvenuta
rimozione</li>
<li>di non usare piu' riferimenti a trenitalia nei metatags dei siti su
Autistici/Inventati</li>
</ul>
</p>

<p>
Oltre a questi provvedimenti di urgenza, domandavano un risarcimento per
i danni morali e materiali.
</p>

<blockquote>
<a href="comunicato.html">
Leggi il comunicato in Italiano
</a>
</blockquote>

<p>
Dopo una serie di udienze per decidere in merito all'urgenza del provvedimento,
ci viene notificato l'obbligo di eseguire immediatemente quanto richiesto
da Trenitalia. <b>SIAMO STATI COSTRETTI A RIMUOVERE IL SITO</b>.
</p>

<blockquote>
<a href="sentenza-03-08-2004.html">
Leggi estratto dalla sentenza
</a>
</blockquote>

<p>
Il 7 settembre 2004 si e' tenuta l'udienza relativa al nostro reclamo
sulla rimozione del sito. Trenitalia ha chiesto di "ESTENDERE 
L'INIBITORIA PRONUNZIATA AD OGNI ALTRA PAGINA DI ANALOGO CONTENUTO": 
significa in pratica eliminare anche la lista dei mirror sorti 
spontaneamente in rete pubblicata qui di seguito.
</p>

<p>
Si tratta di un passaggio piuttosto delicato, anche dal punto di
vista giuridico: il principio secondo il quale mantenere un link
ad una risorsa costituisce un'attivita' di per se' illegale,
trasforma paradossalmente un motore di ricerca in un crogiuolo
di illegalita'. L'immagine che i legali di Trenitalia hanno
di Internet e del suo funzionamento e' piuttosto inquietante.
</p>

<p>
Il nostro avvocato ha invece prodotto la rassegna stampa sulla
vicenda.
</p>

<p>
Entro 60 giorni i giudici si dovranno pronunciare e sapremo
se la pagina potra' tornare al proprio posto oppure no.
Sempre entro questo termine inizieranno le udienze relative
alla causa per danni vera e propria, che come si legge nel 
comunicato potrebbe diventare un grosso problema per il 
nostro server, che vive di sottoscrizioni volontarie.
</p>

<p>
Nel caso perdessimo, soltanto per adempiere all'obbligo
di pagare la pubblicazione della sentenza sui due quotidiani
scelti da Trenitalia saremmo costretti a sborsare una cifra
dell'ordine di 20.000 euro.<br>
Si parla insomma di cifre che la nostra associazione non
ha mai posseduto e che realisticamente non possiedera' mai.
</p>




  <br/><br/><br/><br/>
  <hr/>
  <p align="center">
   <small><em>
    Pagina modificata il 
    13/01/2007   </em></small>
  </p>


 </div>

</body>
</html>
