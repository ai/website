
TRIBUNALE DI MILANO - SEZ. FERIALE - DR. DE BLASI

                    Ass. INVESTICI - resistente - Avv. Pagani

                                      CONTRO

                    TRENITALIA s.p.a. - ricorrente - Avv. Lupi

                 MEMORIA AUTORIZZATA PER L'ASSOCIAZIONE INVESTICI

Nella propria memoria Trenitalia affronta in modo alquanto disinvolto una serie
di problemi molto distanti tra di loro, giungendo anche a citare in modo
inesatto delle leggi per giungere a conclusioni a dir poco paradossali.

SULLA LEGGE APPLICABILE

Controparte ritiene che nel caso di specie non vada applicato il D.L. 70/2003,
in quanto l'Associazione Investici non svolge attivit� economica; andrebbero
invece applicate le norme sulla stampa!!

L'attivit� dell'Associazione Investici, per  quanto  attiene  al  presente  caso,
consiste nel fornire gratuitamente degli spazi web a realt�  di  movimento  senza
pretendere alcuna identificazione "certa", ma una omogeneit� ed  una  sostanziale
adesione al manifesto politico dell'Associazione.

Viene effettuato un screening preventivo sui contenuti che  il  soggetto  intende
portare nello spazio concessogli; il soggetto realizza il  proprio  sito  web  in
piena automia  senza  alcuna  interferenza  da  parte  dell'Associazione;  nessun
controllo viene effettuato a priori sui contenuti del sito.

Ovviamente se l'Associazione si avvede che un sito da lei ospitato  ha  contenuti
incompatibili con i suoi scopi statutari ed i suoi contenuti prende le  opportune
misure.

Tutto  ci�  viene  offerto  gratuitamente,  con  la  semplice  richiesta  di   un
contributo economico volontario.

Tale attivit� � una di quelle individuate all'art. 2 del  DL  70/2003,  che  cita
espressamente, oltre alle "attivit�  economiche  svolte  in  linea",  i  "servizi
definiti dall'art. 1 comma 1 lett. b) L. 317/86 e successive modificazioni".

Tale norma, come modificata dal DL  427/2000  all'art.  2,  definisce  "servizio:
qualsiasi  servizio  della  societ�  dell'informazione,  vale  a  dire  qualsiasi
servizio  prestato  normalmente  dietro  retribuzione,  a   distanza,   per   via
elettronica e a richiesta individuale di un destinatario di servizi".

L'Associazione   Investici   effettua   non   dietro   retribuzione   ma   dietro
contribuzione volontaria, ci� che altri prestatori di servizi  effettuano  dietro
retribuzione; normalmente tale servizio � retribuito, eccezionalmente  (come  nel
nostro caso) � dietro contribuzione volontaria. Ci sembra  che  il  senso  comune
delle parole usate dal legislatore sia chiaro; se avesse voluto  escludere  dalla
previsione normativa i servizi non retribuiti non avrebbe  utilizzato  l'avverbio
normalmente   ma   avrebbe   scritto   "qualsiasi   servizio   prestato    dietro
retribuzione".

Una diretta conferma di questa ovvia interpretazione risiede nell'art. 2 comma  1
2� comma D.L. 70/2003, laddove  �  definito  "prestatore:  la  persona  fisica  o
giuridica   che   presta   un   servizio   della   societ�    dell'informazione".
Contrariamente a quanto scritto nella comparsa Trenitalia a pag. il soggetto  che
esercita  effettivamente  un'attivit�  economica  non   �   "prestatore",   bens�
"prestatore stabilito".

Vi sono dunque due categorie di soggetti che prestano servizi della societ�
dell'informazione: "prestatore stabilito" che esercita effettivamente
un'attivit� econimica e "prestatore", che non esercita effettivamente
un'attivit� economica.

SULLA LEGITTIMAZIONE PASSIVA

Sulle tracce di un ragionamento non perfettamente comprensibile, Trenitalia
afferma che l'Associazione Investici � autrice del sito de quo.

L'associazione Investici ribadisce la propria assunzione di responsabilit�
politica per i contenuti della pagina creata e gestita da zenmai23, ma ribadisce
con altrettanta fermezza di non aver esercitato alcun ruolo n� alcuna ingerenza
nella creazione e nella gestione del sito.

Trenitalia fa una certa confusione tra "dominio" e "sito"; l'Associazione
Investici � proprietaria del dominio e se ne assume la responsabilit� morale,
politica e giuridica. L'Associazione Investici non effettua alcuna ingerenza
nella creazione e nella gestione dei siti da lei ospitati, salvo ovviamente il
diritto di eliminare quei siti e quei contenuti che non siano in linea con la
sua policy.

Quello che Trenitalia erroneamente definisce dominio si chiama invece  url  ed  �
composto dal dominio (autistici.org) e  da  un  URI   (zenmai23/trenitalia).  URI
significa Uniform Resource Identifier; URL significa Uniform Resource Locator.

La composizione dell'url avviene dal lato server in  questa  maniera:  dominio  +
URI = URL

La DocumentRoot e' una cartella (o directory)che

ospita tutti i siti: /home/html.

Gli utenti ottengono: /home/html/utente ed un account ftp.

A questo punto il loro sito � accessibile come

http://autistici.org/utente e quindi gestiscono il  proprio  spazio  come  meglio
credono (gli utenti sono solo utenti, cos� come un utente di geocities � solo  un
utente di geocities.com. quello di tiscali un utente di tiscali.it)

I soci dell'associazione  svolgono  l'attivit�  amministrativa  del  server,  non
partecipano alla  creazione  dei  contenuti,  cos�  come  gli  amministratori  di
geocities.com e di tiscali.it non partecipano alla produzione dei  contenuti  del
medesimo.

Gli utenti tramite l'account ftp aggiornano il sito.

Nello specifico autistici.org/zenmai23/trenitalia

� una directory creata dall'utente zenmai23,

cos� come ad esempio www.geocities.com/pippo

rappresenta l'indirizzo a cui corrisponde il

sito dell'utente pippo. Se l'utente pippo

decide di creare nel proprio spazio una

cartella trenitalia con un index corretto

all'interno  otterremo  www.geocities.com/pippo/trenitalia  ma   questo   non   �
controllabile da chi offre lo spazio se non visitando  tutti  gli  spazi  web  in
questione.

La parte citata da trenitalia � in pratica

una directory creata dall'utente zenmai23

nel proprio spazio, non un dominio (termine

di per se' errato) in qualche modo registrato

dall'associazione  Investici;   il   dominio   registrato   dall'associazione   �
autistici.org.

L'associazione investici prende parte soltanto

alla creazione della prima parte dell'url

(autistici.org/zenmai23).

Quando perviene una richiesta essa viene valutata

leggendo la descrizione inviata attraverso il form

e se corrisponde ai criteri di antifascismo,

antisessismo ed antirazzismo viene creata la

directory /home/html/utente e l'account ftp

relativo.

A  questo  punto  l'Associazione  Investici  non  esercita  pi�  dei  particolari
controlli sullo spazio utente.

Nel caso specifico di zenmai23 la descrizione

inviata e pubblicata nella pagina

delle presenze  http://autistici.org/presenze.php

era relativa ad un gruppo di web designer.

L'utente zenmai23 pu� creare all'interno del proprio

spazio tutte le cartelle che desidera cos�

come l'utente pippo pu� farlo su geocities.

L'indirizzo www.autistici.org/zenmai23/trenitalia non corrisponde ad un sito, ma
ad una pagina del sito.

Il dominio � www.autistici.org; www.autistici.org/zenmai.23 � l'url e questo �
l'indirizzo fornito dall'Associazione Investici a zenmai23. Un sito � diviso in
pagine; zenmai23 (cos� come tutte le altre realt� presenti sul server) ha creato
delle pagine a cui ha dato dei nomi e in tutto ci� l'Associazione non ha avuto
alcun ruolo.

Investici � un'Associazione, regolarmente costituita (doc. 27), il cui
presidente � il sig. XXXX XXXX; del tutto peregrine sono le argomentazioni
volte a individuare una responsabilit� diretta del sig. XXXX.

Rispetto alla richiesta di Trenitalia di fornire i nomi degli autori del sito se
ne ribadisce, oltre che la tardivit�, anche l'inutilit� in questa fase. Se
Trenitalia avesse voluto servirsi di un approccio pi� morbido avrebbe potuto
comportarsi in modo diverso, meno conflittuale che quello utilizzato.

In ogni caso la policy dell'Associazione Investici sul punto � chiarissima e gi�
indicata nella memoria di Trenitalia: la resistente non conosce i nomi degli
autori e dei gestori del sito zenmai23.

Chi ottiene uno spazio sul server dispone di una password che gli permette di
intervenire autonomamente sul sito per implementarlo, modificarlo, ecc. senza
alcun contatto con l'Associazione.

Questo comportamento, contrariamente a quanto afferma Trenitalia, non viola
l'art. 70 del DL 70/2003, che impone vari obblighi al prestatore e non al
"destinatario del servizio" quale � in effetti zebmai23.

SULLA VERITA' DELLA NOTIZIA

Alla fine Trenitalia si degna di riconoscere ci� che tutti sapevano e sanno: che
le armi di morte utilizzate per la guerra all'Irak e per la successiva
occupazione militare sono stati trasportati sui suoi treni.

La ricorrente si nasconde dietro il fragile paravento costituito dall'art. 2597
c.c. ma l'argomentazione � priva di pregio.

Operare in regime di monopolio non significa che Trenitalia sia autorizzata a
concedere i propri mezzi per qualsiasi traffico illecito.

Il trasporto di armi per la guerra in Irak � illecito ai sensi della L. 9/7/1990
n. 185 e in particolare dell'art. 6, che a sua volta fa riferimento all'art. 51
della Costituzione.

Il fatto che il governo non abbia rispettato la Costituzione e la legge non
autorizza Trenitalia e qualsiasi altro soggetto a fare altrettanto!

Il fatto che la notizia sia datata non smentisce il fatto che essa sia vera!

Si ribadisce pertanto che risponde all'assoluta verit� che Trenitalia ha
trasportato armi da guerra utilizzate per una guerra illegittima, e, in quanto
tale, e se possibile, ancora pi� disumana ed ingiusta rispetto ad altri
conflitti.

Per quanto riguarda l'esimente del diritto di satira ci si riporta a quanto
argomentato nei precedenti scritti.

Per quanto riguarda le violazioni del D.L. 70/2003 che la Associazione Investici
avrebbe compiuto, si contesta che esse siano mai avvenute e comunque se ne
eccepisce l'irrilevanza rispetto ai fatti di causa.

Per quanto attiene, infine, alla pretesa di considerare il sito web
dell'Associazione Investici un "prodotto editoriale" ai sensi della legge
62/2001 essa � talmente assurda (oltre che anch'essa irrilevante) che non merita
replica; lo stesso dicasi sulle altre considerazioni giuridiche che vengono
introdotte solo con la memoria di replica (legge sulla privacy, legge sul
marchio) sulle quali si dichiara di non accettare il contraddittorio.

SULL'URGENZA

La pagina in oggetto � presente dal marzo 2003, cio� dall'epoca dell'inizio
della guerra in Irak e da allora � stata visitata da poche centinaia di persone.

Gi� questo dato temporale dovrebbe essere sufficiente per dimostrare che non vi
� alcun periculum in mora.

Per almeno 15 mesi Trenitalia non ha avuto notizia e neppure sentore
dell'esistenza della pagina e questo � un segno della scarsa se non nulla
rilevanza mediatica della pagina in questione.

A questo proposito si � osservato in giurisprudenza che "Non  pu�  ritenersi
sussistente il requisito del "periculum in mora" richiesto per  la proposizione
del procedimento d'urgenza ex art. 700   c.p.c.,   quando  per l'inattivit�  del
 ricorrente  sia decorso  un   periodo di tempo pari alla normale durata
dell'azione esperita in via  ordinaria, senza  che venga allegata la
sopravvenienza di fatti nuovi   che   abbiano   determinato un   diverso
pregiudizio   imminente ed irreparabile, nel  senso  di  pregiudizio che
minaccia di determinare  una lesione  irreversibile  alla  realizzazione del
diritto azionato." (Pretura Milano, 25 novembre 1996) in:    Orient. giur. lav.
1996,1061.

Ci sembra utile rilevare, poi, che la posizione sul punto sostenuta dalla
ricorrente � radicalmente mutata rispetto al ricorso: in tale atto Trenitalia
individuava l'urgenza nella necessit� che non si disperdessero le prove
dell'attivit� illecita.

Si ribadisce, poi, che la stessa caratteristica di "monopolista" invocata da
Trenitalia � idonea ad escludere che essa abbia potuto subire alcun danno,
perch� la sua clientela non pu� diminuire.

Del tutto inconferenti sono poi le affermazioni della ricorrente in ordine alla
ricerca effettuata sul sito "Altavista" utilizzando, ancora una volta, parole
improbabili quali "biglietteria ON-LINE Trenitalia".

Sono gi� acquisite (doc. 22-23) le pagine di google con le parole chiave
"trenitalia" e "ferrovie", vale a dire i criteri di ricerca pi� normali; in
nessuno delle due ricerche la pagina di zenmai23 � presente tra i primi cento
risultati.

IN VIA ISTRUTTORIA

Insiste per le istanze avanzate nella comparza di risposta.

In particolare si chiede che vengano sentiti testi a sommarie informazioni sulle
circostanze gi� indicate nella comparsa nonch� sulla seguente circostanza:

"Quale sia il meccanismo attraverso il quale le persone e/o le associazioni che
intendono utilizzare uno spazio web sul dominio www.autistici.org vengono in
contatto con l'Associazione Investici e quali e di che tipo siano i rapporti tra
l'Associazione e queste persone e/o associazioni in particolare con riferimento
alla gestione dei vari siti".

Indica a teste il sig. YYYY YYYY di Milano.

Si allega:

27) Atto costitutivo Ass. Investici

Milano, 31/7/2004

Avv. Gilberto Pagani




