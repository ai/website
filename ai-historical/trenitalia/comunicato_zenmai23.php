<html>
<head>
  <title>Comunicato del disciolto Zenmai23</title>
  <link rel="stylesheet" type="text/css" href="ti.css"/>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
  <meta http-equiv="Content-Language" content="it"/>
  <meta name="description" content="Citazione della compagnia ferroviaria italiana contro l'associazione Autistici Inventati. Raccolta atti giudiziari del processo per la chiusura di un sito di satira. Case Study."/>
  <meta name="keywords" content="Clienti Insoddisfatti, Sindacato ferrotranvieri, Censura contro la satira, diritto alla satira. sito censurato, guerra in iraq, trasporto armi, trasporto materiale bellico, liberta' di espressione, boicottare, pendolari, orario dei treni"/>
  <meta name="revisit" content="1 day"/>
  <meta name="robots" content="index,follow"/>
</head>


<body>

 <div id="banner">
  <img align="right" clear="none" src="materiali/banner_ai_half.png" alt="banner autistici/inventati sotto attacco"/>
  <h3>E' arrivato un vagone carico di...</h3>
  <small>Documentazione sulla citazione da parte di Trenitalia contro Autistici/Inventati</small>
 </div>



 <div id="sidebar">

  [<a href="index.php">italian version</a>] -  [<a href="index.en.php">english version</a>]<br/>

  <hr/>

  <ul>
   <li><a href="news.php">Aggiornamenti</a></li>
   <li><a href="mirrors.php">Mirrors</a></li>
   <li><a href="legaldocs.php">Documentazione legale</a></li>
   <li><a href="rassegna.php">Rassegna stampa/web</a></li>
   <li>Comunicati:
	<ul>
	<li><a href="comunicato.html">Primo Comunicato A/I</a></li>
	<li><a href="comunicato_ricorso.php">Secondo Comunicato A/I</a></li>
	<li><a href="comunicato_zenmai23.php">Comunicato Zenmai23</a></li>
	</ul></li>
<li><a href="link.php">Link su Trenitalia</a></li>
  </ul>

  <hr/>

  <ul>
   <li><a href="protesta.php">Cosa posso fare?</a></li>
	<ol>
	<li><a href="protesta.php#mail">Manda una mail a Trenitalia</a></li>
	<li><a href="protesta.php#banner">Pubblica il banner sul tuo sito</a></li>
	<li>Firma il <a href="http://www.autistici.org/ai/trenitalia/guest/guestbook.php">Guestbook</a></li>
	<li><a href="protesta.php#signature">Usa questa signature!</a></li>
	<li><a href="protesta.php#stampa">Stampa e diffondi il materiale</a></li>
	<li>Fai una <a href="http://autistici.org/it/donate.html">donazione!</a></li>
	</ol>
  </ul>

  <hr/>

  <a href="http://www.inventati.org">Home Page del progetto Autistici/Inventati</a><br/>

 </div>


 <div id="main">


<h1>
Comunicato del disciolto Zenmai23 
</h1>
[
<a href="http://thething.it/modules.php?name=News&file=article&sid=155">
http://thething.it/modules.php?name=News&file=article&sid=155
</a> ]

<p>
   "Io affermo che il plagiarismo � il metodo artistico realmente
   attuale. Il plagio � il crimine artistico contro la propriet�."
   Luther Blissett
</p>
<p>
   L'esigenza di elaborare queste righe nasce dallo stupore e
   dall'amarezza suscitati dalla notizia della citazione in giudizio per
   Autistici/Inventati a causa del nostro lavoro, e dalle prime decisioni
   dei giudici, come prevedibile solerti difensori dell'immagine della
   compagnia ferroviaria italiana.
   Premettiamo che nessuna persona legata ad Autistici/Inventati ha
   esercitato alcun ruolo n� alcuna ingerenza nella creazione e nella
   gestione del sito in questione. Nemmeno conosciamo direttamente i
   gestori del servizio di hosting, ma ovviamente rinnoviamo a loro la
   nostra solidariet�. Mai avremmo pensato di scatenare un simile
   pandemonio con un lavoro del genere.
</p>
<p>
   In quanto lavoratori della/nella rete, anni fa avevamo pensato di
   utilizzare le nostre minime esperienze per pubblicare una serie di
   lavori, per la maggior parte realizzati poi al di fuori del collettivo
   z23, di critica dell'esistente applicata alle attuali tecnologie della
   comunicazione. Ingerenze di vario genere ci hanno poi portato
   piuttosto rapidamente a separarci e a imboccare strade diverse, aree
   di movimento diverse e in alcuni casi a sbarazzarci delle macchine per
   qualcosa di pi� immediato.
   Il detournement del sito di trenitalia � stato elaborato in una notte,
   in solidariet� alla campagna contro i treni della morte lanciata da
   alcune anime del movimento italiano nella primavera del 2003. E' un
   esercizio classico di plagiarismo, in cui sono state fatte affiorare
   verit� evidentemente scomode all'azienda Trenitalia, i cui avvocati
   immaginiamo saranno impegnati nel dimostrare che quei vagoni pieni di
   bombe per la popolazione irachena, no, non possono essere chiamati
   treni della morte, ma sono solo normali convogli commerciali, che
   trasportano merce in posti lontani per conto di un cliente.
   Sono ipocrisie palesi, le medesime che abbiamo sentito in questi anni
   sulle guerre imperialiste, guerre illegali perfino per le ridicole
   leggi "internazionali" cui dovrebbero sottostare Italia, Stati Uniti e
   compari guerrafondai, guerre illegali in cui Trenitalia, ribadiamo, ha
   avuto il ruolo di vettore di morte, corresponsabile con lo stato
   italiano che ora giudica colpevole chi ha permesso la pubblicazione di
   una satira, mettendone in pericolo la stessa esistenza.
</p>
<p>
   Il consiglio di amministrazione di Trenitalia, e a quanto pare
   similmente lo stato italiano, attraverso i suoi cani da guardia
   togati, intende la rete unicamente come luogo del profitto, spazio
   entro il quale difendere a colpi di citazioni la propria (misera,
   sempre pi� misera) reputazione e recintare perfino le parole,
   pretendendo di farle proprie, e di vietare che ai motori di ricerca si
   possano segnalare quelle che rischierebbero far emergere un briciolo
   di verit� in pi� di quella fornita dai mezzi di comunicazione,
   controllati da chi le guerre decide sempre di farle.
</p>
<p>
   Noi, che volenti o nolenti nella rete ci giriamo da quando � nata o
   poco dopo, sogniamo ancora che possa essere uno spazio del libero
   comunicare, e continueremo a farlo finch� esisteranno realt� come
   quella che ci ha ospitato, tessendo le nostre controreti, cercandoci
   spazi al di fuori delle mappe del controllo.
</p>
<p>
   Il sospetto, fortissimo, � che si sia approfittato di questo minuscolo
   pretesto per cercare di mettere a tacere una realt� di movimento che
   lascia spazio alla libert� di espressione, quella stessa libert�
   tutelata a parole dalle costituzioni di tutti i paesi occidentali, ma
   nei fatti, ce ne accorgiamo ogni giorno, inesistente.
   Se ne sono accorti certamente i lavoratori licenziati in tronco da
   Trenitalia per aver risposto alle domande di un reporter della Rai
   sulle condizioni di lavoro dei macchinisti, ce ne accorgiamo, nel
   nostro piccolo e senza vivere un dramma comparabile al loro, ma
   ugualmente angosciati, noi, ora, in una fresca notte di fine estate 2004.
</p>
<p>
   bune,
   gi�,
   karl.
</p>


  <br/><br/><br/><br/>
  <hr/>
  <p align="center">
   <small><em>
    Pagina modificata il 
    13/01/2007   </em></small>
  </p>


 </div>

</body>
</html>
