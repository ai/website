FROM docker.io/library/node:current-bullseye AS assets
ADD . /src
WORKDIR /src
RUN npm install && env NODE_OPTIONS=--openssl-legacy-provider ./node_modules/.bin/webpack

# Debian bullseye can't build gostatic due to its strict dependency
# on Go >1.17, so we build it separately.
FROM docker.io/library/golang:1.21 AS gobuild
RUN go install github.com/piranha/gostatic@latest
RUN go install git.autistici.org/ai/webtools/cmd/jsonsubst@latest
RUN go install git.autistici.org/ai/webtools/cmd/sitesearch@latest

FROM docker.io/library/debian:bookworm-slim AS build
ADD . /src
WORKDIR /src
COPY --from=gobuild /go/bin/gostatic /usr/bin/gostatic
COPY --from=gobuild /go/bin/sitesearch /usr/bin/sitesearch
COPY --from=gobuild /go/bin/jsonsubst /usr/bin/jsonsubst
COPY --from=assets /src/assets/templates/ /src/assets/templates/
RUN ./scripts/lint.sh && ./scripts/update.sh

FROM docker.io/library/debian:bookworm-slim AS precompress
RUN apt-get -q update && env DEBIAN_FRONTEND=noninteractive apt-get -qy install brotli
COPY --from=build /src/public /var/www/autistici.org
COPY --from=assets /src/assets/static/ /var/www/autistici.org/static/
COPY static/ /var/www/autistici.org/static/
COPY scripts/precompress.sh /precompress.sh
RUN /precompress.sh /var/www/autistici.org

FROM registry.git.autistici.org/ai3/docker/apache2-base:bookworm
COPY --from=gobuild /go/bin/sitesearch /usr/sbin/sitesearch
COPY --from=build /src/index /var/lib/sitesearch/index
COPY --from=assets /src/assets/templates/ /var/lib/sitesearch/templates/
COPY --from=precompress /var/www/autistici.org/ /var/www/autistici.org/
COPY docker/conf/ /etc/
COPY docker/build.sh /tmp/build.sh

RUN /tmp/build.sh && rm /tmp/build.sh

# For testing purposes (8080 is the default port of apache2-base).
EXPOSE 8080/tcp

