title: Chiedi un servizio
----

# Chiedi un servizio

Vuoi un'email, una mailing list, un sito con A/I?

Autistici/Inventati è un collettivo che fa propri e promuove
l'**anticapitalismo**, l'**antirazzismo**, l'**antifascismo**, l'**antisessismo** e l'**antimilitarismo**, oltre al **rifiuto
dell'autoritarismo e del principio di delega**.
Il lavoro che facciamo è tutto volontario. Se lo facciamo, è perché crediamo fermamente in questi principi, e le persone a cui vogliamo
offrire i nostri servizi sono quelle che li condividono.
Per questo, quando ci spiegherai cosa ti spinge qui nel form di richiesta dei servizi, raccontaci dei motivi per cui condividi i nostri principi e di chi sei. 
Non occorre scendere nei dettagli della tua vita personale, però dacci un quadro di perché dovremmo offrire un servizio proprio a te.

Se vuoi sapere di più su di noi, leggi il nostro [manifesto](/who/manifesto), la nostra [policy](/who/policy) e la [privacy policy](/who/privacy-policy).

Ogni richiesta sarà letta da una persona in carne e ossa e non da un robot. Apprezziamo quindi ogni informazione che vorrai condividere con
noi, e speriamo di poter instaurare un rapporto di fiducia, a cominciare dal fatto che anche questa corrispondenza verrà distrutta.
La fiducia però è bidirezionale: così come crediamo a quel che dirai senza richiedere i tuoi dati personali, se vedremo che violi i nostri
principi pubblicamente usando i nostri servizi, non esiteremo a rimuovere il tuo account senza nessun preavviso. Se decidi di usare i nostri
servizi, accertati che questo sia il posto giusto per te.

Speriamo che tu faccia un uso consapevole della tecnologia e dei mezzi di comunicazione che hai a disposizione, e che a questo si affianchi
la rivendicazione politica, in real life, della tua libertà di espressione e di comunicazione.

Infine ricorda che **i nostri servizi sono gratuiti, ma comportano dei costi per noi**.
Dal momento che per tenere in piedi questi server non basta una semplice manutenzione quotidiana (che viene effettuata su base volontaria e
senza nessun compenso economico), apprezziamo molto tutti i contributi volontari per sostenere le spese.

[Scopri come fare una donazione ad Autistici/Inventati](/donate "donate").

**[Chiedi un servizio](https://services.autistici.org/)**
