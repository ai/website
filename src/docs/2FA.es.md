title: Autenticación de dos factores
----

** atención, esta página está obsoleta, la versión en inglés está más actualizada. **

Protege tu cuenta con la autenticación de dos factores
======================================================

La autenticación de dos factores (llamada 2FA) es un mecanismo que
incrementa el nivel de seguridad de tu cuenta al impedir el acceso a tu
panel de usuario y tu webmail a quien pueda haberte robado la contraseña
de tu cuenta.
 La idea es muy simple: una vez activada, necesitarás *algo que
conoces* (tu contraseña) y *algo que tienes* (suele ser tu
smartphone) para acceder a tu panel de usuario y tu webmail.
 Esto significa que una persona malintencionada encontrarás mas difícil
acceder a tu cuenta: si lograse robarte la contraseña, todavía deberían
robarte también tu móvil para poder acceder.

Como activar la autenticación de dos factores
---------------------------------------------

Si quieres activar la autenticación de dos factores, tienes que seguir
estos pasos:

1.  Accede a tu [panel de usuario](https://accounts.autistici.org).
2.  Selecciona tu nombre de usuario en la esquina arriba a la derecha.
3.  Se abrirá un menú desplegable. Selecciona "Enable Two-Factor Auth
    (OTP)".
     ![](/static/img/man_2fa/2FA-1.png "enable 2FA")
4.  Sigues las instrucciones que aparecerán en seguida (tendrás que
    instalar una aplicación en tu móvil, impostar las pregunta de
    emergencia si no los has hecho todavía, y al final presionar
    "Enable").
     ![](/static/img/man_2fa/2FA-2.png "how to enable 2FA")
5.  De ese momento en adelante, cuando tendrás que entrar en el panel de
    usuario, después de haber insertado nombre y contraseña un segundo
    formulario te pedirá por un código "OTP": tendrás que meter el
    código que te proporciona la aplicación del móvil, al seleccionar
    ahí tu nombre de usuario.
     ![](/static/img/man_2fa/2FA-3.png "OTP form")

Como usar clientes de correo y jabber con 2FA
---------------------------------------------

La 2FA es solo para el web. Para todos los otros usos, tendrás
que generar *contraseñas especificas para aplicación*. Para clientes
de correo como Thunderbird y de Jabber/XMPP (Pidgin, Jitsi), necesitarás
una contraseña especifica para cada aplicación, que solo se puede usar
para un cliente concreto en un dispositivo concreto.
 Para generar una contraseña especifica para aplicación, hay que seguir
estos pasos:

1.  Selecciona el menú de configuraciones (el icono de engranaje al lado
    del botón de "Webmail").
2.  Selecciona "Manage App-Specific Passwords".
     ![](/static/img/man_2fa/2FA-4.png "app-specific passwords")
3.  En el menú "Create new credentials", elige entre "email" y "jabber",
    y añade un comentario para identificar a tu nueva contraseña para
    aplicación (por ejemplo, puedes llamarla Thunderbird-pc si será para
    Thunderbird en tu PC) y dale al "Create".
     ![](/static/img/man_2fa/2FA-5.png "create app-specific passwords")
4.  Aparecerá una pagina con tu nueva contraseña.
     ![](/static/img/man_2fa/2FA-6.png "new app-specific password")
5.  Cambia la contraseña en tu aplicación u en tu gestor de contraseñas
    ahora mismo (recuérdate que para clientes de correo hay que cambiar
    al mismo tiempo la contraseña de entrada que de salida de correos).\
     Atención: al cerrar la pagina web, no podrás volver a ver la
    contraseña, así que si no la apuntas o la metes en tu aplicación,
    tendrás que cancelarla y volver a empezar!

Algunas cosas mas -- lee, es importante! :)
-------------------------------------------

Una vez que has activado la autenticación de dos factores, tu punto
débil será la pregunta de emergencia: si alguien quiere acceder a tu
correo y no puede porqué no tiene tu teléfono, puede que intenten
resetear tu contraseña usando la pregunta de emergencia.
 Por lo tanto, en muy importante que la pregunta de emergencia sea
conectada con algo que solo y solamente tu sabes. Algunos aconsejan de
usar pregunta y respuesta inventadas.
 En definitiva, tienes que recuérdate que:

1.  si pierdes tu móvil, la única manera de acceder a tu cuenta será
    contestando la pregunta de emergencia;
2.  si la respuesta a tu pregunta se puede encontrar en las redes
    sociales, tu cuenta no será segura aunque hayas habilitado la 2FA.

Si no tienes uno smartphone y no quieres tener uno, hay una
solución. Es un poco complicada, y necesitarás comprar una *YubiKey*.
Encontrarás las instrucciones sobre como configurar tu Yubikey en la
pagina de habilitación de la 2FA en tu panel.
 Si quieres saber más, empieza por [esta pagina de
YubiKey](https://www.yubico.com/products/yubikey-hardware/yubikey4/).
