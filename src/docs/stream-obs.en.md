title: Streaming with OBS Studio
----

Streaming with OBS Studio
=========================

To stream on [live.autistici.org](https://live.autistici.org), we recommend you use [OBS Studio](https://obsproject.com) following these instructions for connecting to the streaming server:

1. Click "File" -> "Settings".

2. Select the "Stream" tab and enter the following settings:

    - Service: Custom...
    - Server: rtmp://live.autistici.org/ingest
    - Stream Key: the name you want for your stream.

        For example, if you want your stream to be available at the address "https://live.autistici.org/#myawesomebroadcast", your stream key should be "myawesomebroadcast".
        
    ![](/static/img/OBS-server.png "OBS Settings")

3. Finally click "OK".

4. Now you can start your stream by clicking the "Start Streaming" button: your stream will be visible at your chosen URL aftes a few seconds.

    ![](/static/img/OBSstartstream.png "Start stream")

5. If you want to check out what is being streamed right now, you can go to: [https://live.autistici.org/stats](https://live.autistici.org/stats)

You can find a more general guide on how to use OBS Studio [in the official wiki](https://obsproject.com/wiki/OBS-Studio-Overview).
