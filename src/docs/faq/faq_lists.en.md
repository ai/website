
title:  Frequently asked questions about mailing lists and newsletters
----

Frequently Asked Questions
==========================


I am the owner of a mailing list/newsletter, I forgot the password to manage a mailing list. What should I do ?
-----------------------------------------------------------------

If the owner email is hosted on our server, just login in the user panel, there's a link to reset the password.
Otherwise write to help@autistici.org telling us the name of the list.


I am the owner of a mailing list/newsletter, I would like to close the mailing list: what should I do ?
-----------------------------------------------------------------

Only we can close a mailing list, write to [help@autistici.org](mailto:help@autistici.org) specifying the name of the list.
If archives are enabled, tell us if you need a copy.


I am the owner of a mailing list/newsletter, one of subscriber do not get any message from the list.
-----------------------------------------------------------------------------------------

Ask to that user to check his/her spam folder, maybe messages are there.
Otherwise write to [help@autistici.org](mailto:help@autistici.org) specifying the address of the subscriber
and the list name.


I am the owner of a mailing list/newsletter, is it possible to change the list name and/or domain ?
-----------------------------------------------------------------------------------------

No, this is not possible. You have to request a new list and migrate all subscribers.


I would like to unsubscribe from a mailing list: what should I do?
------------------------------------------------------------------

If you want to unsubscribe from a mailing list in our server, go to https://www.autistici.org/mailman/listinfo/LISTNAME
(replacing LISTNAME with the particular list name, for example https://www.autistici.org/mailman/listinfo/test if the list is called test@autistici.org).
At the bottom of the page you'll find the option "unsubscribe", and if you don't know your password,
you can ask for it to be sent to your address through the specific box in the same page.


I would like to unsubscribe from a newsletter: what should I do?
------------------------------------------------------------------

Same as the previous question, but the link to visit is https://noise.autistici.org/mailman/listinfo/LISTNAME

I receive more than one daily digest.
-------------------------------------

Contact the mailing list admin and tell him/her to modify the digest_size_threshhold parameters in the digest section of
the list control panel.


Are there any limits for attachment sending?
--------------------------------------------

Yes: about 10Mb incoming and outgoing, but every list can have lower limits.


How often do you send your newsletters?
---------------------------------------

A/I's newsletter is not sent regularly: this means you'll receive, once in a while, an e-mail
messages from us. Usually our newsletters are about:

* Technical issues
* Updates about the project and services
* News we find particularly meaningful

You can also check our [blog](https://cavallette.noblogs.org/) for news.


I don't want to receive your messages any more. What should I do?
-----------------------------------------------------------------

Well, if you have a mailbox in our servers, you need to receive the few messages we send (sent by
A/I). This is the only way we have to contact you directly, to keep you updated about services and catastrophes and to
inform our community. You don't need a big effort to read them, so try to do it. At any rate, there are a lot of other free mail
services you can use. There is not a single bit in our newsletters that is about marketing, we promise you. It's stuff you need
to know.
