title:  frequently asked questions about websites
----

Frequently Asked Questions
==========================

I'd like to open a web space on the A/I servers, but I don't know if my project is suitable.
--------------------------------------------------------------------------------------------

First of all you should read our [manifesto](/who/manifesto) and our [policy](/who/policy).
After that, if you think your project is suited, go to the [sign-in](/get_service)" subscription form.


I need a counter. What should I do?
-----------------------------------

You can consult our [stats](https://stats.autistici.org/)", which include every website.

May I use a counter or other commercial services in my webpage?
---------------------------------------------------------------

No! Commercial counters are BAD. Apart from being commercial (which would be a reason enough), external conters 
make our efforts not to violate surfers' privacy vain.

These counters record data bases with personal data and IP addresses, which are then sold away. We don't like this.

As regards other commercial services, the same principle applies. You may have good reasons, and we can discuss 
about them, but we generally don't want commercial services in our spaces.

What size can my web space reach?
---------------------------------

There's no specific rule about this: let's say we don't set any limits, unless a space troubles a whole server.

If you want to upload "heavy" materials (mainly audio and video), we recommend you to host them in dedicated
space such as [Arkwiki](http://www.arkiwi.org).


Can I upload copyrighted material, such as mp3 files and films, thus violating their rights?
--------------------------------------------------------------------------------------------

No, we're sorry. Our servers provide services to thousands of people
and we don't want to incur in useless problems as regards copyright violation.
You can use P2P for that stuff.

I have a lot of "heavy" stuff to upload (mp3 files and videos under free licenses). Where shall I put it?
--------------------------------------------------------------------------------------------------------

Before you upload these files, you'd better contact us and check that there's enough space in the HD. At any rate, 
the right place to upload your material is your WebDAV space.

What application can I use to upload my web page?
-------------------------------------------------

Any WebDAV client, as detailed [in our webdav howto](/docs/web/webdav)

I need a blog. What should I do?
--------------------------------

Use [Noblogs](https://noblogs.org)", our blog platform.

I need a CMS: what should I use?
--------------------------------

We do not offer PHP anymore, so no CMS, only static sites...

What database software is installed in A/I's servers?
-----------------------------------------------------

MySql Server.  If you need a graphical frontend consider [adminer](https://www.adminer.org)

Can I use commercial banners in my site?
----------------------------------------

No, you can't. And if you do, you'll lose our respect (and, if you insist, your web site).

How can I set my favicon for my site ?
--------------------------------------
You have to use this html code in the HEAD section of your pages:

```python
 &lt;link rel="shortcut icon" type="image/x-icon" href="/path/to/favicon.ico" /&gt;
```
where /path/to is the relative path of your website.

I can't delete some directories in my WebDAV space.
---------------------------------------------------

Check that your WebDAV client shows hidden files, and delete the file .htaccess before removing the directory.