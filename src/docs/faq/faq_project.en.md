title:  About this project, about us, about infrastructures and about the world
----

Frequently Asked Questions
==========================


What's this project about?
--------------------------

If you don't have a clue, first read our <a href="/who/manifesto">manifesto</a>; 
if you still have doubts and you've also read our faqs, well, 
[write to use](mailto:info@autistici.org). We'll try to answer.

How much bandwidth do we use?
-----------------------------

At the moment our servers use about 20-30Mb/s. However, considering the number of users we have, the bandwidth we 
have at our disposal is always insufficient. 

What Operating System do you use?
---------------------------------

[Debian](http://www.debian.org) GNU/Linux.

Have you got any documents about the servers configuration/installation?
------------------------------------------------------------------------

We wrote some extensive documentation about our infrastructure model back in 2005, that is still relevant in the
general lines, and still deserves a read (even if it is quite outdated in the details). 
We called it [R plan](/who/rplan/) back then because it was an unprecedented effort in defining ways to build a Resistant network. 

More up to date materials can be found by browsing our [Code](https://git.autistici.org/public) that is fairly good documented.
The "new" infrastructure is based largely on [Float](https://git.autistici.org/ai3/float), some piece of code we built recently,
that we are fairly satisfied with.

If you are interested in how we set up things or you would like to replicate our setup these are good places to start.

How long have you been online?
------------------------------

The first server was installed March, 3th, 2001, and we've been online since April, 19th, 2001; so that is 
our birthday. The project was officially launched at the [Hackmeeting 2001](http://www.hackmeeting.org/hackit01)

How many mail users do you have? How many websites and mailing lists do you host?
---------------------------------------------------------------------------------

We can't give the exact figures, but we host about 16000 mailboxes, 1500 websites, 5500 lists and 10000 blogs.

Who is legally liable for this project?
---------------------------------------

The "AI ODV Association" is the legal subject signing contracts for connection and owning the domains
related to the "A/I" project.


I'd like to install a server and offer some/all services offered by A/I. How can I do it?
-----------------------------------------------------------------------------------------

Good: it's a brilliant idea. We can only advise you to read the docs about the configuration of a 
[Debian](http://www.debian.org) server, and perhaps give you some tips. [Float](https://git.autistici.org/ai3/float) is also a good tool
we wrote that you could consider using. 
 
Read some pages in our [Code](https://git.autistici.org/public) and contact us for any suggestion :)



Some services aren't working! This is unacceptable!
---------------------------------------------------

We're sorry about that. But if you find it unacceptable, we advise you to use a different server.
Our priority is privacy and security, and sometimes that means that a service is down. 
Our collective always tries to do its best to keep everything functioning, 
but as you should have already understood, we are never paid for this service and sometimes problems are 
difficult to solve. 
Trust us, give us a little time, and we hope we'll improve our services even more. And this will also be 
possible through your donations.

I wrote you a mail more than a week ago and you haven't answered yet. How so?
-----------------------------------------------------------------------------

Managing this project does request time, but it is not a job for us (meaning that we are not paid for it): 
that's why you can receive an answer 10 minutes or 10 days 
after you've sent us a message. This can depend from the weather or from a locust invasion: you can never know!


I've tried to fill in the form for requesting a service, but something went wrong. What should I do?
----------------------------------------------------------------------------------------------------

Perhaps you requested a service without having an account with us: in this case, you first have to ask for a 
mailbox and then you can ask other services. Or perhaps you requested a blog but you don't have a reliable
mail account. In this case, you first need a mailbox with us or with [another provider](/links), and then 
you can have your blog. Or else there are some problems in your browser setting: if you experience such problems, please contact us at 
[info@autistici.org](mailto:info@autistici.org).

The 'comment' field in the form is compulsory. What should I write in it?
-------------------------------------------------------------------------

Any hint about why you're requesting this service and perhaps what you want to do with it.


Why should I trust you?
-----------------------

Trust actually depends on you: we have a confidence relationship with many users of ours, and we have a 
[manifesto](/who/manifesto) describing all the aims of this project, and the [privacy policy](/who/privacy-policy). 
But this is not a reason enough to entrust us your whole privacy. Even if we try
to prevent this, we could lose control over our servers anytime (due to a police seizure, to a casual interruption of our services or to a bug, 
for instance). We can't therefore grant that your data in our servers will never be violated and warmly 
invite our users to:

* download all their personal data
* encrypt their personal archives.

Do you keep logs connecting service users with personal identities?
-------------------------------------------------------------------

No. However, we can't stop you from using your real name and surname in the mail address you choose, if you so wish.
That would be a bad idea, but we can't know if you really did it, since we did not ask your real name in the first place.

What logs to connections do you keep? According to what principles?
-------------------------------------------------------------------

We don't keep any logs containing sensitive data.
Our servers keep no records of the IP addresses by users connecting to them.
We only keep logs for debugging and security purposes.
To learn more, read our [policy](/who/policy) and the [privacy policy](/who/privacy-policy).
