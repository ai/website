title:  frequently asked questions about progaganda and press
----

Frequently Asked Questions
==========================



I've seen your wonderful +kaos sweaters and I'd like to have one. What should I do? How much do you ask for them?
-----------------------------------------------------------------------------------------------------------------

Those sweaters were all produced and distributed in 2002.
Unfortunately we have no time to manage wares and distribution. As for our whole merchandising, the production
of sweaters will be linked to particular events, such as our <[Kaos Tour](http://kaostour.autistici.org).

I've seen your brilliant T-shirt with a locust on it. How can I get one?
------------------------------------------------------------------------

That was made for the 2004 spring-summer season! Perhaps we'll distribute some T-shirts in our upcoming
events, but we haven't developed an on-demand distribution system yet.

I've seen you stickers and would like to have some: what should I do?
---------------------------------------------------------------------

The easiest solution is to download the relevant file and to print them near where you live! 
Look for what you want in our [Propaganda](/who/propaganda) section.

May I print your stickers on my own?
------------------------------------

Sure! This is also the only sure way to get them ;) 
Download the files [here](/who/propaganda).

I'd like to interview some of you: is it possible?
--------------------------------------------------

Sure, but we can't assure anything. We generally prefer to be interviewed via e-mail and
the text of the article must be released under a Creative Commons license.

Can I write an article about you?
---------------------------------

Of course. However, we would like to be informed about it.

I'd like to invite you to a TV show...
--------------------------------------

We don't usually like TV, but if you are OK with us wearing a Mickey Mouse mask, that could be possibly be done.