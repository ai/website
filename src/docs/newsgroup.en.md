title: Newsgroup Howto
----

Newsgroup Howto
=================

News servers are one of the oldest services hosted on the internet: they are used to communicate and exchange files in thematic areas called
"groups".

To access this service you need a newsgroup client, but most mail client allow you to read newsgroup as well: Thunderbird and Sylpheed for
Windows and Linux, as well as more basic UNIX software like pan, knode, slrn.

Parameters to access A/I Newsgroup Server are:

> Servername: news.autistici.org
> Port: 119  (or 563 for SSL)

Once you'll have downloaded the list of the "groups" hosted on our newsserver you can subscribe to the ones you are interested into and
download all of its messages every time you connect to the server.

It's possible to connect to A/I Newsserver also via encrypted SSL connection, but you'll have to use a client supporting this feature.

To access the Newsserver no authentication is needed. Groups are open to writing by anyone and public for reading by anyone.


