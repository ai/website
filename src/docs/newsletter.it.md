title: Come si usano le newsletter
----

# Come si usano le newsletter

## Che cos'è una newsletter

Una newsletter è sostanzialmente una lista di indirizzi a cui automaticamente vengono inviati dei messaggi da un programma presente sul
server (la macchina che fornisce il servizio).

A differenza di una mailing list, le newsletter sono impostate per non ricevere le eventuali risposte degli iscritti,
in pratica funzionano come una mailing list **chiusa**.


## Come richiedere una newsletter

Per attivare una newsletter è necessario fare richiesta su
[services](https://services.autistici.org) e chiedere una mailing list nella pagina di richiesta del
servizio, e nella pagina successiva inserire il nome della newsletter, il dominio, l'indirizzo email
di chi la gestirà e mettere il segno di spunta su "Usala come una newsletter".

Una volta che approviamo la richiesta (potrebbe volerci qualche giorno)  dovreste ricevere un email di conferma della
creazione di una newsletter con il nome, la password e il link di amministrazione.

## Amministrare una newsletter

Conoscendo il nome potete visitare la pagina relativa alla newsletter.

Ad esempio, se la vostra lista si chiama NOME123 potete visitare la pagina:

**https://noise.autistici.org/mailman/listinfo/NOME123**

Entrando nel pannello di amministrazione potete aggiungere le persone da iscrivere alla newsletter.

Per poter abilitare un indirizzo email a inviare messaggi a una newsletter bisogna:

 - **aggiungerlo tra gli iscritti**.
 - **togliere il flag di moderazione**

Questo va fatto per ogni indirizzo email che si vuole autorizzare a inviare messaggi.

## Opzioni Generali

Nella sezione *Opzioni generali* troverete le impostazioni che riguardano la
pagina di benvenuto della lista, la sua descrizione, i messaggi di benvenuto da
inviare agli utenti, la grandezza massima degli allegati che possono essere
accettati, ecc.

La maggior parte dei parametri che trovate predefiniti in questa sezione
dovrebbero andare bene (ovviamente le descrizioni e i messaggi di benvenuto
sono a vostra totale discrezione e non hanno valore preimpostato), ma
**non dovrete mai cambiare l'opzione "Nome host (host_name)"**
altrimenti la vostra newsletter smetterà di funzionare.


## Gestioni iscritti

Questa sarà una delle sezioni delle opzioni di impostazione che frequenterete con maggiore assiduità.

Per iscrivere molti indirizzi tutti insieme potete visitare la sezione *Iscrizione di massa* dove potrete includere - uno per riga -
un elenco di mail da aggiungere alla lista e un eventuale messaggio di benvenuto personalizzato.

Consigliamo di non iscrivere centinaia di persone se non avete il loro esplicito consenso, nel dubbio **mettete la spunta su "invita"**,
non su "iscrivi" nella pagina di iscrizione di massa, in modo che gli utenti ricevano un messaggio di invito con un link da cliccare
per completare l'iscrizione.

![](/static/img/man_newsletter/AI-bulk-subscription.png "Iscrizione massiva")

Nell'*Elenco iscritti* troverete un lista di tutte le persone iscritte alla newsletter seguite da una serie di opzioni:

- *Moderato* significa che l'iscritto non potrà scrivere all'indirizzo della newsletter (e deve essere cosi', altrimenti diventa una
	mailing list). Solo chi e' autorizzato a spedire i messaggi deve essere non moderato, quindi ad esempio dovete mettere come
	non moderato il vostro indirizzo e/o quello di chi e' autorizzato a inviare i messaggi. Nota: l'email dell'admin della newsletter
    non e' automaticamente autorizzato a inviare messaggi, anche questo indirizzo deve essere iscritto e non moderato.

![](/static/img/man_newsletter/AI-newsletter-moderato.png "Moderazione")

Se volete disiscrivere un singolo utente, cliccate sul box *Cancella* a fianco della sua mail e poi su *"Applica le modifiche"* in fondo
alla pagina.

![](/static/img/man_newsletter/AI-newsletter-cancella.png "Cancellazone")

## Opzioni per la privacy

Nei *Filtri sul mittente* è impostato che i messaggi dei non iscritti devono essere *sospesi*, potete anche impostare *scartati* per
non ricevere le notifiche di questi messaggi.
Se volete autorizzare all'invio qualcuno che non e' iscritto alla newsletter,
andate nella sezione Privacy -> Filtri sul mittente
e cercate l'opzione "List of non-member addrresses whose postings should be automatically accepted"
e aggiungete l'indirizzo da autorizzare.





Per ulteriori dettagli vi rimandiamo al manuale di mailman:

**https://www.gnu.org/software/mailman/mailman-admin/index.html**
