title: Postfix Configuration Howto
----

Postfix Configuration Howto
===============================

This Howto has been written for Debian GNU/Linus distribution

Check you have postfix, libsasl2, libsasl2-modules, openssl packages installed correctly

Most of the files mentioned should already be there and you should be able to edit them easily. If a file does not exist you can create it
with the command *touch filename* for files and *mkdir directoryname* for directories.

In the following howto USER should be changed in the username of the user you login with on your computer, while ADDRESS@DOMAIN.ORG should
be changed in your full A/I mailbox address. PASSWORD should be changed in your mailbox password.

The file you'll need to edit for successfully sending email through A/I servers using postfix are the following ones.

#### */etc/aliases* file

Check it contains the following line

    root:   USER

If it does not, add it and then run the command:

    # postalias /etc/aliases

#### */etc/postfix/sender\_canonical* file

Add the following lines:

    root    ADDRESS@DOMAIN.ORG
    USER   ADDRESS@DOMAIN.ORG

Then run:

    # postmap /etc/postfix/sender_canonical

#### */etc/postfix/virtual* file

All the mail addresses you download via fetchmail should be pointing at your login user on your computer.

    ADDRESS@DOMAIN.ORG      USER
    giovanni@alice.it       USER
    ... ecc. ....           USER

Then run:

    # postmap /etc/postfix/virtual

#### */etc/postfix/sasl\_pwd* file

Add the following line, but be aware that this means that your password will be stored in plain text in your filesystem. However this is the
only way to use postfix to send email through postfix.

    smtp.autistici.org [ tab ] ADDRESS@DOMAIN.ORG:PASSWORD

Then run:

    # postmap /etc/postfix/sasl_pwd

#### Certificates

Install the ca-certificates package

#### Postfix Keys

Create a file named *random\_file* in the */etc/postfix/ssl* directory.

In the file type in 2-3 lines of random digits (just hit your keyboard at random). Then run the following command:

    # openssl genrsa -rand random_file -out chiave-privata.pem 1024

Rename the new *chiave-privata.pem* file and copy it in */etc/postfix/ssl/postfix-key*

#### Create */etc/postfix/tls-sites* file

Edit the file and add the following line:

    smtp.autistici.org [ tab ] MUST

#### Main Postfix configuration

Now we are almost done.

Edit the */etc/postfix/main.cf* file and add the following lines at the end of the file

    relayhost = [smtp.autistici.org]:587
    smtp_sasl_auth_enable = yes
    smtp_sasl_password_maps = hash:/etc/postfix/sasl_pwd
    smtp_sasl_mechanism_filter = plain, login
    smtp_sasl_security_option =
    smtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt
    smtp_tls_key_file = /etc/postfix/ssl/postfix-key
    smtp_use_tls = yes
    smtp_tls_per_site = hash:/etc/postfix/tls-sites
    smtp_tls_loglevel = 1
    smtp_sasl_tls_security_options = noanonymous
    # EOF

**Note:** if you are already using postfix to send your email remember to comment out the relayhost line you were using until configuring
your server for A/I.

After each change in the previous file it's **mandatory** to run the followin *postalias* or *postmap* command.

Once you are finished with the */etc/postfix/main.cf* file, run the following command and you are done

    # postfix reload

#### Postfix logfiles

If you want to check if postfix is sending email or what kind of errors it is encountering, once your mail has been sent you can check the
logfile

    # less /var/log/mail.log

Thanks to rainbow for this howto
