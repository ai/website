title: Como usar Roundcube Webmail
----

Como usar Roundcube Webmail
===============================

- [Cómo acceder a webmail](#login)
- [Cómo agregar direcciones a la libreta de direcciones](#rubrica1)
- [Cómo enviar mensajes a más de una dirección](#rubrica2)
- [No puedo encontrar "Enviados" o la carpeta "Borradores"](#cartelle)

**Nota:** la mayoría de pantalla en este como están en italiano, estamos muy ocupados y todavía no la hemos puesto a su disposición en otros
idiomas. Si tienes tiempo y quieres ayudarnos, por favor haz la captura de pantallas que faltan y las envían a nosotros! Tu ayudaras a hacer
de este como mejor!

<a name="login"></a>

Acceder a su Panel de Control y Webmail
---------------------------------------

Para acceder a su sesión de webmail ingrese por la [pagina de inicio A/I](/) como se muestra en la imagen:

![](/static/img/man_mail/en/roundcube_login_01.png)

Ahora que ha ingresado al Panel de Control. Puede leer su correo haciendo clic sobre su dirección de correo electrónico o en el icono
pequeño.

![](/static/img/man_mail/en/roundcube_login_02.png)

<a name="rubrica1"></a>
Libreta de direcciones
----------------------

Para añadir un nuevo contacto a la libreta de direcciones, haga clic en *Libreta de direcciones* en la esquina superior derecha y luego en
el icono *Añadir contacto* (el que tiene el pequeño círculo verde con el signo *+*).

![](/static/img/man_mail/it/roundcube_addressbook_01.png)

Llene todos los campos que considere necesarios y haga clic en *Guardar*.

![](/static/img/man_mail/it/roundcube_addressbook_02.png)

![](/static/img/man_mail/it/roundcube_addressbook_03.png)

![](/static/img/man_mail/it/roundcube_addressbook_04.png)

<a name="rubrica2"></a>

**Para enviar mensajes a más de una dirección**, haga clic en *Libreta de direcciones* en la esquina superior derecha y seleccione más de un
contacto, manteniendo oprimida la tecla **Ctrl** (es un acceso directo para selecciones múltiples!).

A continuación, haga clic en el icono de *mensaje nuevo* y todos sus contactos estarán allí!

![](/static/img/man_mail/it/roundcube_addressbook_05.png)

![](/static/img/man_mail/it/roundcube_addressbook_06.png)

![](/static/img/man_mail/it/roundcube_addressbook_07.png)

<a name="cartelle"></a>

Carpetas
--------

**Si no puede ver la carpeta *Enviados* o la carpeta *Borradores*** ante todo pruebe en el menú *Configuración* en la esquina superior
derecha de la página: entre en la ficha *Carpetas* y asegúrese de que la casilla respectiva está marcada.


![](/static/img/man_mail/it/roundcube_folders_01.png)

![](/static/img/man_mail/it/roundcube_folders_02.png)

Si las carpetas *Enviados* o *Borradores* no existen, puede crear el espacio usando *Crear*.