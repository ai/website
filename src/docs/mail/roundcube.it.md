title: Manuale per l'utilizzo della webmail (basata sul software libero roundcube)
----

Manuale per l'utilizzo della webmail (basata sul software libero roundcube)
==================================================================================

- [Come accedere alla webmail](#login)
- [Come inserire contatti nella rubrica](#rubrica1)
- [Come inviare mail a più destinatari](#rubrica2)
- [Non vedo la cartella coi messaggi inviati o con le bozze](#cartelle)

<a name="login"></a>

Come accedere alla webmail
--------------------------

Per accedere alla webmail potete fare il login direttamente dalla homepage [www.autistici.org](/) come indicato:

![](/static/img/man_mail/it/roundcube_login_01.png)

A questo punto potete leggere la posta cliccando su "Leggi la posta"

![](/static/img/man_mail/it/roundcube_login_02.png)

Nota: Gli screenshot seguenti sono stati fatti usando il tema grafico "Elastic" di roundcube, selezionabile nelle opzioni.

<a name="rubrica1"></a>

Rubrica
-------

Per inserire un nuovo contatto nella rubrica cliccate su *Rubrica* in alto a destra e poi sull'icona col segno *+* (*Aggiungi un contatto*).

![](/static/img/man_mail/it/roundcube_addressbook_01.png)

Compilate i campi che compaiono nella parte destra dello schermo (Nome e Cognome possono essere trascurati) e cliccate su *Salva*

![](/static/img/man_mail/it/roundcube_addressbook_02.png)

![](/static/img/man_mail/it/roundcube_addressbook_03.png)

![](/static/img/man_mail/it/roundcube_addressbook_04.png)

<a name="rubrica2"></a>

**Per inviare un messaggio a più destinatari** cliccare su *Rubrica* in alto a destra

![](/static/img/man_mail/it/roundcube_addressbook_05.png)

![](/static/img/man_mail/it/roundcube_addressbook_06.png)

![](/static/img/man_mail/it/roundcube_addressbook_07.png)

<a name="cartelle"></a>

Cartelle
--------

**Se non vedete le cartelle dei messaggi inviati o delle bozze:**

![](/static/img/man_mail/it/roundcube_folders_01.png)

![](/static/img/man_mail/it/roundcube_folders_02.png)

Se la cartella *Sent* o *Drafts* non esistono potete crearle cliccando su *Crea*.