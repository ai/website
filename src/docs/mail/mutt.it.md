title: Configurazione di Mutt
----

# Configurazione di Mutt

Questa pagina raccoglie varie indicazioni su come configurare il
client di posta [Mutt](https://www.mutt.org) con il vostro account
A/I.

Negli esempi qui sotto, UTENTE@DOMINIO.ORG è il vostro account, e
PASSWORD la relativa password.

### Leggere la posta (IMAP)

Usando IMAP, i messaggi vengono lasciati sul server, e si possono
utilizzare varie cartelle: è una modalità d'uso pratica, ma
consigliata solo se cancellate regolarmente i messaggi (altrimenti
prima o poi lo spazio sui server finisce).

Per usare il vostro account A/I in questa modalità basta aggiungere
queste direttive al file di configurazione *~/.muttrc*:

```
set folder="imaps://UTENTE@DOMINIO.ORG@mail.autistici.org:993/"
set imap_pass="PASSWORD"
set spoolfile=+INBOX
```

### Leggere la posta (POP)

Con POP è possibile invece scaricare i nuovi messaggi dal server e
trasferirli ogni volta sul proprio computer, ma POP è consapevole solo
dell'esistenza del folder principale INBOX.

Per configurare mutt con POP, in *~/.muttrc*:

```
set folder="pops://UTENTE@DOMINIO.ORG@mail.autistici.org:995/"
set pop_pass="PASSWORD"
set spoolfile=+INBOX
```

Se avete dei conflitti con altri account configurati sul vostro mutt,
che impediscono un'autenticazione corretta, provate ad aggiungere al
vostro *~/.muttrc* la riga:

```
set pop_authenticators="digest-md5:apop:user"
```

### Inviare la posta con Mutt

Le versioni recenti di Mutt possono inviare direttamente messaggi
usando il protocollo SMTP, basta aggiungere quest'altra sezione sempre
allo stesso file *~/.muttrc*:

```
set from="UTENTE@DOMINIO.ORG"
set use_from=yes
set smtp_url="smtps://UTENTE@DOMINIO.ORG@smtp.autistici.org:465/"
set ssl_force_tls=yes
```
Se avete dei conflitti con altri account configurati sul vostro mutt,
che impediscono un'autenticazione corretta, provate ad aggiungere al
vostro *~/.muttrc* la riga:

```
set smtp_authenticators="login:plain"
```

### Inviare la posta con msmtp

Con versioni più vecchie di Mutt era necessario utilizzare un
programma separato per l'invio della posta. Il programma più
comunemente usato a questo scopo è [msmtp](/docs/mail/msmtp).

```
set sendmail="/usr/bin/msmtp -a UTENTE@DOMINIO.ORG"
```
