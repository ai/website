title: How to archive emails
----

How to archive emails
=====================

We are fine acting as a long term storage for our users' emails, even more if
you have enabled encryption for your mailbox. Nevertheless, our storage
resources are finite and, for this reason, we notify the users who exceed a
certain amount of space used by their mailboxes.

We encourage you to keep the space you use on our servers under control. What
follows is an easy method to archive locally your emails, in order to offload us
from the files you save locally. To be clear, these files will be available **on
the device you archive them ONLY**.


Archive emails locally with Thunderbird
=======================================

Having your account [already configured][tc] with Thunderbird, you must first set to
save the archives in "Local Folders"

![Set archives as local](/static/img/man_mail/en/archive_email_1.png)

Then go to the folder that contains the messages you want to archive (i.e. the INBOX)

![The INBOX folder is where the incoming emails are](/static/img/man_mail/en/archive_email_2.png)

Select the emails you want to archive (ctrl-a if you want to select them all, but it is not
recommended to select more than selecting 2000 mails at a time)
If you need to download several thousands of email consider to configure a [POP3 account][tc] in thunderbird.

![The emails selected are highlighted](/static/img/man_mail/en/archive_email_3.png)

Click the `Archive` button in the header of the bottom section. This will create
a folder for each year in the group of emails you have received and move said
emails in those folders

![The newly created Archive folder is on the left bar](/static/img/man_mail/en/archive_email_4.png)

You can select the folder and access the old emails, but only on this computer

![The archived folder is similar to ordinary email folders](/static/img/man_mail/en/archive_email_5.png)


[tc]: /docs/mail/thunderbird
