title: Come archiviare le email
----

Come archiviare le email
========================

Siamo a nostro agio con l'idea di ospitare a tempo indefinito le caselle e gli
archivi email dei nostri utenti, soprattutto se avete abilitato la cifratura
della casella. Ciononostante, le nostre risorse non sono infinite e quindi
avvertiamo gli utenti che usano troppo spazio sui nostri server.

Vi incoraggiamo a tenere lo spazio che usate con i nostri servizi sotto
controllo. Ciò che segue è un metodo facile per archiviare localmente (quindi su
un vostro computer) le vostre email, e togliere un po' di spazio di
archiviazione dai nostri server. Per essere completamente chiari, le email che
archiviate **saranno disponibili SOLO sul computer su cui le salvate**.


Archiviare le email localmente con Thunderbird
==============================================

Supponendo che il vostro account sia [già configurato][tc] su Thunderbird,
andate nelle opzioni dell'account e impostate di conservare gli archivi in "Local Folders"

![Impostare archiviazione in locale](/static/img/man_mail/it/archive_email_1.png)

Andate nella cartella dove sono i messaggi che volete archiviare (ad esempio nella `INBOX`)

![La cartella INBOX è dove arrivano le nuove mail](/static/img/man_mail/it/archive_email_2.png)

Selezionate tutte le email che volete archiviare (ctrl-a per selezionarle tutte,
ad esempio, ma e' sconsigliato fare questa operazione su piu' di 2000 mail alla volta).
Se dovete scaricare svariate migliaia di email, forse meglio farlo configurando un [account POP3][tc] in thunderbird

![Le email selezionate sono evidenziate](/static/img/man_mail/it/archive_email_3.png)

Premete il tasto `Archivia` nella testata della sezione inferiore. Questo
creerà, se non già presente, una cartella per ogni anno delle email che state
archiviando nel gruppo `Archivio`, che potete raggiungere dalla barra sinistra

![Il gruppo Archivio è nella barra sinistra](/static/img/man_mail/it/archive_email_4.png)

Selezionando una cartella, potete accedere alle email che avete archiviato, ma
solo su questo computer

![La cartella con i messaggi archiviati è simile ad una normale cartella](/static/img/man_mail/it/archive_email_5.png)


[tc]: /docs/mail/thunderbird
