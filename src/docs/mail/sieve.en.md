title: How to setup filters in your A/I webmail
----

How to setup filters in your A/I webmail
========================================

What is a filter?
-----------------

From july 2010 on the A/I webmail system allows every user to setup filters autonomously.

A filter is a rule allowing a user to move, copy, remove messages from your inbox depending on their sender, subject, size or any other
header you can think of.

You will find a web interface to manage your personal filters in your webmail, after you log in our [home page](/) with user and password.

How can I add a filter?
-----------------------

![](/static/img/man_mail/en/sieve.png)

1.  Log into your web mailbox
2.  Go to the "Settings" section
3.  Go to the "Filters" section
4.  Click on the "Add Filter" icon
5.  Choose a straightforward name for your filter (like the criteria you are using to sort the messages)
6.  With the first dropdown menu choose which header you are using to sort the messages: sender, subject, size, etc.
7.  With the second dropdown menu choose what should be checked of that header: contains a word, is equal to a word, etc.
8.  In the text box next to the menus write the word you want the header checked against
9.  Click on the Add button
10. Choose what should happen to the messages matching the filter criteria: be moved to a folder; be deleted; be forwarded to a certain mail
    address; and so on.
11. Click on the Save button
