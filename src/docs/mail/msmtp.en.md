title: msmtp Configuration
----

# msmtp Configuration

This page explains how to configure [msmtp](https://marlam.de/msmtp/)
for sending emails via your A/I account.

In the example below, USER@DOMAIN.ORG is your account name (full email
address), and PASSWORD the associated password.

Ensure you have the *ca-certificates* package installed, and add the
following to your *~/.msmtprc* file:

```
account UTENTE@DOMINIO.ORG
port 587
from UTENTE@DOMINIO.ORG
user UTENTE@DOMINIO.ORG
auth on
password PASSWORD
tls on
tls_starttls on
tls_trust_file /ets/ssl/certs/ca-certificates.crt
host smtp.autistici.org
tls_certcheck on
```

To check that the configuration works, try sending a test email to
yourself:

```
echo test | msmtp -a UTENTE@DOMINIO.ORG UTENTE@DOMINIO.ORG
```
