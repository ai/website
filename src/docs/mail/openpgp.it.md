title: Associare una chiave OpenPGP al proprio account
----

# Associare una chiave OpenPGP al proprio account

Se avete una chiave OpenPGP (PGP/GPG), è possibile associarla al
proprio account di posta, per fare in modo che i client possano
trovarla ed utilizzarla automaticamente. All'atto pratico questo si
ottiene con il protocollo [WKD](https://wiki.gnupg.org/WKD),
supportato da versioni sufficientemente recenti di Thunderbird, oltre
che da GPG stesso. Questo protocollo nasce come alternativa ai
tradizionali *key server* PGP, il cui uso è ormai scoraggiato, ed in
pratica delega la pubblicazione delle chiavi alla stessa
infrastruttura che gestisce la posta.

È possibile associare **una singola chiave** OpenPGP al proprio
account di posta, tramite il [pannello
utente](/docs/userpanel#pgp). Non è attualmente possibile associare
più di una chiave ad un account.

## Identità OpenPGP ed alias

Una chiave OpenPGP può includere più di una identità, corrispondenti a
diversi indirizzi email. La stessa cosa in un certo senso vale per il
tuo account di posta, se hai degli alias, tanto che è perfettamente
possibile, per esempio, avere una chiave OpenPGP che non corrisponde
al proprio indirizzo email primario. Per evitare confusioni, e per non
rivelare inavvertitamente legami tra account, è importante capire come
il sistema associa in dettaglio le chiavi OpenPGP agli indirizzi
email.

All'atto dell'upload della chiave OpenPGP per un account, il sistema
considera ciascuna identità in essa contenuta, e verifica se questo
indirizzo email corrisponde all'indirizzo primario dell'account o ad
uno dei suoi alias. Se questo è il caso, la chiave verrà associata a
questo indirizzo email.

## Scadenza

Il pannello utente mostrerà un avviso quando la chiave OpenPGP è
scaduta o in scadenza, ma al di là di ciò nessuna altra azione verrà
intrapresa: la chiave non verrà rimossa dall'account.

Per aggiornare una chiave scaduta basta [uploadarla di
nuovo](/docs/userpanel#pgp), una volta che se ne sia estesa la
validità.

## Revoca

Non c'è un meccanismo automatico per pubblicare certificati di
revoca. Se si vuole revocare una chiave OpenPGP associata al proprio
account, la cosa più semplice da fare è semplicemente [rimuoverla
dall'account](/docs/userpanel#pgp).
