title: Postfix Howto
----

Postfix Howto
=================

Se state usando Debian, in fase di installazione non viene installato postfix, ma exim4.

Pertanto dovete installare postfix nel solito modo:

    # apt-get install postfix

Una volta installato, postfix contiene tutto quanto per poter usato con tls ed inoltre exim4 verrà automaticamente disinstallato ed
eliminato. Controllate che siano già installati, altrimenti installateli, i seguenti pacchetti : **libsasl2 libsasl2-modules openssl**

Iniziamo a configurare postfix. Alcuni file dovrebbero già essere presenti, altrimenti li create con il seguente comando:

    # touch nome_file

Poi create la directory:

    # mkdir /etc/postfix/ssl

Nei seguenti esempi dovrete sostituire la parola UTENTE con il vostro utente di sistema (l'utente che usate per accedere al computer), la
parola UTENTE@DOMINIO.ORG con il vostro indirizzo di posta completo e la parola PASSWORD con la vostra password.

Questi sono i file interessati alla configurazione di postfix per usare il server di Autistici

#### */etc/aliases*

il file deve avere inserita la seguente riga:

    root:   UTENTE

Dovrebbe essere già a posto, ma è meglio controllare ed eventualmente sistemare. Poi dare il seguente comando:

    # postalias /etc/aliases

#### */etc/postfix/sender\_canonical*

Al file aggiungete:

    root		UTENTE@DOMINIO.ORG
    UTENTE		UTENTE@DOMINIO.ORG

Poi dare il seguente comando:

    # postmap /etc/postfix/sender_canonical

#### */etc/postfix/virtual*

Tutti gli indirizzi email che ho in .fetchmailrc devono essere indirizzati al vostro utente

    UTENTE@DOMINIO.ORG		UTENTE
    giovanni@alice.it       UTENTE
    ... ecc. ....           UTENTE

Poi date il seguente comando:

    # postmap /etc/postfix/virtual

#### */etc/postfix/sasl\_pwd*

Al file aggiungete:

    smtp.autistici.org [ tab ] UTENTE@DOMINIO.ORG:PASSWORD

Poi date il seguente comando:

    # postmap /etc/postfix/sasl_pwd

#### Certificates

Installate il pacchetto ca-certificates (che contiene la CA di Letsencrypt)

#### Le chiavi di Postfix

Dobbiamo creare la chiave di postfix.

Andate nella dir */etc/postfix/ssl* e create un file chiamato *file\_casuale*.

All'interno di questo file, digitate due/tre righe di lettere, numeri, simboli a caso. Al termine, date questo comando:

    # openssl genrsa -rand file_casuale -out chiave-privata.pem 1024

Rinominate il nuovo file *chiave-privata.pem* in *postfix-key* e copiatelo in */etc/postfix/ssl/postfix-key*

#### Creare il file */etc/postfix/tls-sites*

All'interno del file digitate:

    smtp.autistici.org [ tab ] MUST

#### Configurazione principale di Postfix

A questo punto, abbiamo quasi terminato e dobbiamo soltanto aggiungere le seguenti righe al file di configurazione di postfix
*/etc/postfix/main.cf*

Andate in fondo al file main.cf ed aggiungete:

    relayhost = [smtp.autistici.org]:587
    smtp_sasl_auth_enable = yes
    smtp_sasl_password_maps = hash:/etc/postfix/sasl_pwd
    smtp_sasl_mechanism_filter = plain, login
    smtp_sasl_security_option =
    smtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt
    smtp_tls_key_file = /etc/postfix/ssl/postfix-key
    smtp_use_tls = yes
    smtp_tls_per_site = hash:/etc/postfix/tls-sites
    smtp_tls_loglevel = 1
    smtp_sasl_tls_security_options = noanonymous
    # EOF

**p.s. : ricordatevi, se usate già postfix, di commentare la riga relayhost con indicato l'altro server di posta che stavate usando !**

Ogni volta che variate uno dei file sopra indicati, **non scordatevi** di dare il comando *postalias* oppure *postmap* indicato.

Poi, dopo qualsiasi variazione ai file di postfix , è **TASSATIVO** dare il comando:

    # postfix reload

Altrimenti il tutto non funziona più...

#### File di log di postfix

Se volete vedere se è tutto ok oppure quali sono gli eventuali problemi, una volta spedita la mail potete controllare nel file di log:

    # less /var/log/mail.log

oppure meglio ...

    # tail -f /var/log/mail.log

Documentazione gentilmente fornita da rainbow che A/I ringrazia sentitamente
