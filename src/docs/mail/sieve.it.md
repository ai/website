title: Guida alla configurazione dei filtri sulla webmail
----

Guida alla configurazione dei filtri sulla webmail
============================================

Che cosa sono i filtri
----------------------

Da luglio 2010, il sistema di posta elettronica di A/I consente l'impostazione di filtri direttamente nella vostra webmail.

Un filtro è una regola che consente di spostare, copiare o rimuovere automaticamente messaggi di posta che avete ricevuto, in base al
mittente, al destinatario, all'oggetto o a qualsiasi altra intestazione contenuta nel messaggio.

Troverete un'interfaccia per amministrare i vostri filtri personali (in tutto e per tutto simili ai filtri che potete normalmente impostare
in un client di posta come Thunderbird o Outlook) nella vostra webmail, raggiungibile dal pannello una volta che vi sarete collegati dalla
[nostra homepage](/) con utente e password.


Come aggiungere un filtro
-------------------------

![](/static/img/man_mail/it/sieve.png)

1.  entrate nella vostra webmail
2.  andate sul bottone "Impostazioni"
3.  andate sul bottone "Filtri"
4.  in alto a sinistra cliccate sull'icona "Aggiungi Filtro"
5.  scegliete un nome chiaro per il vostro filtro (un buon esempio è il motivo per il quale state filtrando i messaggi, il nome della lista
    o del mittente o dell'oggetto che state selezionando)
6.  con il primo menù a tendina scegliete quale aspetto del messaggio analizzare: mittente, destinatario, oggetto, dimensione, ecc.
7.  con il secondo menù a tendina indicate una condizione da verificare: "contiene", "non contiene", ecc.
8.  nel riquadro a destra indicate la parola da filtrare
9.  cliccate su aggiungi
10. scegliete cosa deve succedere ai messaggi che vengono individuati dal filtro: potete scegliere di spostare il messaggio in una certa
    cartella, di inoltrare il messaggio a un certo indirizzo di posta, di cancellare il messaggio.
11. cliccate su salva
