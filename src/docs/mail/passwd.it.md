title: Come cambiare o recuperare la password della propria mailbox
----

Come cambiare o recuperare la password della propria mailbox
=====================

A/I ha preparato un servizio per i più smemorati, che permette di recuperare la propria password rispondendo ad una semplice domanda...

Dato che la domanda di default riguarda gli animali domestici il servizio si chiama GATTI.

Ovviamente è <u>importantissimo</u> impostare la propria risposta prima di perdere la password!

Si imposta attraverso il [pannello utente](/pannello/)

- - -

Come recuperare la Password
---------------------------

### Step 1 - Arriva il gatto

Il gatto viene in tuo aiuto automaticamente quando non ricordi password. Vai sulla pagina di [login](https://accounts.autistici.org),
e clicca sul link  "dimenticato la password ?" per andare alla pagina del gatto.

### Step 2 - Rispondi alla domanda del gatto

Adesso appare la domanda che avevi impostato in precedenza. Inserisci la risposta corretta e poi scegli la nuova password.


