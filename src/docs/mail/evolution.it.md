title: Configurazione di Evolution
----

Configurazione di Evolution
===================

Queste istruzioni si riferiscono ad Evolution, client posta per Linux.

-   Per creare un account cliccare sul menù *Edit-&gt;Preferences*
-   Selezionare *Account* e poi cliccare su *Add*.
-   Nella finestra di dialogo *Identity* inserire la propria identità (es. Utente) ed il proprio indirizzo e-mail, poi cliccare *Next*.
-   Nella finestra *Receiving Email* selezionare il tipo di server (POP o IMAP a seconda della vostra scelta), poi su
    *Configuration-&gt;Server* inserire: "mail.autistici.org", e su *Username* il proprio indirizzo email completo. 
    Dal menù *encryption method* selezionare "STARTTLS after connecting" poi cliccare *Next*.

![](/static/img/man_mail/en/Evolution-1.jpg)

-   Personalizzate *Receiving Options* a piacere e poi *Next*.
-   Nella finestra *Sending Email* selezionare *Server Type*:"SMTP", in *Server Configuration-&gt;Server*: "smtp.autistici.org",
    barrare la casella *Server requires authentication* e abilitare la connessione STARTTLS. 
    In *Authentication* selezionare "PLAIN" ed inserire il proprio indirizzo email.

![](/static/img/man_mail/en/Evolution-2.jpg)

-   Cliccate *Next* fino alla fine.


Si ringrazia Radiodurito e Michael English per questa guida.
