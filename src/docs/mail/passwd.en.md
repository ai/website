title: How to change or recover your mail password
----

How to change or recover your mail password
===================

A/I offers a service for those of you with very bad memory, that allows you to recover your password by answering a simple question...

It is **very important** you set your answer before losing the password! (we suggest doing it at soon as your account is activated).

How to recover your password
----------------------------

### Step 1 - Click on "Forgot your password?" in the login form

Go to the [login](https://accounts.autistici.org) page and click on
"Forgot your password?" under the login form.

### Step 2 - Answer your recover question

Now you see the question you set when you first got your password. Enter the correct answer and choose a new password.
