title: Roundcube Webmail Howto
----

Roundcube Webmail Howto
===========================

- [How to access the webmail](#login)
- [How to add addresses to my addressbook](#rubrica1)
- [How to send messages to more than one address](#rubrica2)
- [I cannot find "sent-mail" or "draft" folder](#cartelle)

<a name="login"></a>

Access your User Panel and Webmail
----------------------------------

To access your webmail login from [A/I homepage](/) as shown in the picture:

![](/static/img/man_mail/en/roundcube_login_01.png)

Now you entered your User Panel. You can read your mail clickin on both your mailbox address or the small mail icon.

![](/static/img/man_mail/en/roundcube_login_02.png)

**Note:** the following screenshots was made using the "Elastic" rounduce graphical theme, available in the settings.

<a name="rubrica1"></a>

Address Book
------------

To add a new contact to your addressbook click on *Addressbook* in the top-right corner and then on the *Add a contact* icon (the one with
the small green-circled *+* sign).

![](/static/img/man_mail/en/roundcube_addressbook_01.png)

Fill in all the fields you deem necessary and then click on *Save*

![](/static/img/man_mail/en/roundcube_addressbook_02.png)

![](/static/img/man_mail/en/roundcube_addressbook_03.png)

![](/static/img/man_mail/en/roundcube_addressbook_04.png)

<a name="rubrica2"></a>

**To send messages to more than one address** click on *Address book* in the top-right corner and select more than one contact while keeping
your **CTRL** key pressed (it's a shortcut for multiple selections!).

Then click on the *New Message* icon and all of your contacts will be there!

![](/static/img/man_mail/en/roundcube_addressbook_05.png)

![](/static/img/man_mail/en/roundcube_addressbook_06.png)

![](/static/img/man_mail/en/roundcube_addressbook_07.png)


<a name="cartelle"></a>

Folders
-------

**If you cannot see the *Sent* folder or the *Draft* folder** first of all check the *Setting* menu in the top-right corner of the page: go
into the *Folders* tab and ensure that their respective checkbox is marked.

![](/static/img/man_mail/en/roundcube_folders_01.png)

![](/static/img/man_mail/en/roundcube_folders_02.png)

If the *Sent* or *Draft* folder do not exist, you can create them by using the *Create* button.
