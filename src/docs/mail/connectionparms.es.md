title: Parámetros de conexión para el servidor de correo 
----

Parámetros de conexión para el servidor de correo
=================================================

Resumen de los parámetros necesarios para configurar un cliente de correo para leer y enviar correo electrónico desde el computador con su
cuenta A/I. (Vamos a suponer que el nombre de cuenta es *usuario@inventati.org*).

<style type="text/css">th {text-align:right;}</style>
<table align="center">
<tr>
<td style="border-right: 1px black solid;">
<table cellspacing="10">
<tr>
<td colspan="2">
#### <u>correo entrante</u>

</td>
</tr>
<tr>
<th>
servidor
</th>
<td>
`mail.autistici.org`
</td>
</tr>
<tr>
<th>
protocolo
</th>
<td>
`POP` o `IMAP`
</td>
</tr>
<tr>
<th>
puerto
</th>
<td>
para POP: `110` (TLS) o `995` (SSL)
 para IMAP: `143` (TLS) o `993` (SSL)
</td>
</tr>
<tr>
<th>
nombre de usuario
</th>
<td>
*usuario@inventati.org*
</td>
</tr>
</table>
</td>
<td valign="top">
<table cellspacing="10">
<tr>
<td colspan="2">
#### <u>correo saliente</u>

</td>
</tr>
<tr>
<th>
servidor
</th>
<td>
`smtp.autistici.org`
</td>
</tr>
<tr>
<th>
puerto
</th>
<td>
`465` (SSL), `587` (TLS)
<tr>
<th>
nombre de usuario
</th>
<td>
*usuario@inventati.org*
</td>
</tr>
</table>
</td>
</tr>
</table>

<a name="POP/IMAP"></a>

POP o IMAP?
-----------

POP3 e IMAP4 son los protocolos estándar para la lectura de su correo electrónico en el Internet: ambos pueden ser utilizados para la
lectura de su buzón de correo A/I, y la elección de uno de ellos significa decidir cómo utilizar su buzón de correo:

- El protocolo POP se ha concebido para una sola cosa: la descarga de nuevos mensajes de una cuenta remota . Mediante el uso de POP, va a
    utilizar su Buzón A/I igual que su buzón de correo físico: cuando usted tiene correo nuevo lo trae y lo mantiene en casa. Este es el
    modo de uso preferido, ya que reduce la carga de los servidores y permite a todo el mundo decidir por sí mismo sobre el grado de
    privacidad que quiere tener con su correo.
- El protocolo IMAP se ha desarrollado con el fin de gestionar un directorio de correo remoto, por ejemplo mediante la creación de
    subdirectorios en los que se pueden organizar los mensajes. Si utiliza IMAP, el correo se mantendrá en el servidor, por lo que el
    espacio que se puede utilizar en el disco del servidor será limitado, pero puede ser una solución si, por ejemplo , que no tiene un
    computador propio o usted desea ser siempre poder leer su correo a través de un correo web.

Estos son a grandes rasgos las dos formas posibles de utilizar el buzón A/I, pero puede ser útil saber que también puede mantener los
mensajes en el servidor durante el uso de POP y que también se puede descargar durante el uso de IMAP.

