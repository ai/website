title: Associating an OpenPGP key with your account
----

# Associating an OpenPGP key with your account

It is possible to associate an OpenPGP (PGP/GPG) key with your
account, so that clients will find it automatically. Discovery uses
the [WKD](https://wiki.gnupg.org/WKD) protocol, which is supported by
Thunderbird and by GPG itself. This protocol is an alternative to PGP
Key Servers, whose usage is now discouraged, and it delegates key
publishing to the email infrastructure.

You can associate a **single** OpenPGP key with your account via the
[via the user panel](/docs/userpanel#pgp). We currently do not support
multiple keys per account.

## OpenPGP identities and aliases

An OpenPGP key can have multiple UIDs in it, corresponding to
different email addresses. The same can be said for your email
account, if you have aliases. In fact, your OpenPGP key might not even
have an identity for your primary email address... to avoid
inadvertently disclosing links between aliases / identities, it can be
useful to understand how OpenPGP keys are mapped to email addresses

When you upload your OpenPGP key, we look at the identities it
contains, and check if each one of them corresponds to either your
primary email address or one of its aliases. If that's the case, the
key is advertised for that address.

Remember that, as with traditional PGP key servers, all the identities
in the same OpenPGP key are discoverable and linkable to each other.

## Expiration

The user panel will show a warning when your OpenPGP key is about to
expire, or is expired already. But besides that, no action will be
taken: the key won't be removed from your account.

To update it, just [upload your key again](/docs/userpanel#pgp) once
you've edited its expiration date.

## Revocation

We don't have an automated way to publish key revocation certificates,
so in order to stop publishing an OpenPGP key that you have revoked,
just [delete it from your account](/docs/userpanel#pgp).

