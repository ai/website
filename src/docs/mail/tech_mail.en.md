title: Technical note on e-mail service
----

Technical notes about e-mail service
========================================

On the user's side, A/I's mail system is totally decentralised: if you want to read and send your e-mail, you can connect to any server of
the network. That's the function of our special addresses `mail.autistici.org` and `smtp.autistici.org`, which "sort" connections among the
servers more or less randomly

According to the [R\* Plan](/who/rplan/), at any given moment your mailbox is materially located on one server of
the A/I network.

That's why your mailbox can happen to be unaccessible due to a temporary connection problem; when this happens, your incoming mail
**won't** get lost, but will stay queued in the other network servers. If the problem is not transitory, your mail account will be moved to
a different server and you will be able to read your new mail very soon (to access your archives you'll have to wait for the affected server
to be restored).

Remember that:
- messages in the trash folder are automatically deleted if older than 7 days
- messages in the spam folder are automatically deleted if older than 30 days
- if you do not read your mailbox for more than 12 months, the mailbox is disabled and its content deleted.


Encryption
-----------

User mailboxes are now encrypted with the same password used for login.
That means we are unable to read the mailbox content, and that if you forget [the password and the security answer]
(docs/mail/passwd) you will loose your mailbox and its content.

Connection parametres
---------------------

The necessary parametres to connect to your mail server are reported [here](/docs/mail/connectionparms).

Aliases
-------

You can create up to 5 mail aliases in your userpanel. Alias are additional email address for your mailbox.
When a message is sent to an alias you setup, this is forwarded into your mailbox.
An alias can be used as from address when you send mail, but you have to configure an additional identity in your mail client,
on in the webmail [webmail](/docs/mail/roundcube) settings.
When you send a message using your alias as sender, your main address will be reported in the hidden headers of the message.

Sending mail
------------

We would like to remind you that sending several mail to large number of recipients is not possible from single user mail accounts.
If you need to do such a thing, please ask us a newsletter!

It's not possible to send messages using a from address different from your mail address or an alias you set up.

Anti-spam filters
-----------------

A/I implements on its servers some mechanisms sorted out in order to reduce incoming spam. Since we cannot possibly configure these filters
for each single user, they are generally very conservative (i.e. they allow the passage of as many messages as possible). Messages we
consider spam are dropped into a "Spam" folder in every user's mailbox. This folder will be cleared every 30 days.

If you check your mailbox via **webmail** or via **IMAP** mail client you will be able to verify that messages in the "Spam" folder are
really what we think they are. And if our software was mistaken you will be able to put the message back into your INBOX.

But if you only check your mailbox via a **POP** mail client you won't be able to see the content of the "Spam" folder. This means you
could lose messages that were not actually spam but only seemed to be ones.

To avoid this misunderstanding you have to choose one of the following solutions: either you routinely check your mailbox via **webmail**
sorting the "Spam" folder out, or you'll have to [configure a filter](/docs/mail/sieve) on your **webmail** that will
sort all your supposedly spam messages back into your INBOX, where you will be able to decide what to do with them.

Due to some of our anti-spam systems, some messages may be delivered with a minutes delay (but remember that with e-mail instant delivering
is never granted: you'd better think about this before worrying about a possible disfunction in A/I's mail services)...
