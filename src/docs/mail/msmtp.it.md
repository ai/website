title: Configurazione di msmtp
----

# Configurazione di msmtp

Questa pagina raccoglie varie indicazioni su come configurare il
programma [msmtp](https://marlam.de/msmtp/) per inviare posta con il
vostro account A/I.

Negli esempi qui sotto, UTENTE@DOMINIO.ORG è il vostro account, e
PASSWORD la relativa password.

Per inviare posta, assicuratevi di aver installato il pacchetto
*ca-certificates*, ed aggiungete le seguenti righe al vostro file
*~/.msmtprc*:

```
account UTENTE@DOMINIO.ORG
port 587
from UTENTE@DOMINIO.ORG
user UTENTE@DOMINIO.ORG
auth on
password PASSWORD
tls on
tls_starttls on
tls_trust_file /ets/ssl/certs/ca-certificates.crt
host smtp.autistici.org
tls_certcheck on
```

Verificate che le vostre impostazioni funzionino inviandovi un
messaggio di test:

```
echo test | msmtp -a UTENTE@DOMINIO.ORG UTENTE@DOMINIO.ORG
```
