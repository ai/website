title: Mutt Configuration
----

# Mutt Configuration

[Mutt](https://www.mutt.org) is a well-known and powerful
console-based email client. This page explains how to configure it to
use your A/I account.

In the examples below, USER@DOMAIN.ORG is your account name (full
email address), and PASSWORD the associated password.


### Reading email (IMAP)

The IMAP protocol allows you to keep email messages on the server,
with a hierarchical folder structure. It is quite practical, but only
advised if you regularly delete old messages (otherwise sooner or
later the server will run out of space).

To use Mutt with your A/I account in this fashion, add the following
to Mutt's *~/.muttrc* configuration file:

```
set folder="imaps://USER@DOMAIN.ORG@mail.autistici.org:993/"
set imap_pass="PASSWORD"
set spoolfile=+INBOX
```

### Reading email (POP)

The POP protocol allows you to download each new message from the
server and store it on your computer instead.

Put this in your *~/.muttrc* file if this is what you want:

```
set folder="pops://USER@DOMAIN.ORG@mail.autistici.org:995/"
set pop_pass="PASSWORD"
set spoolfile=+INBOX
```

If you have multiple email accounts configured in your *muttrc*, you
might have conflicts preventing successful authentication. In that
case try adding:

```
set pop_authenticators="digest-md5:apop:user"
```

### Sending emails with Mutt

Recent versions of Mutt allow sending emails via SMTP directly from
Mutt itself. To do so, add this to your *~/.muttrc*:

```
set from="USER@DOMAIN.ORG"
set use_from=yes
set smtp_url="smtps://USER@DOMAIN.ORG@smtp.autistici.org:465/"
set ssl_force_tls=yes
```

If you're having trouble authenticating successfully, try adding the
following line as well:

```
set smtp_authenticators="login:plain"
```

### Sending emails via msmtp

Older versions of Mutt can only send emails by using an external
program. A common choice for this purpose is [msmtp](/docs/mail/msmtp).

```
set sendmail="/usr/bin/msmtp -a USER@DOMAIN.ORG"
```
