title: Cómo configurar filtros en tu correo web
----

Cómo configurar filtros en tu correo web.
================================================

¿Qué es un filtro?
------------------

Desde julio de 2012 el sistema de correo web de A/I permite a todos los usuarios la creación de filtros.

Un filtro es una regla que permite a un usuario mover, copiar, borrar mensajes en su casilla dependiendo del remitente, asunto, tamaño u
otras categorías que se te ocurran.

Encontrarás una interfaz web para administrar tus filtros personales en tu correo web, luego de iniciar sesión con tu nombre de usuario y
contraseña en nuestra [página principal](/)

¿Cómo puedo agregar un filtro?
------------------------------

![](/static/img/man_mail/en/sieve.png)

1.  Accede a tu correo web.
2.  Ve a la sección "Configuración".
3.  Ve a la sección "Filtros".
4.  Haz click en el ícono "Agregar Filtro".
5.  Elige un nombre sencillo para tu filtro (que dé cuenta del criterio que estés usando para ordenar los mensajes)
6.  En el primer menú desplegable elige el encabezado que estés usando para ordenar los mensajes: remitente, asunto, tamaño, etc.
7.  En el segundo menú desplegable elige qué debería verificarse de ese encabezado: contiene una palabra, es igual a la palabra, etc.
8.  En la casilla que está al lado de los menúes, escribe la palabra que quieras buscar en el encabezado.
9.  Pincha en el botón Agregar.
10. Elige lo que deseas que se haga con los mensajes que estén dentro del criterio del filtro: ser movidos a otra carpeta; borrados;
    reenviados a cierta dirección de correo, etc.
11. Pincha en el botón Guardar.
