title: Note tecniche sulle caselle di posta
----

Note tecniche sulle caselle di posta
===========================================

Il sistema di posta di A/I si presenta all'utente in modo completamente decentralizzato: per leggere e inviare la posta è possibile
connettersi ad uno qualsiasi dei server che appartengono alla rete. Questo è lo scopo degli indirizzi speciali `mail.autistici.org` e
`smtp.autistici.org`, che permettono di "smistare" le connessioni tra i vari server in modo più o meno casuale.

Secondo quanto previsto dal [Piano R\*](/who/rplan/), la tua casella di posta si trova materialmente in un dato momento su uno
solo dei server della rete di A/I.

Per questo è possibile a volte che un problema temporaneo di connettività renda la tua mailbox inaccessibile: quando questo si verifica, la
posta destinata a te **non** va assolutamente persa, ma rimane in coda sugli altri server della rete.

Nel caso che il problema non sia di natura effimera, il tuo account di posta verrà spostato su un server differente, e potrai riprendere a
leggere la nuova posta (mentre per quella vecchia bisognerà attendere il ripristino del server che si è rotto).

Tieni presente che:
- i messaggi spostati nel cestino sono eliminati se più vecchi di 7 giorni
- i messaggi nella cartella dello spam sono eliminati se più vecchi di 30 giorni
- se non accedi alla mailbox per piu' di un anno sarà disattivata e il contenuto eliminato

Crittazione
-----------

Le mailbox degli utenti ora sono crittate con la stessa password usata per il login.
Questo significa che noi non possiamo più in alcun modo leggere il contenuto delle mailbox, e che se dimentichi [la
password e la risposta alla domanda del gatto](docs/mail/passwd) perderai l'accesso alla mailbox e al suo contenuto.


Parametri di connessione
------------------------

I parametri necessari per connettersi ai server di posta sono brevemente ricapitolati in [questa pagina](/docs/mail/connectionparms)


Aliases
-------

Potete creare fino a 5 alias di posta dal vostro pannello utente. Gli alias sono indirizzi aggiuntivi alla vostra mailbox,
quando un messaggio è inviato a un alias che avete impostato, questo viene inoltrato alla vostra mailbox.
Un alias può anche essere utilizzato come mittente per i messaggi che inviate, dovete però impostarlo come
identità nel vostro client di posta, oppure nelle impostazioni della [webmail](/docs/mail/roundcube)
Quando inviate un messaggio usando un alias come mittente, il vostro indirizzo principale comparirà comunque
nelle intestazioni nascoste del messaggio.


Invio della posta
-----------------

Ci preme ricordarvi che da una casella di posta non è possibile mandare grandi quantità di mail a un numero elevato di indiri
avete bisogno di un servizio del genere, chiedeteci di aprirvi una newsletter!

Non è possibile inviare posta usando un mittente che non sia la vostra mailbox o un suo alias.


Filtri anti-spam
----------------

Autistici/Inventati implementa sui propri server alcuni meccanismi pensati per ridurre la ricezione di email spazzatura (*spam*). Dato che
non ci è possibile configurare nel dettaglio questi filtri per ciascun utente, essi sono in genere molto conservativi (cioè lasciano passare
più messaggi possibile).

I messaggi contrassegnati come spam vengono salvati in una cartella "Spam" che viene ripulita automaticamente ogni 30 giorni di tutto il
suo contenuto.

Se consultate la vostra e-mail via via **webmail** o via **IMAP** potrete valutare la correttezza della presenza di un messaggio nella
cartella "Spam" direttamente.

Viceversa se un utente consulta la propria posta solo attraverso un client e il protocollo **POP** non potrà visualizzare la cartella
"Spam" e il suo eventuale contenuto. Questo aumenta il rischio che messaggi erroneamente catalogati come SPAM vengano cancellati perché
abbandonati nella cartella "Spam".

Per evitare questo spiacevole inconveniente ci sono solo due soluzioni: consultare di tanto in tanto via **webmail** la vostra casella di
posta e risistemare adeguatamente il contenuto della cartella "Spam"; [configurare un filtro](/docs/mail/sieve) dalla vostra
**webmail** che sposti tutti i messaggi della cartella "Spam" nella vostra "Posta in Arrivo" da cui poi potrete valutare autonomamente la
rilevanza dei messaggi.

Come conseguenza di alcuni dei sistemi antispam utilizzati, è possibile che a volte la consegna dei messaggi sia ritardata di qualche minuto
(ma è comunque bene ricordare che la posta elettronica su Internet non garantisce tempi di recapito istantanei: è bene averlo presente prima
di preoccuparsi di un'eventuale disfunzione dei servizi di posta di A/I)...
