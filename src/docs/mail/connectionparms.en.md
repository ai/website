title: Connection parameters for our mail server
----

Connection parameters for our mail server
=========================================

Summary of the necessary parameters for configuring a mail client and reading and sending mail from your computer with your A/I account.
Let's assume that your account name is *user@inventati.org*:

## Incoming mail

<table>
<tbody>

<tr>
<th>
Server
</th>
<td>
mail.autistici.org
</td>
</tr>

<tr>
<th>
Protocol
</th>
<td>
POP3 or IMAP4
</td>
</tr>

<tr>
<th>
Port
</th>
<td>
for POP3: 110 (STARTTLS) or 995 (SSL)<br>
for IMAP4: 143 (STARTTLS) or 993 (SSL)
</td>
</tr>

<tr>
<th>
Username
</th>
<td>
<i>user@inventati.org</i>
</td>
</tr>

</tbody>
</table>


## Outgoing mail

<table>
<tbody>

<tr>
<th>
Server
</th>
<td>
smtp.autistici.org
</td>
</tr>

<tr>
<th>
Port
</th>
<td>
587 (STARTTLS) or 465 (SSL)
</td>
</tr>

<tr>
<th>
Username
</th>
<td>
<i>user@inventati.org</i>
</td>
</tr>

</tbody>
</table>

<a name="POP/IMAP"></a>

# POP or IMAP?

POP3 and IMAP4 are the standard protocols for reading your mail in the Internet: they can both be used for reading your A/I mailbox, and
choosing one of them means deciding how to use your mailbox:

The POP protocol has been conceived for just one thing: downloading new messages from a remote account. By using POP, you'll use your
A/I mailbox just as your physical mailbox: when you have new mail you fetch it and keep it at home. This is the usage mode we prefer,
because it reduces the load on our servers and allows everybody to decide on her own about the degree of privacy she wants to keep with
her mail.

The IMAP protocol has been developed so as to manage a remote mail directory, for instance by creating subdirectories in which you can
arrange your messages. If you use IMAP, your mail will be kept in the server, so the space you can use in the server disk will be
limited, but it can be a solution if, for example, you don't have a computer of your own or you wish to be always able to read your mail
through a webmail.

These are broadly the two possible ways for using your A/I mailbox, but it can be useful to know that you can also keep your messages in the
server while using POP and that you can also download them while using IMAP.


## my mail client does not support SSL or is obsolete

Obsolete mail clients could not support latest SSL version, so we recommend
you to upgrade to a modern client.
