title: Strumenti per il tuo blog
----

Strumenti per il tuo blog
====================

I blog devono molto allo sviluppo di strumenti per la pubblicazione dinamica che semplificano enormemente le procedure di aggiornamento dei
siti web.

Un agile strumento per un blog deve fornire un'interfaccia "amichevole" (di facile accesso attraverso un web browser) e gestire in maniera
dinamica i propri contenuti, attraverso archivi e operazioni di ricerca.

Un blog ha due indirizzi Internet che non vengono modificati dopo che il blog è stato impostato

- il suo indirizzo di accesso pubblico.
- il suo indirizzo di gestione, protetto da una password nota a chi amministra il blog.

Si può aprire un blog sia partecipando a una comunià di blog, sia usando le funzionalità per blog del proprio server.

COMUNITÀ BLOG
-------------

Per aprire un blog in una comunità blog già esistente, come quella di A/I, servono di solito pochi minuti. Si sceglie un identificativo e
una password e in pochi click del mouse il blog è pronto.

Tra i difetti ci sono spesso il numero limitato delle opzioni di layout e l'assenza di caratteristiche troppo sofisticate, oltre ai
frequenti avvisi pubblicitari e al rischio (scongiuri) che tutta la comunità venga chiusa.

USARE GLI STRUMENTI PER I BLOG
------------------------------

Gli strumenti per i blog sono programmi che si installano su un server e usano script per gestire automaticamente il sito, insieme a un
database per memorizzare il materiale postato. Una volta installato, un programma del genere opera attraverso un semplice browser. Non sono
richieste capacità particolari per impostare e gestire un blog (come l'uso dell'HTML), ma a volte l'installazione e la configurazione è
complicaa (impostare i criteri di accesso, creare un database e sistemare le procedure di caricamento file attraverso FTP).

È una soluzione per persone che già hanno familiarità con i blog e ha il vantaggio che si può adattare, configurare e modificare come si
preferisce

Ma richiede alcune conoscenze tecniche, è anche pù esposta a commenti indesiderti e impone di immagazzinare manualmente i contenuti.

PERCHÉ UN BLOG SU AUTISTICI/INVENTATI?
--------------------------------------

- PRIVACY - perché I nostri server conservano esclusivamente i log strettamente necessari a operazioni di debugging, che comunque non sono
    associabili in alcun modo ai dati identificativi degli utenti dei nostri servizi.
     Per questo chiediamo a chi apre un blog di utilizzare una mail di uno qualsiasi dei [vari server autogestiti](/links) (e con
    policy equivalenti per quanto riguarda i log, in giro nel mondo).
     Con questo vincolo, siamo sicuri di non poter ricondurre (noi) un blog a nessuna persona fisica, e quindi di poter dire che il nostro
    servizio difende l'anonimato per quanto possibile (se scrivete il vostro indirizzo di casa nel blog o se qualcuno ha molto interesse a
    scoprire chi siete, tenete presente che la tecnologia alla fine, e nonostante tutto, non è nostra amica).
- NON COMMERCIALE - perché non facciamo profili dei nostri utenti da rivendere alle società di marketing; questo però significa che
    NoBlogs è un servizio libero, ma non che è gratuito.
     Non è gratuito quanto meno per noi, perché anche se non chiediamo niente in cambio per i servizi che offriamo, dobbiamo comunque
    sostenere delle spese, consistenti, per mantenere i server e per la banda.
     Speriamo però che chi sceglie di aprire un blog sulle nostre macchine capisca che non sta approfittando di un servizio gratuito, che
    abbia fatto questa scelta perché preferisce agire in un certo modo, autogestito e al di fuori di interessi commerciali e beghe di
    potere.
     Se è così, siamo sicuri che chi usa NoBlogs si preoccuperà della sopravvivenza di questo luogo e si ricorderà che per farlo continuare
    a vivere abbiamo bisogno di qualche donazione. Le coordinate le trovate [qui](/donate).
- COMUNITÀ - perché chi forma la comunità di NoBlogs aderisce ai principi di antirazzismo, antisessismo, antifascismo e antimilitarismo
    della nostra policy, e perché speriamo che da questa base di partenza questo servizio venga usato non tanto per scrivere il proprio
    diario (anche, ci mancherebbe), ma soprattutto per creare una comunità che produca e scambi contenuti, contribuendo a uno spazio di
    informazione, comunicazione, relazione e iniziativa politica indipendente.


