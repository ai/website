title: Suggerimenti per far notare meglio il tuo blog
----

Suggerimenti per far notare meglio il tuo blog
==============================================

Qualche consiglio per farsi indicizzare dai motori di ricerca
-------------------------------------------------------------

I blog sono siti web in tutto e per tutto e quindi sono indicizzati dai vari motori di ricerca, come Google, Yahoo! Search e MSN Search. Per
raggiungere il suo scopo, un blog deve ottenere una buona visibilità nelle pagine dei risultati dei motori di ricerca passando dalle
principali parole chiave. Per questo, un sito dev'essere progettato fin dall'inizio in modo da reagire ai criteri di classificazione
automatica impiegati da quei motori.

I blog sono dotati di varie caratteristiche intrinseche che facilitano la loro indicizzazione da parte dei motori di ricerca e ne
favoriscono un rating adeguato e il posizionamento tra i primi posti delle pagine dei risultati.

Ecco alcune di queste caratteristiche:

- In genere contengono molto testo, che contribuisce all'indicizzazione. I motori di ricerca non indicizzano facilmente i siti pieni di
    grafica e di animazioni Flash ma poveri di testo.
- Ogni "post" occupa in genere un'unica pagina, accessibile attraverso un "permalink" e incentrata su un unico argomento: i motori di
    ricerca indicizzano molto più questo genere di pagine che le pagine lunghe dedicate a molti argomenti diversi (come gli archivi o le
    home page dei blog).
- Il titolo dei post viene generalmente riprodotto nello *heading* (la parte iniziale del codice HTML della pagina) o nell'URL
    (l'indirizzo).
     Per esempio, sul blog Operai sociali, all'indirizzo http://operaisociali.noblogs.org/, ogni post si trova in una pagina a sé stante
    come nel caso di <http://operaisociali.noblogs.org/post/2006/10/12/LA-LUNGA-NOTTE-DEGLI-OPERAI-SOCIALI>
     Il titolo del post ("LA LUNGA NOTTE DEGLI OPERAI SOCIALI") non si ripete solo nell'URL della pagina, ma anche nello *heading* del
    documento.
     Quindi il titolo del post viene aggiunto dopo il nome del blog, che compare da solo soltanto sulla home page del
    blog (http://operaisociali.noblogs.org).
- Le parole chiave descrittive contenute nella parte iniziale del codice (il contenuto della tag &lt;TITLE&gt; nel codice HTML) e negli
    URL di questi documenti sono criteri cruciali per i motori di ricerca e quindi per fare indicizzare un post è fondamentale attribuirgli
    un titolo selezionato con cura.
- I link alle altre pagine del blog, soprattutto agli archivi, sono tutti testuali e vengono creati automaticamente (v. esempio nella
    colonna di destra del blog Radio Free Nepal).
     Questo è un elemento molto favorevole all'indicizzazione, in quanto il contenuto testuale dei link è cruciale per il peso attribuito
    dai motori di ricerca alle pagine a cui puntano quei link.
     Pertanto, tornando al nostro esempio, la presenza delle parole “State Vandalism in Nepal” nel primo link o di “Radio Free Nepal” nel
    nono accentua il peso della pagina indicata dal link relativamente alla ricerca di questi termini.
     Inoltre, la pagina contenente questi link (il testo cliccabile viene considerato importante dai motori di ricerca) e la pagina cui i
    link puntano saranno ritenute rilevanti.

COME FARSI INDICIZZARE MEGLIO UN BLOG
-------------------------------------

I blog presentano alcuni elementi intrinseci che ne aumentano la possibilità di essere indicizzati. Una volta che un motore di ricerca ha
"scoperto" un blog, vuoi perché gli è stato segnalato manualmente, vuoi grazie agli "spider" che il motore usa per seguire i link, grazie ad
alcune caratteristiche tipiche, quel blog avrà molte più possibilità di un sito web classico di figurare tra i primi posti. Nondimeno,
conviene sforzarsi di accentuare ancora un po' di più la propria visibilità.

Ecco alcuni consigli su come fare, utilizzando le parole chiavi più importanti inerenti all'argomento del blog.

### 1. Tenete presente gli espedienti tecnologici che contribuiscono all'indicizzazione.

- Il titolo del post dev'essere completamente riprodotto nello *heading* della pagina (il tag "TITLE") e nel suo URL (cosa che non
    succede sempre, visto che nell'indirizzo alcuni software troncano il titolo dopo un certo numero di caratteri, quindi conviene scrivere
    titoli brevi).
- È sempre meglio creare dei “permalink” (link a una pagina contenente un unico post).
     Acquisite quante più capacità tecniche è possibile in modo da poter ricorrere alla massima quantità possibile di fattori che
    favoriscono l'indicizzazione.
     Per verificare questi punti, date un'occhiata ai blog basati sulla tecnologia che state prendendo in considerazione (ad esempio nella
    comunità NoBlogs) e studiatene l'aspetto e l'organizzazione: in questo modo si imparano un sacco di cose.

### 2. Scegliete il titolo migliore per i vostri post

Questo dettaglio è molto importante: il titolo del vostro post sarà riprodotto nello *heading* della pagina del post, nel suo URL e negli
elenchi di link in cui è citato: insomma, in tre punti fondamentali per i motori di ricerca. Pertanto, i titoli dei post devono contenere,
in poche parole, i termini più importanti per l'indicizzazione. Evitate titoli quali "Hai ragione!" "Benvenuto!" o "Fantastico!": il titolo
dovrebbe descrivere o riassumere in meno di cinque parole il contenuto del post cui fa riferimento. Pensate alle parole per cui vi
piacerebbe che un motore di ricerca facesse comparire il vostro post e inseritele nel titolo. Non sarà facile, ma è di certo molto efficace.

### 3. Date peso al testo

I motori di ricerca adorano il testo, quindi dateglielo. Potete pubblicare tutte le foto che volete, a condizione che siano associate a un
testo. Provate a scrivere post di almeno 200 parole in modo da aumentare la possibilità che vengano individuati dai motori di ricerca.
Inoltre, evitate di affrontare troppi argomenti nello stesso post, perché ai motori di ricerca cose del genere risultano sgradite. La regola
aurea è un argomento, un post.

### 4. Considerate bene il primo paragrafo dei vostri post

Un fattore cruciale è anche la posizione delle parole più importanti all'interno del testo: fate sempre molta attenzione al primo paragrafo.
Se ad esempio volete essere indicizzati per le parole "ostaggi liberati", inseritele tra le prime 50 parole dell'articolo. Lo stesso vale
per qualunque parola chiave scegliate.

Una pagina che contenga le parole chiave all'inizio otterrà sempre risultati migliori sui motori di ricerca di una pagina che le contenga
alla fine (a parità di contenuto). Date peso a quelle parole mettendole in grassetto, ad esempio: in questo modo il motore di ricerca capirà
che sono importanti.

### 5. Evitate di scrivere post dal contenuto molto simile

Tutti i motori di ricerca hanno modo di individuare i contenuti doppi, e se due pagine sono troppo simili, solo una delle due sarà
ricordata, mentre l'altra comparirà solo di rado nelle pagine dei risultati.

Google, ad esempio, mostra il messaggio:
 "Al fine di visualizzare i risultati più rilevanti, sono state omesse alcune voci molto simili a quelle già visualizzate. In alternativa, è
possibile ripetere la ricerca includendo i risultati omessi".

Con i blog questo risultato si ottiene spesso, in quanto le pagine contenenti i singoli post possono sembrare molto simili.

Per esempio, se avete un'introduzione identica in ogni pagina, converrà che la mettiate alla fine o che la lasciate soltanto nella home
page, perché in questo modo le altre pagine non sembreranno troppo simili l'una all'altra.

### 6. Non date un titolo troppo lungo al vostro blog

Per i motori di ricerca, il titolo (contenuto anche nella tag "TITLE") è migliore se composto da 5-10 parole, senza contare articoli,
congiunzioni e particelle varie. In genere il titolo della pagina di un blog si divide in due parti:

- Il titolo generale del blog
- Il titolo del post

Per non superare le 10 parole nel titolo delle pagine contenenti post, conviene limitarsi a un massimo di cinque parole sia per il titolo
generale che per i titoli dei post. Non sono molte, è vero, ma l'insieme di concisione e informazione è cruciale per farsi indicizzare
facilmente dai motori di ricerca.

### 7. Mettete in evidenza i feed RSS del vostro blog \[per smanettoni\]

Per "thread XML" o "feed RSS" si intende uno strumento grazie al quale i vostri lettori possono ricevere una notifica ogni volta che
pubblicate un post. Il software utilizzato da NoBlogs lo crea automaticamente, e modificando il template potrete mettere l'elenco dei feed
più in evidenza per renderne più agevole l'uso.

### 8. Diffondete i vostri link

I link sono molto importanti per i motori di ricerca, in quanto gli permettono di compilare una classifica di gradimento (che Google chiama
PageRank) delle pagine web. Per aumentare i link diretti al vostro blog:

- Inseritelo nelle directory (v. oltre)
- Cercate siti "affini" che offrono materiali sullo stesso argomento. Lo scambio di link tra blog della stessa sfera d'interesse dovrebbe
    essere una priorità (oltre a essere un'abitudine diffusa e approvata dalla comunità dei blogger, che è meglio). I blog sono adattissimi
    a inserire elenchi di link, in quanto i margini laterali sono spesso vuoti e riempirli di contenuti è facile.

COMPARIRE NELLE DIRECTORY TEMATICHE
-----------------------------------

Comparire nei motori di ricerca d'interesse generale (come Google, MSN, Yahoo! ed Exalead) e nelle directory (es. Yahoo! Directory e Open
Directory) è molto importante, ma anche comparire nelle directory tematiche lo è, in quanto:

- si attraggono più visitatori interessati al particolare argomento;
- si aumenta la quantità dei link al vostro blog, cosa favorevole alla notorietà del sito;
- ci si fa conoscere da altri blogger che potrebbero essere interessati a scambiarsi link con siti affini.

Tra i vari strumenti di ricerca (motori di ricerca e directory) che indicizzano i blog, vi sono:

in inglese

- Blogwise : http://www.blogwise.com/
- Daypop : http://www.daypop.com/
- Feedster : http://www.feedster.com/
- Technorati : http://www.technorati.com/
- Waypath : http://www.waypath.com/
- Blogarama : http://www.blogarama.com/
- Syndic8 : http://www.syndic8.com/

in francese

- Blogonautes http://www.blogonautes.com/
- Blogolist http://www.blogolist.com/
- Weblogues http://www.weblogues.com/
- Blogarea http://www.blogarea.net/Links/
- Pointblog http://www.pointblog.com/
- Les Pages Joueb http://pages.joueb.com/

Per un elenco più completo: http://search-engines.blogs.com/mon\_weblog/2005/05/les\_search-engines\_de\_.html

Date anche un'occhiata alle directory dei singoli provider:

- http://www.canalblog.com/cf/browseBlogs.cfm
- http://www.dotclear.net/users.html
- http://www.blogspirit.com/fr/communautes\_blogspirit.html

CONCLUSIONI
-----------

I blog possono vantare tutte le caratteristiche che favoriscono l'indicizzazione da parte dei motori di ricerca. Con i consigli forniti in
questo articolo, dovreste ottenere ottimi risultati e accentuare la visibilità del vostro blog. Quindi non dovete fare altro che iniziare, e
ricordatevi che il contenuto fa la parte del leone.
