title: Come si usano i newsgroup
----

Come si usano i newsgroup
=================

I news server sono uno dei servizi "storici" della rete, usati per comunicare e/o scambiarsi files in aree tematiche chiamate "gruppi".

Per poter accedere a questo servizio vi serve un programma per accedere ai newsgroup. Per windows e linux ci sono Thunderbird e Sylpheed che
sono anche programmi di posta, per linux ci sono anche pan, knode, slrn etc.

I parametri da impostare sono:

> Nome del server: news.autistici.org
> Porta: 119  (o 563 per SSL)

Dopodiche' dovete scaricare la lista dei newsgroup presenti sul server e scegliere a quali "iscrivervi" ("subscribe") in modo da scaricare
tutti i messaggi di questi newsgroup quando vi collegate.

Per collegarvi con una connessione criptata vi serve un client che supporti questa feature (ormai presente in molti programmi).

Non serve l'autenticazione, l'accesso in lettura/scrittura e' consentito a tutti.


