title: Documentazione per gli admin delle liste
----


Per la gestione delle mailing list vedi:

- [Mailing List](/docs/mailinglist)



Per la gestione delle newsletter vedi:

- [Newsletter](/docs/newsletter)
