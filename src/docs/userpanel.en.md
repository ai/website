title: User Panel Howto
----

User Panel Howto
================

To use any of the [services](/services/) provided by A/I, you have to own a A/I account and thus be able to access your
**User Panel** where you can find any information about your services and how to handle them.

This manual wishes to detail what you can do within your **User Panel**

<a name="topsummary"></a>

Summary
-------

- [Login](#login)
- [Things to do after the first login](#firstlogin)
  1.  [Set password recovery question](#recoverquestion)
  2.  [Change password](#passwordchange)
- [Main page](#main)
- [How to change the User Panel language](#languagechange)
- [How to read a mailbox and how to change its settings form the User Panel](#mailbox)
  1.  [How to add or remove an alias](#alias)
  2.  [How to add your PGP key](#pgp)
- [How to manage a website from the User Panel](#website)
- [How to manage an WebDAV account from the User Panel (and how to change WebDAV account password)](#webdav)
- [How to manage a mysql database from the User Panel](#mysql)
- [How to manage mailing lists from the User Panel](#mailinglist)
- [How to close your email account](#close)

<a name="login"></a>

Login
-----

As you can easily see from the image, to login into your **User Panel** you have to click on the big red button in the [homepage](/), fill
in your account (the full mailbox address) and your password. Then click on *login*.

![](/static/img/man_userpanel/en/login_page01.png "User Panel Login Page")

If that button does not work, you can access the **User Panel** login page [directly](https://accounts.autistici.org) and fill in your account (the full
mailbox address) and your password. Then click *login*.

![](/static/img/man_userpanel/en/login_page02.png "User Panel Login Page")

<a name="firstlogin"></a>

# Things to do after the first login

<a name="recoverquestion"></a>

## Recover question

If it's the first time you login into your **User Panel** you will be redirected on a page to set a recovery question/answer:
if you happen to forget or lose your password, this question and its answer will be the only ways to recover it and be able to login to your panel
again.

![](/static/img/man_userpanel/en/recovery_set.png "Set password recovery question")

You have to fill your actual password, a recovery question and the answer, then click on *Update*

**Warning**: you have to be sure to remember the exact answer to the question that you typed (including Capital letters or symbols or
numbers) since providing the correct answer will be the only way to recover your password.

Please keep in mind that **we do not know the answer you provided** since it's kept encrypted in our database. You are the only person
knowing what to answer and thus being able to recover your lost or forgot password.

Once you have clicked on the *Update* button you will be automatically redirected back to your **User Panel**.

<a name="passwordchange"></a>

## Change your password

The second thing you need to do right away after you first login into your **User Panel** is changing your password.

To change your password, click on **Account management**

![](/static/img/man_userpanel/en/main_management.png "Enter account management")

and then on **Change Password**

![](/static/img/man_userpanel/en/main_management.png "Account management settings")

Once you click on the *Change password* link you will be directed to a form where you can fill in **twice** your new password. Once you'll
have done so and clicked on the *Update* button, you'll have a new password to login into your **User Panel** AND **to login into your
mailbox**.

![](/static/img/man_userpanel/en/change_password01.png "Change your password")

**Warning**: here you are changing your main account password. If you wish to change your WebDAV password or Mysql password, you'll have to
look into [other parts of this manual](#topsummary).

Once you have clicked on the *Update* button you will be automatically redirected back to your **User Panel**.

<a name="main"></a>

User Panel Main Page
--------------------

![](/static/img/man_userpanel/en/main.png "Your User Panel Main page")

What you can see in the picture above is a very basic version of how your **User Panel** could look like. It's the **User Panel** of someone
owning a simple mailbox on our servers. No additional addresses, no website, no list, no newsletter, no nothing. More or less like
the 50% of our users, so don't feel ashamed or anything.

Now let's try to make it a bit more cheerful and to make sense of it.

![](/static/img/man_userpanel/en/main_boxes.png "Your User Panel Main page areas")

You can split up the **User Panel** in different areas: some of them are really crowded but the colors should help you make out which is
which.

-   In the **Yellow box** at the bottom of the page there's a link to change the language your panel uses. By default it's set to the language
    your browsers shows to understand, but it can be wrong.
-   In the **Red box** on the right there's an **Account Management** black button, which open a page where you can set some parameters such as:
    change your password, update the recovery question/answer, set the 2FA and close the account.
-   In the central part of the **User Panel** main page, squatted by a **Green box**, are listed all your services on our platform.
-   And in the top right, squatted by a **Blue box** there's a link to **logout** of the **User Panel**. If you did not move on to
    your mailbox, remember to click on it before leaving the panel.

<a name="languagechange"></a>

How to change the User Panel language
--------------------------------------------------

If you want to change the language of your **User Panel** simply click on your desired language (see red shape in the image below).
Once you click on the button the page will reload in the chosen language and a light green alert will inform you that your
**User Panel** language has been changed.

![](/static/img/man_userpanel/en/language.png "change your user panel language")

<a name="mailbox"></a>

How to read mailbox and to change its settings
----------------------------------------------

We have already explained [how to change your mailbox password](#passwordchange) and [how to set your password recovery question](#recoverquestion).
Now it's time to move on and to login into your mailbox to read some e-mails: click on the *mail icon* or on
your *mail address* in the account management area. You will be redirected automatically to our webmail: you can find a
[useful howto about it in our support page](/docs/mail/roundcube). Remember that once you logout of the webmail, you'll be also
automatically logged out of the **User Panel**.

![](/static/img/man_userpanel/en/mailbox_read.png "click to read your mailbox")

If you want to know how much space are you using up, it's reported next to your mail address, see image below.
We would like to remind you that we do not set a maximum amount of space for your mailbox, but remember that our
resources are not endless and that you share our servers with thousands of other people.
Be careful how much space you use up.

![](/static/img/man_userpanel/en/mailbox_space.png "read how much space you use up on our servers")

<a name="alias"></a>

## How to add or remove an alias

First of all: what is an **alias**?
 An **alias** is an alternative mail address that you can give around to receive e-mails on your account. You can also configure it as
sender in your mail client to send e-mails. But it will always be connected to the same account: that is your mailbox account will be able
to send and receive e-mails on both addresses.

On A/I servers you can have and manage at most 5 aliases for any single account you have. If you need more, please [contact us](/get_help).
 If you want to create an **alias** for your mailbox, click on the link next to your mailbox account.

![](/static/img/man_userpanel/en/mailbox_createalias.png "Click to create an alias")

Once you click you will be directed to a form where you can fill the alternative address you wish for and click on the *Create* button.

![](/static/img/man_userpanel/en/mailbox_createaliasform.png "Click to create an alias")

Now your mailbox management area changed a bit and you can find your **alias** next to your mailbox address.
If you already have an alias and want to remove it, you simply have to click on the *trash* sign next to the address you wish to delete.
(see red arrow in the next image)

![](/static/img/man_userpanel/en/mailbox_deletealias.png "Click here to delete an alias")


<a name="pgp"></a>

Howto add a PGP public key to your user panel
---------------------------------------------

You can add your PGP public key to your user panel so that it is
available to clients supporting the [WKD
protocol](https://wiki.gnupg.org/WKD).

Clicking on the "OpenPGP" button next your email address:

![](/static/img/man_userpanel/it/pgp01.png "Click to add a PGP key")

a form will open where you can paste your PGP public key in ascii format.

![](/static/img/man_userpanel/it/pgp02.png "Paste your PGP key")

Click on **Upload Key**

some checks will be performed to ensure that your email address is an
ID of your key, if everything is ok the key will be saved.

![](/static/img/man_userpanel/it/pgp03.png "Verify that all is ok")

you could quickly check if the key is available, e.g. if you are using
linux, with the command:

```
/usr/lib/gnupg/gpg-wks-client --verbose --check your_mail@autistici.org
```


<a name="website"></a>

How to manage a website from the User Panel
-------------------------------------------

If you requested to A/I something more than a simple mailbox address then your **User Panel** will be somewhat more complicated.
If you are managing some website on A/I servers, your account management area will be enriched by a new section.

![](/static/img/man_userpanel/en/main2.png "check your website in the user panel")

As you can see managing a website includes three different kind of submenu: one dedicated to your **website** proper; another one to the
**WebDAV account** you can use to update your website; and another one to the **databases** connected to a site. Each of this submenu holds
specific configuration possibilities.

![](/static/img/man_userpanel/en/website.png "How many things you have to manage for a website to work!")

If you click on the *wheel icon* a dropdown menu will appear with some items:

-   *View site* will open your website
-   *Details* will show you some details and security alerts about installed CMS/software
-   *Analytics* opens a page where you can find the code you need to add to your site to use A/I web statistics tool. See [A/I stats Howto](/docs/web/webstats)
    for more information.

![](/static/img/man_userpanel/en/website_stats.png "copy and paste the code for having web statistics on your site")


<a name="webdav"></a>

How to manage an WebDAV account from the User Panel (and how to change WebDAV account password)
-----------------------------------------------------------------------------------------------

To update your site you have been given a username and a password (different from the one you use to login into the **User Panel** and
mailbox account).

Using those credentials through a WebDAV client as detailed in [A/I WebDAV Howto](/docs/web/webdav) will allow you
to update your website.

Next to the **dav** sub-section there's a *wheel icon*, clicking on it you can change your dav password.

![](/static/img/man_userpanel/en/dav.png "Manage your WebDAV account")

before the *wheel icon* there's a number reporting the disk space used by your website.


<a name="mysql"></a>

How to manage a mysql database from the User Panel (and how to reset your database password)
--------------------------------------------------------------------------------------------

![](/static/img/man_userpanel/en/mysql.png "Manage your Mysql database")

If you are using a MySQL database to run your website, this tab of your website management area show the database name.
To change the password write to [helpdesk](mailto:help@autistici.org)

<a name="mailinglist"></a>

How to manage a mailing list from the User Panel
------------------------------------------------

If you are a mailing list administrator you will find your mailing lists in your **User Panel**.

![](/static/img/man_userpanel/en/list.png "Manage your Lists")

Clicking on the *wheel icon* will open a dropdown menu with some items:
-   *Admin* to be redirected to the list admin page
-   *Reset password*  to get a new list management password
-   *View archives*  a link to list archives (if enabled)


<a name="close"></a>

How to close your mail account
-------------------------------------------------------------

Clicking on **Account management** and then the red button **Disable Account** a page will open:

![](/static/img/man_userpanel/en/close.png "Close your account")

Clicking on *Disable this account* your mailbox will be disabled.

That means:

no messages will be received/sent anymore (including the aliases)

in a couple of days all messages in the mailbox will be deleted

if you have set up an addressbook in the webmail, that will remain in our backups.

your email and the aliases will be kept in our user management system so that no one will be able to request
the same addresses (to avoid that someone will receive messages addressed to you).

The data we will keep are:

* the mail email address and the aliases you set
* the mailbox creation date
* the question (not encrypted) and the security answer (encrypted)
* the mailbox password (encrypted)
* the OTP codes, if set (encrypted)

If you need a complete removal write to [helpdesk](mailto:helpdesk@autistici.org)

