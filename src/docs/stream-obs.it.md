title: Streaming con OBS Studio
----

Streaming con OBS Studio
=========================

Per avviare uno streaming su [live.autistici.org](https://live.autistici.org), ti consigliamo di usare [OBS Studio](https://obsproject.com) seguendo queste istruzioni per il collegamento:

1. Clicca "File" -> "Impostazioni".

2. Seleziona la tab "Dirette" e usa la seguente configurazione:

    - Servizio: Personalizzato...
    - Server: rtmp://live.autistici.org/ingest
    - Codice delle dirette: il nome che vuoi dare al tuo stream.

        Per esempio, se vuoi che il tuo stream sia accessibile all'indirizzo "https://live.autistici.org/#unprogrammafantastico" il tuo codice delle dirette sarà "unprogrammafantastico".

    ![](/static/img/OBS-dirette.png "Setting OBS")

3. Clicca "OK".

4. A questo punto potrai avviare la diretta cliccando il pulsante "Avvia la diretta" e il tuo stream sarà visibile all'indirizzo prescelto dopo pochi secondi.

    ![](/static/img/OBS-avviadiretta.png "Avvia streaming")

5. Se vuoi controllare i collegamenti in corso, puoi andare all'indirizzo 
[https://live.autistici.org/stats](https://live.autistici.org/stats)

Una guida (in inglese) su come usare OBS Studio si può trovare [nel wiki ufficiale di OBS](https://obsproject.com/wiki/OBS-Studio-Overview).
