title: IRC Chatting Howto
----

IRC Chatting Howto
==========

Si estás interesado en chatear con IRC pero no lo tienes claro, puedes empezar con la aplicación web de
[Indivia](https://webirc.indivia.net/cgiirc/irc.cgi)
Si encuentras interesante el chat IRC, puedes probar algunos
clientes más sofisticados como [Xchat](/docs/irc/xchat) y
[Pidgin](/docs/irc/pidgin).

La información básica que necesitas para chatear es la siguiente:

- [Parámetros de configuración](#parm_irc);
- el canal temático en el que desea chatear (los nombres de los canales comienzan con el símbolo \#, por ejemplo: \#indymedia o \#etc).
    Puedes elegir el canal que prefieras. El canal más popular en irc.autistici.org es \#hackit99, pero la red acoge también el canal de
    Indymedia Italia, \#indymedia. Para encontrar a los miembros del Colectivo en nuestro canal y hacer todas esas preguntas que jamás
    responderemos podéis visitar el canal \#ai.
- Tu nick, que es el nombre que quieres utilizar durante la conversación (puedes usar tu nombre real o el apodo que quieras). Si quieres
    estar seguro de que tu nombre no es utilizado por otra persona, **regístralo** (si quieres saber más acerca de este y otros servicios,
    lee este [manual](/docs/irc/irc_services)).

Los principales comandos que necesitarás en IRC – que están contenidos en los menús específicos de los clientes gráficos como
[Xchat](/docs/irc/xchat) – son los siguientes:

- /nick seguido por el nick que quieres utilizar – para cambiar tu nick;
- /join seguido del nombre del canal al que quieres acceder – para acceder al canal (ej.: /join \#hackit99);
- /part seguido por el nombre del canal – para cerrar sesión en ese canal;
- /quit para salir de IRC;
- /msg seguido del nick de otra persona – para comenzar una conversación privada (llamado "mensaje privado" o 'query') con ella.

Por supuesto hay muchos más comandos: si quieres obtener más información, podrás encontrar varios tutoriales en la web.

El servidor IRC de A/I ofrece algunos servicios adicionales denominados "Servicios de IRC”. Para aprender más sobre ellos, consulta este
[manual](/docs/irc/irc_services).

<a name="parm_irc"></a>

Información Técnica
------------------------------------------

<table>
<tbody>
<tr class="odd">
<td align="left">Dirección de servidor</td>
<td align="left">ai.irc.mufhd0.net</td>
</tr>
<tr class="odd">
<td align="left">Puerto usuario SSL</td>
<td align="left">9999 o 6697</td>
</tr>
</tbody>
</table>

También se puede conectar con el servidor a través del puerto 9999 con cifrado SSL.

Para la configuración de los distintos clientes, lee los tutoriales sobre [X-Chat](/docs/irc/xchat),
[Pidgin](/docs/irc/pidgin) (para Windows, Mac y Linux), [Irssi](/docs/irc/irssi) (línea de comandos
de Linux).

Dado que la red mufhd0 se gestiona conjuntamente con [ECN](http://www.ecn.org) y [Indivia](http://www.indivia.net), si los servidores de
A/I tuvieran problemas os podéis conectar a través de estos otros servidores.

Contactos sobre IRC
-------------------

Si tienes cualquier problema o duda acerca de nuestro servicio de IRC y/o sobre la posibilidad de vincular otros servidores, escribe a:
<irc@autistici.org>.
