title: IRC Services Howto
----

IRC Services Howto
==================

## Indice

- [introduzione](#intro)
- [come si usano i services](#comesiusa)
- [come si utilizza ChanServ](#chanserv)
- [come si gestiscono i takeover](#takeover)

- - -

<a name="intro"></a>

## Introduzione

I Services sono delle applicazioni che girano sul server IRC e permettono di gestire nickname e canali in modo chiaro ed efficace. I
Services attualmente disponibili sono:

- NickServ: È un gestore di nickname che gli utenti possono usare per proteggersi contro gli abusi
    di nickname. Ogni utente puo' avere il suo gruppo di nick e puo' cambiare la sua configurazione riflettendo i cambiamenti su tutto il
    gruppo di nick.
- ChanServ: È un gestore di canali che aiuta gli utenti ad amministrare i propri canali in un modo
    altamente personalizzabile. ChanServ mantiene una lista di utenti privilegiati e bannati sui singoli canali che permette di controllarne
    gli accessi. Inoltre elimina tutti i problemi di takeover grazie a funzioni di op/unban/invite e anche mass deop e mass kick.

- - -

<a name="comesiusa"></a>

## Come si usano i services?

I services sono dei programmi che possono essere usati mandando dei messaggi agli utenti virtuali NickServ e ChanServ. Quindi,
supponendo che io voglia ottenere l'help dei comandi di NickServ dovro' scrivere:

    /msg NickServ HELP

questo comando dovrebbe funzionare su tutti i client. Alcuni permettono di abbreviare la sintassi in questo modo

    /nickserv HELP
    /ns HELP

non funziona con tutti client (BitchX ad esempio). Analogamente per ChanServ utilizzeremo i comandi:

    /msg ChanServ HELP
    /chanserv HELP
    /cs HELP

*NB: In questo HOWTO i comandi che vanno dati al client sono in <font class="ircsend">ROSSO</font>.*

- Come si utilizza NickServ?

La prima cosa da fare quando si utilizzano i Services è registrare il nostro NickName. In questo modo eviteremo che qualcun'altro possa
utilizzare il nick registrato al posto nostro. Inoltre tutti gli altri Services si basano sul riconoscimento del nickname effettuato da
NickServ. Per registrare un nickname si usa il comando REGISTER in questo modo.

    /msg NickServ REGISTER miapassword miaemail@indirizzo.com

a questo comando NickServ ci risponde con:

    -NickServ- Nickname megabug registered under your account

indicando che il nickname è stato correttamente registrato. Se non volete rivelare l'email potete metterne una fasulla, ma facendo in questo
modo non potrete recuperare la password nel caso questa venga smarrita. Ora che siamo registrati tutte le volte che ci riconnetteremo al
server IRC riceveremo il seguente messaggio da NickServ:

    -NickServ- This nickname is registered and protected. If it is your
    -NickServ- nick, type /msg NickServ IDENTIFY password. Otherwise,
    -NickServ- please choose a different nick.

Che ci invita, gentilmente, a cambiare nick perché quello che stiamo usando è registrato. Nel nostro caso, essendo il nickname registrato da
noi, ci bastera' dare a NickServ il comando...

    /msg NickServ IDENTIFY miapassword

    -NickServ- Password accepted - you are now recognized.

...per essere riconosciuti.

*NB: In questo HOWTO i messaggi ricevuti dal server sono in <font class="ircrecv">VERDE</font>.*

Possiamo controllare se un utente si è identificato facendogli un WHOIS. Per esempio quello che segue è il risultato di un whois a un utente
NON identificato (notare che "identificato" è diverso da registrato in quanto un nick puo' essere registrato, pero' qualcuno potrebbe
collegarsi e poi non inserire la password quando gli viene richiesta...):

    /whois megabug

    .----------------------------------------- -- -
     | megabug (~megabug@29773c92.1959841d.dialup.tiscali.it) (Italy)
     : ircname : megabug
     | server : paranoia.irc.mufhd0.net (Autistici/Inventati IRC Server)
     : idle : 0 hours 4 mins 17 secs (signon: Tue Apr 23 15:42:01 2002)

mentre se un utente si identifica il suo WHOIS cambiera' nel seguente:

    /whois megabug

    .----------------------------------------- -- -
     | megabug (~megabug@29773c92.1959841d.dialup.tiscali.it) (Italy)
     : ircname : megabug
     | register : megabug - is a registered nick
     | server : paranoia.irc.mufhd0.net (Autistici/Inventati IRC Server)
     : idle : 0 hours 4 mins 17 secs (signon: Tue Apr 23 15:42:01 2002)

notare che è comparsa la riga "register : megabug - is a registered nick".

Se qualcuno "ruba" volontariamente il nostro nick, possiamo forzarne il rilascio con il comando RECOVER in questo modo:

    &lt;megabug&gt; io sono un ladro

    &lt;megabug\_&gt; e io che sono il vero megabug ti frego!
    /msg NickServ RECOVER megabug miapassword
    
    -:- megabug is now known as Guest9568776
    -NickServ- User claiming your nick has been killed.
    -NickServ- /msg NickServ RELEASE megabug to get it back before the one-minute timeout.

in questo modo l'utente che stava usando il nostro nick viene forzatamente "rinominato". I Services mantengono il nostro nick per un minuto,
dopodiché possiamo riprenderne il possesso:

    /nick megabug

    &gt;&gt;&gt; You(megabug\_) are now known as megabug
    -NickServ- This nickname is registered and protected. If it is your
    -NickServ- nick, type /msg NickServ IDENTIFY password. Otherwise,
    -NickServ- please choose a different nick.

    /msg NickServ IDENTIFY miapassword

    -NickServ- Password accepted - you are now recognized.

Possiamo anche fare in modo che la protezione sul nick si attivi anche quando non ci siamo, in modo che se non ci si identifica entro un
minuto il nick venga automaticamente recuperato. Questa è una delle opzioni che possiamo abilitare sul nostro nick con il comando SET. Per
avere l'elenco completo delle opzioni basta fare

    /msg NickServ HELP SET

l'opzione di cui parlavamo prima si chiama KILL, e per abilitarla si usa il seguente comando:

    /msg NickServ SET KILL ON

    -NickServ- Protection is now ON.

inoltre, se la nostra connessione non ha un indirizzo IP fisso (come la maggiorparte delle connessioni private) dobbiamo eliminare tutte le
entry nella nostra access list (per maggiori informazioni su questa feature consultate l'help online di NickServ). Comunque i comandi da
dare per ripulire la access list sono:

    /msg NickServ ACCESS LIST

    -NickServ- Access list:
    -NickServ- \*megabug@\*.2d34f077.stru.polimi.it

questo comando visualizza la nostra access list. Dobbiamo togliere la entry "\*megabug@\*.2d34f077.stru.polimi.it" con il comando:

    /msg NickServ ACCESS DEL \*megabug@\*.2d34f077.stru.polimi.it

    -NickServ- \*megabug@\*.2d34f077.stru.polimi.it deleted from your access list.

    /msg NickServ ACCESS LIST

    -NickServ- Access list:

ora la nostra access list è vuota. Se proviamo a riconnetterci al server irc vedremo il seguente messaggio:

    -NickServ- This nickname is registered and protected. If it is your
    -NickServ- nick, type /msg NickServ IDENTIFY password. Otherwise,
    -NickServ- please choose a different nick.
    -NickServ- If you do not change within one minute, I will change your nick.

(se non ci identifichiamo entro un minuto)

    &gt;&gt;&gt; You(megabug) are now known as Guest9568777
    -NickServ- Your nickname is now being changed to Guest9568777

NickServ ha forzato il cambio di nick come volevamo.

- - -

<a name="chanserv"></a>

## Come si utilizza ChanServ?

Ora che abbiamo registrato il nostro nickname possiamo utilizzarlo per registrare un canale col servizio ChanServ.

Per registrare un canale bisogna essere Op del canale stesso, naturalmente, se il canale da noi scelto non è ancora registrato e se non
viene utilizzato da nessuno, questo fatto non costituirà un problema.

    /join \#prova

    -:- megabug \[~megabug@1e79b4c9.27ea28c2.fastres.net\] has joined \#prova
     -:- \[Users(\#prova:1)\]
     \[@megabug \]
     -:- Channel \#prova was created at Sat Apr 27 02:21:49 2002

Ora che siamo operatori del canale \#prova, siccome \#prova non è ancora registrato, possiamo registrarlo con il comando REGISTER:

    /msg ChanServ REGISTER \#prova miapassword Descrizione del canale

    -ChanServ- Channel \#prova registered under your nickname: megabug
-ChanServ- Your channel password is miapassword - remember it for later use.

La registrazione del canale è stata effettuata e l'utente che l'ha registrato (megabug in questo caso) è diventato il fondatore del canale.
Il fondatore di un canale può cambiare le modalità di accesso al canale, può definire una lista di nick che godono di alcuni privilegi, ecc.
Ma andiamo con ordine.

Proviamo a uscire e rientrare nel canale:

    /part \#prova
    /join \#prova

    -:- megabug \[~megabug@1e79b4c9.27ea28c2.fastres.net\] has joined \#prova
    -:- \[Users(\#prova:1)\]
    \[@megabug \]
    -:- mode/\#prova \[+nrt\] by ChanServ
    -:- Topic (\#prova): changed by ChanServ: (ChanServ)
    -:- mode/\#prova \[+o megabug\] by ChanServ
    -:- Channel \#prova was created at Sat Apr 27 02:34:31 2002

come possiamo notare ChanServ ci ha oppato automaticamente. L'auto-Op è una funzione molto carina di ChanServ. Di default il fondatore di un
canale viene oppato (anche perché è colui che ha i privilegi più alti sul canale). Inoltre il fondatore può compilare una lista di nick che
verranno automaticamente oppati quando, a loro volta, si collegheranno al canale.
 In realtà il fondatore può mantenere tre liste separate di privilegi:

- VOP (VOice People) - Questa lista contiene i nick che riceveranno automaticamente la modalità Voice
    quando entrano in canale.
- AOP (Auto OP) - Questa lista contiene i nick che riceveranno automanticamente la modalità Op quando
    entrano in canale.
- SOP (Super OP) - Chi è in questa lista ha gli stessi privilegi degli AOP ma in più può usare l'AutoKick
    e leggere o scrivere messaggi nel memo del canale.

Le tre liste usano gli stessi comandi per inserire/togliere nick dall'elenco. Quindi se per la lista AOP uso il comando:

    /msg ChanServ AOP \#prova LIST

analogamente per le liste VOP e SOP utilizzerò rispettivamente i comandi:

    /msg ChanServ VOP \#prova LIST
    /msg ChanServ SOP \#prova LIST

e questo è valido per tutti i comandi che andremo a vedere.

Il comando LIST che abbiamo appena visto, visualizza il contenuto di una lista. Siccome il canale è stato appena creato, la lista sarà
vuota

    -ChanServ- \#prova AOP list is empty.

Per aggiungere un nick alla lista si usa il comando ADD:

    /msg ChanServ AOP \#prova ADD baku

    -ChanServ- baku added to \#prova AOP list.

    /msg ChanServ AOP \#prova LIST

    -ChanServ- AOP list for \#prova:
    -ChanServ- Num Nick
    -ChanServ- 1 baku

come possiamo notare il comando ADD ha aggiunto l'utente "baku" alla lista AOP del canale.

Analogamente possiamo cancellarlo con il comando DEL

    /msg ChanServ AOP \#prova DEL baku

    -ChanServ- baku deleted from \#prova AOP list.

Con questi pochi comandi possiamo regolare in maniera molto fine gli accessi al canale. Come già detto solo il fondatore del canale può
modificare queste liste. Può essere utile però la possibilità di acquisire i privilegi di fondatore di un canale anche quando non si è
connessi al server IRC con il nick usato per fondare il canale stesso. ChanServ prevede anche questa possibilità usando il comando IDENTIFY:

    /msg ChanServ IDENTIFY \#prova miapassword

    -ChanServ- Password accepted - you now have founder-level access to \#prova.

In questo caso anche se non siamo stati noi a fondare il canale, conoscendo la password possiamo acquisire i diritti di accesso riservati al
fondatore, oppure possiamo distribuirla a persone di nostra fiducia che ci aiuteranno a gestire il canale.

<a name="takeover"></a>

## Come si gestiscono i takeover?

Nei canali di cui abbiamo i privilegi di founder non dobbiamo preoccuparci dei takeover dato che esiste un meccanismo per recuperare anche
le situazioni più estreme. Il comando in questione, disponibile solo al founder del canale naturalmente, è il comando CLEAR. Esistono alcune
varianti di questo comando utilizzabili in base alle necessità:

    /msg ChanServ CLEAR \#prova OPS

Questo comando toglie i privilegi di OP a tutti gli utenti di \#prova. È utile quando qualcuno fa un takeover togliendo i privilegi di OP ma
senza usare kick/ban.

    /msg ChanServ CLEAR \#prova BANS

Se qualcuno dopo un takeover kicka e banna tutti gli utenti del canale è possibile rimuovere i ban con questo comando. Naturalmente va usato
dopo un CLEAR OPS in modo che chi ha fatto il takeover non possa reimpostare i ban subito dopo.

    /msg ChanServ CLEAR \#prova MODES

Rimuove tutti i modi di un canale (in particolare +k +i e +l). Se qualcuno imposta il canale "a invito" (+i) o protetto da password (+k) o
con un limite di utenti (+l), possiamo rimettere il canale nelle condizioni di partenza con questo comando. Naturalmente va usato dopo un
CLEAR OPS per lo stesso motivo di prima.

    /msg ChanServ CLEAR \#prova USER

Questo comando rimuove tutti gli utenti dal canale. In altre parole fa un kick di massa...
 Chiaramente se si può evitare lo si evita. Ma potrebbero esserci dei casi in cui è la soluzione con meno sbattimenti ;).

Una volta ripreso il controllo del canale possiamo ridiventare operatori (nel caso ChanServ non l'abbia già fatto automaticamente) con il
comando:

    /msg ChanServ OP


