title: IRC Chatting Howto
----

IRC Chatting Howto
==========

If you're interested in chatting, but you don't know what it is all about, you can start with
[indivia's](https://webirc.indivia.net/cgiirc/irc.cgi) web
interface. If you find IRC chat interesting, you can try some more
sophisticated client such as [Xchat](/docs/irc/xchat) or
[Pidgin](/docs/irc/pidgin).

The main information you need for chatting is the following:

- [configuration parameters, see below](#parm_irc);
- the *channel* where you wish to chat (channel names begin with the \# symbol, e.g.: \#indymedia or \#etc). You can choose whatever channel
    you prefer. The most popular channel on irc.autistici.org is \#hackit99, but the network hosts italy indymedia's channel as well,
    \#indymedia; You can find A/I collective members on our channel to pester us with questions we will not answer, \#ai
- your *nick*, that is the name you want to use while chatting (you can use your real name or a nickname you like). If you want to be sure
    that your nick isn't used by someone else, **register** it (if you want to know more about this and other services, read this
    [manual](/docs/irc/irc_services)).

The main commands you'll need in IRC -- contained in specific menus in graphic clients such as
[Xchat](/docs/irc/xchat) -- are the following:

- /nick followed by the nick you want to use for changing your nick
- /join followed by the name of the channel you wish to access (e.g. /join \#hackit99);
- /part followed by the name of the channel you wish to log out from;
- /quit to quit IRC;
- /msg followed by someone else's nick in order to start a private conversation (a socalled 'private message' or 'query') with her.

There are of course many more commands: if you wish to learn more, you'll find several howtos in the Web.

A/I's IRC server offers some additional services called 'IRC Services'. To learn more about them, read this
[manual](/docs/irc/irc_services).

<a name="parm_irc"></a>
Technical Info
--------------

- Server address: `irc.autistici.org`
- Port (with SSL): `6697` or `9999`

For client configuration, read the howtos on [X-Chat](/docs/irc/xchat),
[Pidgin](/docs/irc/pidgin) (for Linux, Mac and Windows), [Irssi](/docs/irc/irssi) (Linux command line).

Since the mufhd0 network is managed together with [ECN](http://www.ecn.org) and [Indivia](http://www.indivia.net), should the A/I servers
have problems you can connect to these other servers.

Contacts
--------

If you have any problems or questions about our IRC service and/or about the possibility of linking other servers, write to:
<irc@autistici.org>.
