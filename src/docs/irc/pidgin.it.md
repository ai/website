title: Connettersi a IRC con Pidgin 
----

Connettersi a IRC con Pidgin
============================

Se già usate Pidgin, potreste trovare comodo usare lo stesso client anche per chattare in IRC. Pidgin può collegarsi a IRC crittando la
connessione con SSL (se la rete che usate lo permette, come nel caso di A/I). Pidgin è insomma una buona soluzione se non volete installare
un software che non conoscete o un client specifico per IRC, però purtroppo non offre tutte le funzionalità di altri software appositi.

1.  Dopo aver lanciato Pidgin, aprite la finestra di gestione dell'account nel menu in alto: Account -- Gestisci gli account
2.  Cliccate su 'Aggiungi' per aggiungere un nuovo account
3.  Nella prima scheda che trovate, inserite i dati come nell'immagine (ovviamente, scegliete il nome utente, o nickname, che preferite):
     ![pidgin account manager](/static/img/man_irc/it/pidgin1.png)
4.  Ora passate alla scheda 'Avanzate' e configurate il programma come nell'immagine:
     ![pidign account manager advanced tab](/static/img/man_irc/it/pidgin2.png)
    N.B.: se non aggiungi qui il tuo nick nello spazio 'Nome identità', sarà possibile agli altri utenti di IRC visualizzare il nome che hai
    scelto per l'utente del computer che stai usando.
5.  A questo punto cliccate il tasto 'Salva' in basso a destra. Quando Pidgin cercherà di collegarsi alla rete, visualizzerete questa
    schermata:
     ![certificate screen in pidgin](/static/img/man_irc/it/pidgin3.png)
6.  Cliccate il tasto 'Accetta' e Pidgin dovrebbe connettersi alla rete
7.  Si aprirà una finestra come quella qui sotto. A questo punto non vi resta che una cosa da fare: accedere al canale che preferite. Nel
    riquadro che usate per scrivere i vostri messaggi, scrivete esattamente il comando che segue (incluso lo / iniziale e il cancelletto –
    vedi illustrazione): /join \#nomecanale
     ![join the channel](/static/img/man_irc/it/pidgin4.png)
8.  Cliccate Invio e Pidgin accederà al canale. Sulla destra vedrete la lista delle altre persone presenti nel canale, sulla sinistra la
    solita finestra della chat e in basso lo spazio dove scrivere i vostri messaggi. Da ora in poi potete chattare come fate normalmente
    con Pidgin.
9.  [Qui](/docs/irc/) trovate alcuni dei comandi che potrebbero servirvi in IRC.
