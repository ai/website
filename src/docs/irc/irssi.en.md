title: Irssi Howto
----

Irssi Howto
=====================

*last revision: 2007, oct 7th
provided by dezperado AT autistici DOT org*

This small manual details how to setup irssi IRC client to chat on the A/I IRC server. We suggest you to read the manual until the very last
line before you start hacking around your PC.

Irssi IRC client can be download from [here](http://irssi.org/) and you can use it on any UNIX box. It's a very advanced client, but still
not very demanding in term of computer resources since it only runs as command line client.

Irssi Quickstart
----------------

To connect to irc.autistici.org server (aka ai.irc.mufhd0.net) we use the command:

     irssi  -c irc.autistici.org -n username

We will see on our screen many welcoming messages detailing features and informations on the server:

    ....
    10:55 -!- -
    10:55 -!- - Benvenuti su mufhd0.net
    10:55 -!- -
    ....
    10:55 -!- -
    10:55 -!- - STRICTLY NO BOTS - NON METTETE NESSUN BOT o morirete.
    10:55 -!- -
    ....

Now we can write:

    /list
    to check out which channels are open to participation. And:
    /j \#fuffa
    to join the \#fuffa channel.
    To leave a channel we can use the /leave command.
    To quit Irssi we can use the /quit command.
    To get some help we can use the /help command (it's not very much, but we'll have to cope with it)

If we joined several channels using multiple times /j (or /join) we can use Alt-1, Alt-2 to switch between different channels.

We could also type the commands /window left and /window right to achieve the same result.

There are many more IRC commands, but for a beginner these are more than enough!

### Irssi config

Let's create a small configuration setup so as to avoid to type too many commands the next time we want to connect to mufhd0.
 We open our bash terminal and type the following commands:

        $ mkdir ~/.irssi
        $ vim ~/.irssi/config	
    	(of course we can use emacs or pico or whatever to edit the file)

In the file section about "servers" add the lines below (possibly the section of the file with similar lines can be very very long, be
warned!)

    servers = (
      {
        address = "ai.irc.mufhd0.net";
        chatnet = "ai";
        use_ssl = "no";
        port = "9999";
      }
    );

Now we will be able to chat on mufhd0 network simply typing the command /connect ai.

Unfortunately with this setup we are still vulnerable to a low degree of privacy (we are not using an encrypted connection) and to man in
the middle attacks (if we are chatting in a hostile environment and someone is looking forward to sneak on our communications).

**Let's try to cover our tracks a bit!**

### Irssi config + ssl

If we want to access to mufhd0 through an encrypted connection we have to change our configuration setup. Open the file ~/.irssi/config and
change the servers configuration:

      {
        address = "ai.irc.mufhd0.net";
        chatnet = "ai";
        use_ssl = "yes";
        ssl_verify = "yes";
        port = "9999";
      },

Now our connection to the server will be encrypted and we will be able to connect to mufhd0 server via SSL using the command: /connect ai.
 **Note: Irssi must have been compiled with SSL support (using *'./configure --enable-ssl'* before running *'make'*)**

Unfortunately we are still vulnerable to man in the middle attack.

We have to download mufhd0 certificates to be sure to connect to the real mufhd0 servers.

Let's see how to do it!

### Irssi config + authenticated ssl

We have to use 'sudo' or 'su' to gain root privileges on our box.
The information below works with Linux and Openssl 0.9 and similar configurations.
Let's edit again our ~/.irssi/config file with the following lines:

      {
        address = "ai.irc.mufhd0.net";
        chatnet = "ai";
        use_ssl = "yes";
        ssl_verify = "yes";
        ssl_cafile = "/etc/ssl/certs/ca-certificates.crt";
        port = "9999";
      },

### extra : using irssi with a registered nickname

Now we have a secure configuration to connect to mufhd0 IRC servers we can register a nick on the A/I network (chatting with the Nickserv
virtual user, whose task you can discover using the '/msg NickServ HELP' once connected)

      {
        address = "ai.irc.mufhd0.net";
        chatnet = "ai";
        use_ssl = "yes";
        ssl_verify = "yes";
        nick = "utente";
        password = "123456";
        port = "9999";
      },

With a registerd nick (and thus with our password) we have to connect to the IRC servers via SSL otherwise we will leak our credentials to
anybody nosing around our networks.

### extra docs on Irssi

Irssi official site is very helpful but a bit weak on the documentation side. We can find anything about Irssi in the following links:

- [irssi.org](http://irssi.org/)
- [f0rked.com/public/irssi-docs/help-full.html](http://f0rked.com/public/irssi-docs/help-full.html)
- [lizzie.spod.cx/screenirssi.shtml](http://lizzie.spod.cx/screenirssi.shtml)
- [irssi.org/scripts/](http://irssi.org/scripts/)
- [openssl.org/](http://www.openssl.org/)
