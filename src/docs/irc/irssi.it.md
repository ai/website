title: Manuale per irssi
----

Manuale per irssi
=================

*data ultima revisione : 7 ottobre 2007
provided by dezperado AT autistici DOT org*

Questo minihowto spiega come impostare il client IRC irssi per chattare sul server IRC di autistici. Consigliamo di leggere fino in fondo il
documento prima di smanettare :)

Il client irssi è disponibile [qui](http://irssi.org/) e funziona su UNIX (quindi anche su GNU/Linux).

È un client molto evoluto ma abbastanza leggero, in quanto non ha bisogno dell'ambiente di finestre X.

Irssi Quickstart
----------------

Accediamo al server irc.autistici.org ( anche noto come ai.irc.mufhd0.net ) invocando dalla shell :

     irssi  -c irc.autistici.org -n utente

Quindi veniamo accolti da diversi messaggi di benvenuto e informazioni con le caratteristiche e funzionalità del server:

    ....
    10:55 -!- -
    10:55 -!- - Benvenuti su mufhd0.net
    10:55 -!- -
    ....
    10:55 -!- -
    10:55 -!- - STRICTLY NO BOTS - NON METTETE NESSUN BOT o morirete.
    10:55 -!- -
    ....

A questo punto scriviamo

    /list
    per vedere la lista dei canali aperti, e
    /j \#fuffa
    per accedere (o creare) al canale \#fuffa.
    Usciamo da un canale con /leave
    Usciamo da irssi con /quit
    Otteniamo (poca) informazione d'aiuto con /help

Se abbiamo usato /j (abbreviazione di /join) diverse volte per diversi chan, usiamo ALT-1,ALT-2,... per saltare da una finestrella di chat
all'altra.

Possiamo anche usare /window left o /window right .

Ci sono moltissimi altri comandi, ma per iniziare questo basta!

### Irssi config

Creiamo una piccola configurazione così la prossima volta che vogliamo chattare su mufhd0 non dobbiamo ricordarci troppe cose.

Dalla shell:

        $ mkdir ~/.irssi
        $ $EDITOR ~/.irssi/config	(oppure kate, vim, gedit, nedit,.. per $EDITOR )

Cerchiamo o creiamo una 'sezione' della configurazione simile alla seguente (nota che può essere una sezione molto molto lunga!):

    servers = (
      { address = "irc.stealth.net"; chatnet = "IRCnet"; port = "6668"; }
    );

e aggiungiamo una sezioncina per mufhd0:

    servers = (
      {
        address = "ai.irc.mufhd0.net";
        chatnet = "ai";
        use_ssl = "no";
        port = "9999";
      },
      { address = "irc.stealth.net"; chatnet = "IRCnet"; port = "6668"; }
    );

Ora possiamo chattare su mufhd0, con il comando /connect ai .

Purtroppo siamo suscettibili a:

- perdita di privacy (non usiamo connessione crittata)
- attacchi man in the middle (se siamo in ambiente ostile, qualcuno potrebbe impersonare il server mufhd0 e gabbarci)

**Vediamo come proteggerci.**

### Irssi config + ssl

Se vogliamo accedere in modalità crittata a mufhd0, apriamo ~/.irssi/config come prima, e aggiustiamo

      {
        address = "ai.irc.mufhd0.net";
        chatnet = "ai";
        use_ssl = "yes";
        ssl_verify = "yes";
        port = "9999";
      },

Ora le connessioni saranno criptate, tramite SSL, e ci si connetterà da dentro irssi con il comando /connect ai.
 **NOTA: irssi deve essere stato compilato con SSL (quindi eseguendo *./configure --enable-ssl* prima della compilazione).**

Tuttavia, siamo ancora suscettibili di attacchi man in the middle.

Per chattare in piena sicurezza abbiamo bisogno di scaricare un certificato che dia la piena garanzia di avere a che fare con il vero
irc.autistici.org.

### Irssi config + authenticated ssl

Dobbiamo poter avere i privilegi di root sul computer, prima di tutto, usando 'sudo' o 'su'.

Questa ricetta vale per Linux+openssl 0.9 e 'vicine'.

Ricorreggiamo la nostra sezione di configurazione in ~/.irssi/config:

      {
        address = "ai.irc.mufhd0.net";
        chatnet = "ai";
        use_ssl = "yes";
        ssl_verify = "yes";
        ssl_cafile = "/etc/ssl/certs/ca-certificates.crt";
        port = "9999";
      },

### extra: usare irssi con un nick registrato

Ora che abbiamo la nostra bella configurazione sicura, possiamo registrarci un nick sul server ('chattando' con l'utente NickServ, che ci
registrerà: vedi /msg NickServ HELP una volta connesso ).

      {
        address = "ai.irc.mufhd0.net";
        chatnet = "ai";
        use_ssl = "yes";
        ssl_verify = "yes";
        nick = "utente";
        password = "123456";
        port = "9999";
      },

Con un nick registrato (e quindi con password) è necessario connettersi in maniera crittata e sicura, altrimenti non abbiamo garanzie.

### documentazion extra su Irssi

Il sito ufficiale di irssi offre aiuto ma è un po' carente. Ecco qui alcuni links:

- [irssi.org](http://irssi.org/)
- [f0rked.com/public/irssi-docs/help-full.html](http://f0rked.com/public/irssi-docs/help-full.html)
- [lizzie.spod.cx/screenirssi.shtml](http://lizzie.spod.cx/screenirssi.shtml)
- [irssi.org/scripts/](http://irssi.org/scripts/)
- [openssl.org/](http://www.openssl.org/)

