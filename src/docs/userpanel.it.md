title: Howto per il pannello utente
----

Howto per il pannello utente
============================


Per usare uno qualsiasi dei [servizi](/services/) che A/I offre dovete essere titolari di un utente presso i server di A/I e quindi
essere in grado di accedere al vostro **Pannello Utente** nel quale trovare tutte le informazioni sui vostri servizi attivi e su come
interagirvi.

Questo manuale vorrebbe essere una guida dettagliata a tutto quello che potete fare accedendo al vostro **Pannello Utente**

<a name="topsummary"></a>

Indice
------

- [Accesso](#login)
- [Cosa fare dopo il primo accesso](#firstlogin)
  1.  [Impostare la domanda per il recupero della propria password](#recoverquestion)
  2.  [Cambiare la propria password](#passwordchange)
- [Pagina principale del Pannello Utente](#main)
- [Come cambiare la lingua del proprio Pannello Utente](#languagechange)
- [Come leggere la propria mail e come cambiare le sue impostazioni dal Pannello Utente](#mailbox)
  1.  [Come aggiungere o cancellare un alias](#alias)
  2.  [Come aggiungere la propria chiave PGP (per renderla disponibile tramite protocollo WKD)](#pgp)
- [Come gestire un sito ospitato su A/I dal Pannello Utente](#website)
- [Come gestire il proprio account WebDAV dal Pannello Utente (e come cambiare la propria password WebDAV)](#webdav)
- [Come gestire il proprio database mysql dal Pannello Utente](#mysql)
- [Come gestire le proprie mailing list dal Pannello Utente](#mailinglist)
- [Come chiudere il proprio account](#close)

<a name="login"></a>

Accesso
-------

Per accedere al vostro **Pannello Utente** dovete cliccare sulla scritta "Login" che
c'è in alto a destra sulla [homepage del sito di A/I](/)

![](/static/img/man_userpanel/it/login_page01.png "Pagina di login del Pannello Utente")

inserire il vostro utente (l'indirizzo mail completo!) e la vostra password e poi cliccare su *Sign in*.

![](/static/img/man_userpanel/it/login_page02.png "Pagina di login del Pannello Utente")

<a name="firstlogin"></a>

# Cosa fare dopo il primo accesso

<a name="recoverquestion"></a>

## Impostare la domanda per il recupero della propria password

Se è la prima volta che accedete al vostro **Pannello Utente** sarete indirizzati su una pagina per impostare una domanda di
ripristino: se vi dimenticaste la vostra password conoscere tale domanda e la sua risposta diventerebbe l'unico modo per
accedere nuovamente al vostro utente e alla vostra casella di posta.

Questa pagina è anche chiamata dei *Gatti* (dato che di solito le domande più gettonate sono quelle sui propri animali abbiamo
pensato di dedicargli tutto il sistema di ripristino delle password dimenticate).

![](/static/img/man_userpanel/it/recovery_set.png "Form per l'impostazione della domanda di ripristino, anche noto come Gatto")

Dovete inserire la password del vostro account, una domanda e la relativa risposta, poi cliccate sul bottone *Aggiorna*.

**Attenzione**: dovete essere sicuri di ricordarvi l'esatta risposta alla domanda che avete inserito (incluse lettere maiuscole, simboli,
numeri e quant'altro) dato che fornirla sarà l'unico modo per recuperare la vostra password in caso di smarrimento.

Inoltre vogliamo ricordarvi che A/I **non conosce la risposta alla domanda che avete inserito** dato che essa viene conservata nei nostri
database in forma crittata. Siete e sarete l'unica persona a sapere come rispondere alla domanda e quindi ad essere in grado di recuperare
la password che vi siete dimenticati.

Una volta che avrete cliccato sul bottone *Aggiorna* verrete riportati al vostro **Pannello Utente**

<a name="passwordchange"></a>

## Cambiare la password

La seconda cosa che dovete **tassativamente** fare prima di procedere a scoprire la vostra mail e gli altri servizi offerti
da A/I è cambiare la vostra password.

Per cambiare la password, cliccate su **Gestione Account**

![](/static/img/man_userpanel/it/main_management.png "Pulsante gestione account")

e poi su **Cambia la password**,

![](/static/img/man_userpanel/it/management.png "Opzioni per l'account")

Una volta che avrete cliccato sul link *Cambia la password* verrete indirizzati a una form dove potrete inserire la vostra
password attuale e **due volte** la vostra nuova password. Dopodiché cliccate sul bottone *Aggiorna* e avrete una nuova password per accedere al vostro **Pannello Utente** e **alla vostra casella di posta**.

![](/static/img/man_userpanel/it/change_password01.png "Pagina per cambiare la vostra password")

**Attenzione**: in questo passaggio state cambiando la password del vostro utente e della vostra casella di posta. Se volete cambiare la
vostra password WebDAV o la password per gestire una mailing list dovrete riferirvi ad altre [parti di questo manuale](#topsummary).

Una volta che avrete cliccato sul bottone *Aggiorna* verrete riportati automaticamente al vostro **Pannello Utente**

<a name="main"></a>

Pagina principale del Pannello Utente
-------------------------------------

![](/static/img/man_userpanel/it/main.png "Pagina principale del Pannello Utente")

Quello che vedete nell'immagine soprastante è una versione molto base di come potrebbe apparire il vostro **Pannello Utente**. E' il
**Pannello Utente** di un utente che ha solo una casella di posta sui server di A/I. Non ha indirizzi di posta aggiuntivi, né liste, né
siti, né newsletter, niente di niente. Più o meno come la maggior parte dei nostri utenti, quindi non c'è niente di cui vergognarsi :)

Ora vediamo di dargli un po' di colore e di capire come diavolo è fatto il **Pannello Utente**

![](/static/img/man_userpanel/it/main_boxes.png "Le sezioni del Pannello Utente")

Potete immaginare il **Pannello Utente** diviso in diverse sezioni:

-   In basso, evidenziato da un **rettangolo giallo**, trovate un link per cambiare la lingua per la visualizzazione del **Pannello Utente**.
-   Nella parte destra della pagina trovate un pulsante a sfondo nero con la scritta **Gestione account**, evidenziato da un
    **rettangolo rosso**, che porta a una pagina dove impostare alcuni parametri generali del vostro account:
    cambiare la password, cambiare la domanda/risposta di recupero, impostare la 2FA, e chiudere l'account.
-   Nella zona centrale del **Pannello Utente**, evidenziato da un **rettangolo verde**, troverete le informazioni principali per accedere
    e configurare i vostri servizi su A/I. La maggior parte di quello che trovate in questo manuale riguarda questa sezione del pannello.
-   E infine, evidenziato dal **rettangolo blu** in alto a destra c'è il punto in cui potete effettuare il **logout** dal **Pannello Utente**.
    Se non siete passati a leggere la vostra posta, prima di uscire ricordatevi di cliccare su quel link prima di chiudere il browser.

<a name="languagechange"></a>

Come cambiare la lingua del proprio Pannello Utente
---------------------------------------------------

Se volete cambiare la lingua in cui visualizzate il vostro **Pannello Utente** non dovrete far altro che cliccare sulla lingua che
desiderate (nella zona evidenziata in rosso nell'immagine sottostante). Una volta cliccato il bottone la pagina verrà ricaricata tradotta
nella lingua selezionata e un avviso nella barra verde chiaro in alto vi informerà dell'avvenuto cambiamento di lingua del vostro
**Pannello Utente**.

![](/static/img/man_userpanel/it/language.png "cambiate la lingua in cui il pannello viene visualizzato")

<a name="mailbox"></a>

Come leggere la propria mail e come cambiare le sue impostazioni dal Pannello Utente
------------------------------------------------------------------------------------

Abbiamo già spiegato come [cambiare la password della vostra mail](#passwordchange) e come
[impostare la vostra domanda per il recupero della password in caso di smarrimento](#recoverquestion). Ora potete finalmente collegarvi alla
vostra casella di posta e leggere un po' di e-mail: cliccate sulla *webmail* nella zona principale del
**Pannello Utente**, e verrete indirizzati automaticamente alla nostra webmail. Se volete sapere come usarla, potete leggere il [nostro
 manuale](/docs/mail/roundcube). Ricordatevi che una volta che avrete fatto logout dalla webmail, sarete anche stati
scollegati dal vostro **Pannello Utente** automaticamente.

![](/static/img/man_userpanel/it/mailbox_read.png "Cliccate per leggere la vostra mail!")

Se volete sapere quanto spazio state usando sui nostri server, potete vedere questo dato nella sezione email del pannello,
come evidenziato nella figura sottostante.
Vorremmo ricordarvi che non esiste un limite allo spazio disco che potete usare sui server di A/I, ma ricordatevi sempre che le
nostre risorse non sono infinite e che le condividete con migliaia di altre persone. Prestate attenzione a quanto spazio utilizzate.

![](/static/img/man_userpanel/it/mailbox_space.png "Guardate quanto spazio occupate sui nostri server")

<a name="alias"></a>

## Come aggiungere o cancellare un alias

Prima di tutto: che diavolo è un **alias**?

Un **alias** è un indirizzo di posta elettronica alternativo su cui volete ricevere i vostri messaggi: in pratica chi invierà un messaggio
al vostro indirizzo o al vostro **alias** vi raggiungerà senza problemi. Inoltre tale indirizzo può essere usato come mittente dei vostri
messaggi di posta elettronica se configurate il vostro client di posta adeguatamente. In ogni caso il vostro **alias** sarà collegato
inscindibilmente al vostro indirizzo di posta.

Sui server di A/I potete gestire in autonomia al massimo 5 alias per ogni casella di posta che avete richiesto. Se avete bisogno di più di 5
alias di posta, [contattateci](/get_help) e vedremo cosa possiamo fare.

Se volete creare un **alias** per la vostra casella di posta cliccate sul pulsante **Gestisci gli alias**  di fianco al vostro indirizzo e-mail nella zona principale del **Pannello Utente**

![](/static/img/man_userpanel/it/mailbox_createalias.png "Cliccate per creare un alias")

Una volta che avrete cliccato verrete indirizzati a una form dove potrete inserire l'indirizzo alternativo che volete attivare (sulla sinistra) e il dominio (sulla destra).  Cliccate poi sul bottone *Crea* e avrete il vostro nuovo indirizzo di posta elettronica.

![](/static/img/man_userpanel/it/mailbox_createaliasform.png "Cliccate per creare un alias")

Ora se cliccate di nuovo sul pulsante **Gestisci gli alias** potete trovare il vostro **alias** nella pagina.
Se volete rimuoverlo, dovete semplicemente cliccare sul simbolo del cestino di fianco all'indirizzo che volete cancellare (indicato dalla freccia rossa nell'immagine sottostante).

![](/static/img/man_userpanel/it/mailbox_deletealias.png "Cliccate per cancellare un vostro alias")


<a name="pgp"></a>

Come aggiungere la propria chiave pubblica PGP al Pannello Utente
-------------------------------------------------------------

E' possibile aggiungere la propria chiave pubblica PGP al pannello utente in modo che sia disponibile
ai client che supportano il [protocollo WKD](https://wiki.gnupg.org/WKD).

Cliccando sul pulsante "OpenPGP" nella sezione del pannello di fianco al vostro indirizzo email.

![](/static/img/man_userpanel/it/pgp01.png "Cliccate per aggiungere la chiave PGP")

si aprirà un form dove dovrete incollare la vostra chiave PGP pubblica in formato ascii.

![](/static/img/man_userpanel/it/pgp02.png "Inserire la chiave PGP")

cliccate poi su **Upload Key**

il form esegue un controllo per verificare che il vostro indirizzo email sia compreso tra gli ID presenti nella chiave,
e se tutto va bene salverà la vostra chiave pubblica.

![](/static/img/man_userpanel/it/pgp03.png "Verificate che tutto sia andato bene")

potete controllare velocemente se la chiave viene offerta tramite protocollo wkd ad es. se usate linux con il comando:

```
/usr/lib/gnupg/gpg-wks-client --verbose --check la_tua_mail@autistici.org
```

<a name="website"></a>

Come gestire un sito ospitato su A/I dal Pannello Utente
--------------------------------------------------------

Se avete richiesto ad A/I qualcosa di più di una semplice casella di posta elettronica il vostro **Pannello Utente** sarà un po' più
complicato di quanto visto finora.
Se gestite anche un sito web ospitato su A/I la vostra area principale del **Pannello Utente** avrà una nuova sezione.

![](/static/img/man_userpanel/it/main2.png "Gestite il vostro sito dal Pannello Utente")

Come potete osservare la gestione di un sito ospitato su A/I prevede tre sottosezioni: una dedicata al **vostro sito** in senso proprio; una
dedicata all'**account WebDAV** usato per caricarvi i materiali; e un'altra ancora relativa alla gestione dell'eventuale **database**
connesso al sito. Ognuna di queste sottosezioni vi offre la possibilità di configurare cose diverse.

![](/static/img/man_userpanel/it/website.png)

Se cliccate sulla rotella alla destra del *nome del sito* si aprira' un menu dropdown con alcune voci:

-   *Visita il sito* aprirà il vostro sito.
-   *Dettagli* vi mostrerà informazioni riguardo eventuali problemi di sicurezza di CMS installati.
-   *Analytics* vi indirizza a una pagina dove potete trovare il codice necessario ad attivare le statistiche fornite direttamente da A/I. Leggi
    il [manuale sull'uso delle statistiche nei siti web ospitati da A/I](/docs/web/webstats) per maggiori informazioni.

![](/static/img/man_userpanel/it/website_stats.png "Copiate e incollate il codice indicato per attivare le statistiche sul vostro sito")



<a name="webdav"></a>

Come gestire il proprio account WebDAV dal Pannello Utente (e come cambiare la propria password WebDAV)
-------------------------------------------------------------------------------------------------------

Per aggiornare il vostro sito avrete bisogno di un utente e una password (diverse da quelle che usate per collegarvi al **Pannello Utente**
e alla vostra casella di posta).

Utilizzando tali nuove password tramite un client WebDAV come descritto nel nostro [manuale per WebDAV](/docs/web/webdav)
potrete accedere e aggiornare il vostro sito web.

Di fianco alla sottosezione **dav** troverete una rotella, cliccandoci sopra comparirà il pulsante per cambiare la password:

![](/static/img/man_userpanel/it/dav.png "Cambiate la vostra password WebDAV")

Prima della rotella c'e' un numero che riporta la dimensione dello spazio che occupate per il sito.


<a name="mysql"></a>

Come gestire il proprio database mysql dal Pannello Utente
----------------------------------------------------------

![](/static/img/man_userpanel/it/mysql.png "Gestite il vostro database MySQL")

Se avete richiesto un database MySQL da usare con il vostro sito, questa sottosezione dell'area principale del **Pannello Utente** vi
consentirà di visualizzare il nome del database e la sua dimensione.
Per cambiare la password del database scrivete a [helpdesk](mailto:help@autistici.org)

<a name="mailinglist"></a>

Come gestire le proprie mailing list dal Pannello Utente
--------------------------------------------------------

Se siete amministratore di una o più liste, le troverete nel vostro **Pannello Utente**.

![](/static/img/man_userpanel/it/list.png "Gestite le vostre liste")

Cliccando sulla rotellina alla destra del nome della lista si aprira' un piccolo menu con 3 opzioni:

-   *Amministrazione" che vi porterà all'interfaccia web di amministrazione della lista.
-   *Reimposta la password" per resettare la password di gestione se l'avete dimenticata.
-   *Se la vostra lista è pubblica avrete anche il link agli archivi pubblici della lista


<a name="close"></a>

Come chiudere il proprio account mail dal Pannello Utente
-------------------------------------------------------------

Cliccando sul *Account Management* e poi su *Chiusura Account* si aprirà questa schermata:

![](/static/img/man_userpanel/it/close.png "Chiudere il proprio account")

cliccando su "Disabilita questo account" la vostra mailbox verra' disabilitata.

Questo significa che:

smetterà istantaneamente di ricevere e inviare posta (anche tramite eventuali alias)

nel giro di un paio di giorni tutti i messaggi presenti nella mailbox e non scaricati
saranno cancellati, in modo irreversibile.

la rubrica (se utilizzata con la webmail) restera' conservata nei nostri backup.

la mailbox e gli alias resteranno comunque presenti nel nostro sistema di gestione degli utenti
in modo che nessun altro possa richiedere un account con lo stesso indirizzo e ricevere in futuro messaggi
che erano indirizzati a voi.

I dati che saranno conservati sono:

* l'indirizzo email principale e i suoi alias
* la data di creazione dell'account di posta
* la domanda (non criptata) e la risposta di recupero (criptata)
* la password della mailbox (criptata)
* i codici OTP, se impostati (criptati)

Se invece volete una rimozione completa scrivete a [helpdesk](mailto:helpdesk@autistici.org)

