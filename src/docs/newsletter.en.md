title: Newsletter Howto
----

# Newsletter Howto

## What is a newsletter

A **newsletter** is essentially a list of mail addresses to which mails are sent automatically by a software installed on the server.

Unlike a mailing list, newsletters are set as a one way communication,
essentially they work as a **closed** mailing list.


## How to request a newsletter

To activate a newsletter you need to request it from
[services](https://services.autistici.org) and ask for a mailing list in the service request page,
then in the following page insert the newsletter name, the domain, the mail address of the owner and select
the checkbox "Use as a newsletter".

Once that the request is approved (it could take some days) you should receive a confirmation email with password
and the link to the newsletter management page.

Howto manage a newsletter
-----------------------------

If for example the newsletter is named NAME123, the link to the administration page is:

**https://noise.autistici.org/mailman/admin/NAME123**

To enable an email address to send messages to a newsletter you need to:

 - **add it as a subscriber**.
 - **remove the moderation flag**

This has to be done for every email address you want to authorize su send messages.


### General Options

In the *General Options* page you will be able to set your newsletter welcome page, its description, the welcome message for people subscribing to
it and many more basic stuff, the maximum allowed message size and so on.

Most of the default options will be fine, but you should know that you shall **never change the *host\_name* option** since it would break
your newsletter.


### Membership Management

The *Membership management* page will easily be one of the most visited page in your newsletter administrator experience.

To subscribe several adresses you should go to the *Mass subscribing* section, where you can add - one per line -
a list of emails to subscribe to the newsletter. You will also be able to write a short welcome message to the ones you are subscribing.

Please do not subscribe hundreds of email addresses without their explicit consent.   In the mass subscription page you can choose "invite"
instead of "subscribe" in the checkbox so that users will receive an invitation messages with a link to click to complete the subscription.

![](/static/img/man_newsletter/AI-bulk-subscription.png "Mass subscription")

In the *Membership List* subpage you'll find a list of all the mail subscribed to the newsletter along with a series of option for each one of
them:

- *mod* refers to moderated users, so people that not allowed to send messages to the newsletter.
        Only the authorized people should be not moderated, so for example you should set as not moderated your email address and/or that
        of other people authorized to send messages. Note: the email of the admin of the newsletter is not automatically authorized to
        send messages, so even this address should be subscribed to the newsletter and not moderated.

![](/static/img/man_newsletter/AI-newsletter-moderato.png "Moderation")

If you want to unsubscribe a single user, just click on the *unsub* box next to his/her mail and then on *"Submit your changes"*.

![](/static/img/man_newsletter/AI-newsletter-cancella.png "Unsubscribe")


### Privacy Options

In the *Sender filters* page, the default action for messages from moderated users is to hold messages, you can set it to discard
so that you are not notified about these messages.
In the sendr filter page you can also authorize addresses not subscribed to the newsletter to send messages:
look for the "List of non-member addrresses whose postings should be automatically accepted" field and add the email address.




For details, see the Mailman manual:

**https://www.gnu.org/software/mailman/mailman-admin/index.html**
