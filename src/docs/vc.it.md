title: Videoconferenze con Jitsi Meet
----

Videoconferenze con Jitsi Meet
===============================

Benvenuta nel servizio di videoconferenze Jitsi Meet di Autistici/Inventati!

Per avviare una videoconferenza, puoi usare sia un browser, preferibilmente Chromium o Firefox, sia l'app di Jitsi Meet [per desktop](https://github.com/jitsi/jitsi-meet-electron) o [per dispositivi mobili](https://github.com/jitsi/jitsi-meet).

Puoi avviare o unirti a una videoconferenza con qualunque di questi strumenti, a prescindere dal modo in cui si stanno collegando gli altri.

Nota: le videoconferenze su vc.autistici.org funzionano bene per qualche decina di utenti, fino a un massimo di 35 circa. Per un numero di utenti maggiore, o per situationi in cui la connettività è limitata, disattivare le videocamere e usare solo l'audio può migliorare la funzionalità del servizio.

## Videoconferenze via browser

Per avviare una videoconferenza, vai al sito [https://vc.autistici.org/](https://vc.autistici.org/).

Puoi avviare una videoconferenza con un nome generato casualmente cliccando semplicemente il pulsante "Go" nel riquadro "Start a new meeting".

Se preferisci dare un nome alla tua stanza, inserisci il nome prescelto nel riquadro "Start a new meeting" prima di cliccare il pulsante "Go".

Il browser ti chiederà di autorizzare l'uso del microfono e della telecamera. Autorizza l'uso di entrambi per poter parlare e farti vedere dagli altri partecipanti.

Una volta entrata nella tua stanza, puoi invitare altre persone a partecipare copiando l'URL nella barra degli indirizzi del browser, oppure cliccando il tasto "Invite more people": si aprirà una finestra in cui troverai un pulsante per copiare il link da condividere con gli altri, oltre ad altri metodi per mandare l'invito.


## Collegarsi con l'app

Per collegarti con l'app, l'unica cosa che devi fare è configurare l'indirizzo del server, che troverai nelle impostazioni: nel campo "URL del server" inserisci: https://vc.autistici.org per usare il servizio di Autistici/Inventati.


## Trasmetti la tua videoconferenza in diretta su live.autistici.org

Il nostro servizio di videoconferenza ti permette anche di trasmettere il tuo seminario o la tua riunione in diretta sul nostro servizio di streaming [live.autistici.org](https://live.autistici.org).

Questo può tornare utile, per esempio, nel caso in cui stai organizzando un evento con un numero limitato di persone che devono parlare e un numero molto più alto di ascoltatrici e ascoltatori. In casi del genere puoi avere fino a 35 persone che parlano nella videoconferenza su [vc.autistici.org](https://vc.autistici.org) e un pubblico più vasto sul tuo canale su [live.autistici.org](https://live.autistici.org).

*Nota: se vuoi proteggere la tua videoconferenza con una password, dovrai far partire lo streaming prima di attivare la password.*

Ecco come funziona:

1. Avvia una videoconferenza seguendo le istruzioni qui sopra.
2. Clicca i tre puntini in basso a destra per aprire le impostationi.
3. Clicca "Start live stream" o "Inizia una diretta".

    ![](/static/img/man_jitsimeet/jitsimeet-startlivestream.png "Avvia la diretta")

4. Inserisci la chiave del tuo stream. Per esempio se vuoi trasmettere il tuo seminario su https://live.autistici.org/#ilmiofantasticostream, qui dovrai inserire "ilmiofantasticostream".

    ![](/static/img/man_jitsimeet/jitsimeet-enterkey.png "Inserisci la chiave per trasmissione in diretta")

5. Clicca il tasto "Start live stream" o "Inizia una diretta" e il tuo stream partirà immediatamente: un'icona azzurra con la scritta "LIVE" comparirà nell'angolo in alto a destra dello schermo.

    ![](/static/img/man_jitsimeet/jitsimeet-livestream.png "Live stream")

6. Quando vuoi interrompere la diretta, torna alle impostazioni e clicca "Stop live stream" o "Interrompi la diretta".

    ![](/static/img/man_jitsimeet/jitsimeet-stoplivestream.png "Stop live stream")



