title: Manuali per l'anonimato
----

Manuali per l'anonimato
=======================

Sempre più spesso si sente parlare di sistemi di controllo ad alto livello che tracciano, registrano e catalogano un'infinità di
informazioni riguardanti le attività di chiunque stia in rete, di sistemi, insomma che violano palesemente il diritto/bisogno fondamentale
della privacy.

Il vostro vicino di scrivania o il vostro capo in ufficio può facilmene controllare cosa state facendo in rete, siti commerciali tracciano
le vostre attività per indagini di mercato senza chiedervi un esplicito consenso, la vostra posta può essere letta da chiunque abbia accesso
alle macchine del vostro provider ecc. ecc.

Per evitare tutto ciò, per evitare che le nostre attivià in rete siano tracciate e registrate, è possibile navigare con un anonymizer come
Tor e crittare la posta con GPG.

Per aiutarvi a tutelare la vostra privacy, abbiamo scelto di pubblicare tra i nostri materiali alcuni manuali per gestire le chiavi di
crittazione e per installare e configurare Tor sul vostro computer.

- [Guida generale per la protezione della privacy della posta](/docs/mail/privacymail "Privacy Mail")
- [Navigazione anonima e hidden services](/docs/anon/tor "Guida Tor")
- [Crittazione della posta](http://www.gnupg.org/documentation/guides.it.html "GnuPG guides")
- [Posta anonima](https://remailer.paranoici.org) "Anonymous Remailer")


