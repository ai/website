title: Navigazione anonima con Tor 
----

Navigazione anonima con Tor
===========================

Uno degli strumenti che più ci sembrano raccomandabili per tutelare il proprio
anonimato in rete è [Tor](https://www.torproject.org), una rete di tunnel
virtuali che permette di navigare in Internet senza rivelare la propria
identità.

Per navigare in modo anonimo utilizzando Tor, dovete installarvi [Tor
Browser](https://www.torproject.org/projects/torbrowser.html.en) (disponibile
per tutti i sistemi operativi), un browser già configurato per la navigazione
anonima.

Tutto quello che vi serve sapere è sul [sito ufficiale di
Tor](https://torproject.org/documentation.html)

Considerate, comunque, che Tor da solo non è garanzia di anonimato: ci sono dei
rischi di cui essere consapevoli e degli accorgimenti da prendere, tutti
illustrati [qui](https://www.torproject.org/download/download.html.en#Warning).


