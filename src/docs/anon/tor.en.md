title: Tor Howto
----

Tor How-to
==========

One method we strongly advise for surfing the web anonymously is
[Tor](https://torproject.org), a network of virtual tunnels allowing people to
browse the Web without revealing their identity.

To surf the net anonymously using Tor, you have to install [Tor
Browser](https://www.torproject.org/projects/torbrowser.html.en) (available for
all operating systems): a browser configured for anonymous browsing.

You can find detailed instructions [here](https://torproject.org/documentation.html).

Consider however that Tor by itself is not all you need to maintain your anonymity.
There are risks you need to be aware of and adjustments you need to make, all
listed [here](https://www.torproject.org/download/download.html.en#Warning).



Configuring Thunderbird to send and receive email via Tor
---------------------------------------------------------

#### **Context**

While this approach is not recommended by torproject, here we provide a few steps to use Tor to communicate with our mailservers.
This might be a good option in situations where one is not able to access our mailservers directly (for example because of state censorship).

**Disclaimer:** this guide does not explain how to send anonymous emails (for that see how to use our [anonymous remailer][ar]), nor these configurations
enhance the anonymity of the messages you send. Connecting to our services through Tor is a way to obfuscate from the network provider you are
using that you are connecting to our servers. This does neither make you more anonymous than you already are
in our regards, because you have to authenticate to our servers to be able to use them, or shields you from unwillingly send identifying information
to your correspondents through mail (i.e. in some mail header).
If you have such needs, we suggest to either send the email via the webmail (via Tor, if you want to be anonymous in your navigation for your
network provider) or to use [Tails][t], configuring your email account in [Thunderbird offered in that distribution][tt].

[ar]: https://remailer.paranoici.org
[t]: https://tails.boum.org/
[tt]: https://tails.boum.org/doc/anonymous_internet/thunderbird/index.en.html


#### **Prerequisite 1: having a running Tor instance on your computer**

Tor is a program that exposes a local proxy to connect to the internet (and also to the network of hidden services). Tor Browser is a
modified flavour of Firefox, crafted to work better with the Tor proxy. It ships with a working Tor proxy. Whenever you turn on Tor Browser,
a local instance of Tor proxy is started.
Nevertheless, you can run Tor proxy independently from the Tor Browser. If you are able to do so, go ahead. Otherwise, to use Thunderbird through
Tor, you will need to first start Tor Browser.

#### **Prerequisite 2: configure the account**

First of all, you need a working Autistici/Inventati email account (if you are not able to access directly our mailservers, and did not verify
if the account is working, just go ahead).
Let us add a new account, click on the main menu, then select `New`, and then `Existing Mail Account...`

![](/static/img/man_mail_tor/en/00_create_new_1.png)
![](/static/img/man_mail_tor/en/00_create_new_2.png)

Then you will be prompt with a box, where you can put the name you want to display, the email and the password.

![](/static/img/man_mail_tor/en/01_new_account.png)

Clicking on `Continue` will trigger Thunderbird to search for the mailserver. In case of success the box will appear like this

![](/static/img/man_mail_tor/en/02_new_account_server_data.png)

If the mailservers are not retrieved, just insert these parameters

<table class="table">
    <thead>
        <tr>
            <th scope="col" style="text-align: center">protocol</th>
            <th scope="col" style="text-align: center">server</th>
            <th scope="col" style="text-align: center">port</th>
            <th scope="col" style="text-align: center">SSL</th>
            <th scope="col" style="text-align: center">authentication</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td scope="col" style="text-align: center">IMAP</th>
            <td scope="col" style="text-align: center">mail.autistici.org</th>
            <td scope="col" style="text-align: center">485</th>
            <td scope="col" style="text-align: center">STARTTLS</th>
            <td scope="col" style="text-align: center">Normal password</th>
        </tr>
        <tr>
            <td scope="col" style="text-align: center">SMTP</th>
            <td scope="col" style="text-align: center">smtp.autistici.org</th>
            <td scope="col" style="text-align: center">587</th>
            <td scope="col" style="text-align: center">STARTTLS</th>
            <td scope="col" style="text-align: center">Normal password</th>
        </tr>
    </tbody>
</table>

and modify the username to insert your **full email**, clicking on `Manual config`.

![](/static/img/man_mail_tor/en/03_new_account_fix_username.png)

#### **Configure Thunderbird to use Tor as proxy**

Now you should have configured Thunderbird with your A/I account.

![](/static/img/man_mail_tor/en/04_new_account_folders.png)

Let's modify how Thunderbird connects to the server. Rembember that this is a **global configuration**: **every account** you have on Thunderbird
will be used **through Tor**. Go to the menu, then select `Preferences`, and then again `Preferences`.

![](/static/img/man_mail_tor/en/09_network_settings_1.png)
![](/static/img/man_mail_tor/en/09_network_settings_2.png)

In the new windox, select `Advanced` in the left column, then click on the `Network & Disk Space` tab.

![](/static/img/man_mail_tor/en/09_network_settings_3.png)

On that tab, click `Settings` in the `Connection` section. In the new window, select `Manual proxy configuration` and enter
the `127.0.0.1` in the `SOCKS Host` field and. Now, if you are using a local Tor proxy instance, insert `9050` on the same row in the `Port` field,
otherwise, if you are running Tor Browser, insert `9150`.
Now, select also `SOCKS v5` below. Make also sure to tick the `Proxy DNS when using SOCKS v5` box.

![](/static/img/man_mail_tor/en/10_network_settings_box.png)
