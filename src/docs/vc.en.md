title: Jitsi Meet Videoconferences
----

Jitsi Meet Videoconferences
===============================

Welcome to Autistici/Inventati's Jitsi Meet videoconferencing service!

To start a videoconference, you can use either a browser, preferably Chromium or Firefox, or the Jitsi Meet [desktop](https://github.com/jitsi/jitsi-meet-electron) or [mobile app](https://github.com/jitsi/jitsi-meet).

You can start or join a videoconference with any of these tools, whatever apps the other people in the meeting are using.

Note: vc.autistici.org videoconferences work fine for some dozens users, up to a maximum of about 35 connections. For a higher number of participants, or in case of low connectivity, it can be useful to disable webcams and only use voice.

## Connecting with a browser 

To start a videoconference, go to [https://vc.autistici.org/](https://vc.autistici.org/).

To start a videoconference with a randomly generated name, just click the "Go" button in the "Start a new meeting" field.

If you'd rather give a specific name to your room, enter this name in the "Start a new meeting" field before you click the "Go" button.

Your browser will ask you to allow vc.autistici.org to access the computer's microphone and webcam - click "Allow" in both cases to be able to talk and show your face to the other participants.

Once you've entered your room, you can invite more people to join by copying the link in the URL bar of your browser. You can also click the "Invite more people" button: in such case, you will see a new window with a button to copy the link and other options to share the invitation with other participants.


## Connecting with the app

To connect to Autistici/Inventati's videoconference service with the Jitsi Meet app, you just need to set the server URL: go to the app settings and enter https://vc.autistici.org in the "server URL" field.


## Stream your videoconference on live.autistici.org

Our videoconference service also offers you an option to stream your workshop or meeting directly to our streaming service [live.autistici.org](https://live.autistici.org).

This can be useful, for example, when you would like to have a lot of participants in your online event, but not everybody is expected to talk. In such case, you can have up to 35 speakers in your videoconference room on [vc.autistici.org](https://vc.autistici.org), and a much larger audience on your channel on [live.autistici.org](https://live.autistici.org).

*Note: if you want to password-protect your videoconference, you need to first start streaming and then add a password to the conference.*

Here's how it works:

1. Start a videoconference following the instructions above.
2. Click the 3-dots icon to open the settings.
3. Click "Start live stream".

    ![](/static/img/man_jitsimeet/jitsimeet-startlivestream.png "Start live stream")

4. Enter your stream key. For example, if you want your stream to be available at the address "https://live.autistici.org/#myawesomebroadcast", your stream key should be "myawesomebroadcast".

    ![](/static/img/man_jitsimeet/jitsimeet-enterkey.png "Enter stream key")

5. Click the "Start live stream" button and your stream will start. A blue "LIVE" icon will appear in the top right corner of your screen.

    ![](/static/img/man_jitsimeet/jitsimeet-livestream.png "Live stream")

6. When you want to stop streaming your meeting, go back to your settings and click "Stop live stream".

    ![](/static/img/man_jitsimeet/jitsimeet-stoplivestream.png "Stop live stream")


