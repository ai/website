title: Documentation for list admins
----


To manage a mailing list see:

- [Mailing List](/docs/mailinglist)



To manage a newsletter see:

- [Newsletter](/docs/newsletter)
