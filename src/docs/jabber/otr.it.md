title: Guida a OTR per Jabber
----

Guida a OTR per Jabber
=============================

*[pagina ufficiale OTR](https://otr.cypherpunks.ca/)*

Il supporto OTR (Off-the-Record Messaging) permette di aggiungere all'instant messaging le seguenti funzioni:

- **Crittazione:** Sarà più difficile leggere i vostri messaggi.
- **Autenticazione:** si può star certi che il proprio corrispondente è proprio quello che si pensa che sia, dopo aver verificato la prima
  volta l'identità dell'interlocutore.
- **Possibilità di smentita:** I messaggi che si inviano non hanno firme digitali verificabili da una terza parte. Alla fine di una
  conversazione chiunque potrebbe modificare un messaggio per far credere che provenga da voi. Tuttavia, durante una conversazione con OTR
  il vostro corrispondente avrà la sicurezza che i messaggi che vede sono autentici e non sono stati modificati.
- **Segretezza nell'inoltro:** Se si perde la propria chiave privata, non verrà compromessa nessuna conversazione precedente.

Come usare OTR con Pidgin
-------------------------

Installare e usare OTR è semplicissimo: basta scaricarlo [qui](http://www.cypherpunks.ca/otr/index.php#downloads) (se usate Windows, cercate
Win32 Installer) e installarlo sulla propria macchina (curando di aver installato, e chiuso, Pidgin prima). (OTR è disponibile in molte
repository Linux).

A questo punto aprite Pidgin e cliccate su "Plugin" nel menu "Strumenti".

![](/static/img/man_jabber/it/otr1.png)

Vi si aprirà una finestra con un elenco di plugin da abilitare: cercate il plugin "Off-the-Record Messaging" e spuntate la casella
corrispondente.

![](/static/img/man_jabber/it/otr2.png)

Da questo momento le vostre comunicazioni saranno crittate, e potrete scegliere il comportamento che OTR deve assumere con ogni singolo
contatto cliccando con il tasto destro sul contatto e selezionando l'opzione "OTR Settings".
