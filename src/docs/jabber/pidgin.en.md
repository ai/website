title: Pidgin Jabber Configuration Howto
----

Pidgin Jabber Configuration Howto
=====================================

First of all download [Pidgin (Windows and Linux)](http://pidgin.im/download/) or install it from your Linux distribution repository.

Pidgin is the client we most heartily recommend since it's the only one allowing you to use [OTR
support](http://www.cypherpunks.ca/otr/#docs) and to encrypt your Jabber communication to the safest level.

After downloading and installing the software, you will be prompted with a similar window:

![](/static/img/man_jabber/it/pidgin1.png)

Click on the "Account" menu and on "Manage":

![](/static/img/man_jabber/it/pidgin2.png)

Click on "Add" and you will be prompted by a "Add an Account" window:

![](/static/img/man_jabber/it/pidgin3.png)

In this window you have to select "XMPP" from the dropdown list.

In our example we will be configuring Pidgin for the user: 'nomeutente@inventati.org'.

In the 'Public Name' field fill in your username without the domain (without '@inventati.org' in our example). In the 'Domain' field fill

in the '@domain.org' part of you username ('@inventati.org' in our example).

In the 'Resource' field fill in whatever suits you (you can also leave it empty). While in the 'Local nickname' field fill in the name you
want the other to read when they are contacting you.

It would be better not to fill in your password in the configuration window: you can type it every time you connect to our Jabber server
with Pidgin.

Now you can move to the "Advanced" tab
Mark the checkbox "SSL/TLS" and fill in "5222" in the "Connection Port" field, and "jabber.autistici.org" in the "Server" field. Save your
configuration and you are ready to go.

![](/static/img/man_jabber/it/pidgin4.png)

Now you will be back to Pidgin starting window: type your password and check your user tab to connect to our Jabber server (you can also
configure your client to automatically connect at startup).

### Now take care of your privacy!!!

Encryption as we manage it on the server side does not ensure the privacy of your jabber communication and third parts could always try to
sneak on you. We suggest you to install the [Jabber OTR plugin](http://www.cypherpunks.ca/otr/#docs) allowing you to manage directly the
encryption between you and your friends, comrades, and partners.
