title: Come configurare Adium (solo per Mac OSX) per Jabber
----

Come configurare Adium (solo per Mac OSX) per Jabber
===========================================================

Installate [Adium](http://www.adium.im) e lanciatelo. Selezionate il menu: Adium--&gt;Preferences...

Compare il pannello delle Preferenze.

![adium panel preferenze](/static/img/man_jabber/it/adium-01-accounts.png)

Cliccate sul segno più (in basso a sinistra) e scegliete Jabber.

Compare il pannello dell'account.

![adium panel accounts](/static/img/man_jabber/it/adium-02-jabber_account.png)

Nella casella "Jabber ID" inserite il vostro indirizzo e-mail; è preferibile non inserire la password in questa schermata (dove viene
memorizzata) ma digitarla ad ogni connessione a jabber.

Cliccate su "Personal" ed inserite un nick ed (eventualmente) una immagine

![adium panel personal](/static/img/man_jabber/it/adium-03-preferences-personal.png)

Cliccate su "Options" ed inserite il nome del server "jabber.autistici.org", la porta 5222 e spuntate la casella "Use TSL Encryption...".

![adium panel options](/static/img/man_jabber/it/adium-04-preferences-options.png)

Ci sono molte altre cose che si possono configurare ma queste possono bastare per iniziare. Cliccate su "OK", inserite la password e
cominciare a chiacchierare con jabber :)
