title: Come configurare PIDGIN per jabber
----

Come configurare PIDGIN per jabber
==================================================

[Scarica Pidgin (Windows e Fedora Core)](http://pidgin.im/download/) o installalo dalle repository.

Pidgin è il client che più di tutti vi consigliamo perché permette di usare il [supporto OTR](/docs/jabber/otr) e quindi di
crittare le proprie comunicazioni.

Una volta installato il programma, lanciatelo e vi troverete questa finestra:

![](/static/img/man_jabber/it/pidgin1.png)

Cliccate sul menu "Account" e poi su "Gestisci".

![](/static/img/man_jabber/it/pidgin2.png)

Cliccate su "Aggiungi" e vi si aprirà la finestra "Aggiungi Account":

![](/static/img/man_jabber/it/pidgin3.png)

Nella finestra "Aggiungi Account" selezionate "XMPP" nel menu a tendina. L'esempio mostra la configurazione per un utente che ha l'email:
nomeutente@inventati.org.</br> Nella finestra "Nominativo pubblico" inserite il vostro nome utente senza dominio (la parte che sta prima
della chiocciola della vostra e-mail), nella casella "Dominio" scrivete il dominio (la parte che sta dopo la chiocciola). Nella casella
"Risorsa" potete scrivere quello che volete (anche nulla!) e nella casella "Pseudonimo locale" scrivete il nome che preferite sia
visualizzato per identificarvi. È preferibile non inserire la password in questa schermata (dove viene memorizzata) ma digitarla a ogni
connessione a jabber.

Infine passate alla scheda "Avanzate"

Spuntate la casella "Use "Richiedere SSL/TLS" e inserite 5222 nel riquadro "Porta per la connessione" e "jabber.autistici.org" nel riquadro
"Server di connessione", infine salvate le impostazioni cliccando su "Salva".

![](/static/img/man_jabber/it/pidgin4.png)

A questo punto tornerete alla casella iniziale e non dovrete fare altro che digitare la password e cliccare sulla casella di spunta accanto
al vostro utente per collegarvi (ma potete anche impostare il client in modo che si colleghi all'avvio).

### Adesso pensa alla tua privacy !!!

La crittazione lato server (quella gestita da noi) non garantisce da eventuali e decisamente possibili violazioni della sicurezza delle
nostre macchine da parte di terzi. Suggeriamo quindi di installare, per una maggior privacy, sitemi di crittazione personali (gestiti
direttamente da voi).

Per Jabber, potete usare [il plugin OTR](/docs/jabber/otr).
