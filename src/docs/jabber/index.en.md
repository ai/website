title: Jabber Howto
----

Jabber Howto
============

Jabber (these days usually called XMPP) is an instant messaging
protocol, automatically available to all A/I users.

Some of its features include:

- We keep no logs on our servers
- Encrypted connections (among network users) are possible
- Supports [OTR](https://otr.cypherpunks.ca/)

General parametres for Jabber client configuration
--------------------------------------------------

Your Jabber ID is the same as your A/I email address.

No further configuration should be needed with any modern
XMPP client, the autodiscovery mechanisms will automatically
tell your client which server to connect to.

Jabber clients
--------------

We wrote some manuals to configure some of the major Jabber clients available (images are only in Italian, but we guess you can understand that much)

- [Pidgin](/docs/jabber/pidgin) (windows and linux) and [OTR encryption plugin](http://www.cypherpunks.ca/otr/win-install/otr-setup.html).
- [Conversations](/docs/jabber/conversations) (android)
- [CoyIM](/docs/jabber/coyim) (windows, mac and linux)
- [Adium](/docs/jabber/adium) (mac)
- [Psi](/docs/jabber/psi) (windows, mac e Linux)

Help
----

The service has been active, in Beta stage, from the 25th December 2005. If you have problems or recommendations or if you want to give us a
hand for an adequate documentation, get in touch via our [helpdesk](https://helpdesk.autistici.org).

About the implementation
------------------------

The service uses the [Prosody](https://prosody.im) free XMPP server.

