title: PSI Jabber Client Configuration Howto
----

PSI Jabber Client Configuration Howto
=========================================

Install [psi](http://psi-im.org/download) and launch it: it will prompt you with the "Add Account" panel.

![](/static/img/man_jabber/it/psi-01.png)

In the "Name" field fill in your e-mail address (username@domain.org) and click on "Add". Now the software will prompt you with the "Account
properties" panel.

![](/static/img/man_jabber/it/psi-02.png)

In the "Jabber ID" field fill in your e-mail address and click on the "Connection" tab.

It would be better not to fill in your password at this stage (because it would be saved in your Preferences and thus in your filesystem):
you can always type it every time you launch Psi.

![](/static/img/man_jabber/it/psi-03.png)

Mark the checkbox "Use SSL encryption" and in the "Host" field fill in "jabber.autistici.org".

Check that the connection port is "5222" and move on to the "Preferences" tab.

![](/static/img/man_jabber/it/psi-04.png)

Mark the "Ignore SSL warnings", and save your configuration. Now you can launch Psi, type your password and chat using jabber via Psi
client. :)
