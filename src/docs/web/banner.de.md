title: Banner
----

Banner
======

Gehostet von
------------

zum Veröffentlichen auf deiner von A/I gehosteten Werbseite

- ![](/static/img/banners/hostedby/icon-hostedby.png)
- ![](/static/img/banners/hostedby/icon-noblogs.org.png)
- ![](/static/img/banners/hostedby/icon-webcounter.png)

Du kannst zum Beispiel den folgenden Code einbinden:

 &lt;!-- LOGO Hosted by AUTISTICI-INVENTATI --&gt;
 &lt;a href="http://www.inventati.org" target="_blank" rel="noopener"&gt;&lt;img src="http://www.inventati.org/static/img/banners/hostedby/icon-hostedby.png"&gt;&lt;/a&gt;
 &lt;!-- LOGO AUTISTICI-INVENTATI - END --&gt;


- - -

Historisch
----------

- ![](/static/img/banners/hostedby/host_bianco.png)
- ![](/static/img/banners/hostedby/host_blue.png)
- ![](/static/img/banners/hostedby/host_nero.png)

### +kaos Kampagne

- ![](/static/img/banners/hostedby/125x125kaos.gif)
- ![](/static/img/banners/hostedby/310x60kaos.gif)

### alte "gehostet von"-Logos

- ![](/static/img/banners/hostedby/ai_bianco.png)
- ![](/static/img/banners/hostedby/ai_nero.png)
- ![](/static/img/banners/hostedby/autistici_button.png)
- ![](/static/img/banners/hostedby/inventati_button.png)
- ![](/static/img/banners/hostedby/banner_sparIAmo.gif)

- - -

Kampagnen
---------

- ![](/static/img/banners/campagnaR_120x120.gif)
- ![](/static/img/banners/campagnaR_468x60.gif)
- ![](/static/img/banners/pianoR_120x120.gif)
- ![](/static/img/banners/pianoR_468x60.gif)
- ![](/static/img/banners/NoBlogs_120x120.gif)

- - -

Andere Banner
-------------

siehe unsere Propaganda-Abteilung: <http://www.inventati.org/propaganda>


