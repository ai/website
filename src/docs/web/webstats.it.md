title: Manuale sulle statistiche per siti/blogs
----

Manuale sulle statistiche per siti/blogs
===============

A/I utilizza un sistema di statistiche web basato su [matomo](http://matomo.org) e che può essere utilizzato per conoscere le statistiche di
qualsiasi sito o blog ospitato su A/I e su Noblogs.

Ecco come attivarle:

1.  Se avete un sito statico, accedete al vostro [pannello utente](https://accounts.autistici.org) e cliccate sulla rotella accanto al nome del vostro
    sito, e poi cliccate sul link "Analytics".
2.  Incollate nel footer di una qualsiasi pagina del vostro sito (subito prima di &lt;/body&gt;) il codice che si visualizza nella
    schermata, quello compreso tra:

        <!-- Piwik -->

    e

        <!-- /Piwik -->

3.  A questo punto se cliccate su **"Visualizza le statistiche complete"** accederete alla schermata delle statistiche aggiornata in tempo
    reale, ma attenzione alla data, che si visualizza di default al giorno prima e che si può comunque modificare.
4.  Se invece avete installato un CMS come Wordpress, ingegnatevi o implorateci: magari ci inteneriamo e proviamo a trovare la soluzione che
    fa per voi. :)

I blog della piattaforma di **Noblogs** non hanno bisogno di attivare le statistiche: per gli utenti della nostra piattaforma di blogging
sarà sufficiente cliccare sul link **WP-PIWIK** nella propria bacheca per accedere alle statistiche.


