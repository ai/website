title: Caratteristiche tecniche dell'hosting di siti web 
----

Caratteristiche tecniche dell'hosting di siti web 
================================================

Questa pagina elenca le caratteristiche dello spazio web offerto da
Autistici/Inventati.

Potete trovare maggiori informazioni anche nelle nostre
[FAQ](/docs/faq/) e nelle nostre pagine dedicate al [servizio di
hosting web](/services/website).

## URL del sito

A meno che non abbiate un vostro dominio, la URL principale a cui è
sempre raggiungibile un sito è:

> `https://www.autistici.org/sito/`

(o www.inventati.org). A/I usa un proxy per redirigere automaticamente
i vostri visitatori al server dove è effettivamente ospitato il vostro
sito.

## Spazio occupato

Lo spazio web a disposizione su A/I non è sottoposto a dei limiti
rigidi. Questo non vuol dire che lo spazio non debba essere usato in
modo responsabile: a tale scopo nel Pannello è sempre riportato il
totale dello spazio occupato su tutti i nostri server, tenerlo basso
aiuta ad alleggerire il carico delle nostre strutture!

## PHP / MySQL

Non offriamo più la possibilità di usare PHP per nuovi siti.

## Script CGI

Per via di come sono organizzati i servizi, al momento A/I **non**
fornisce la possibilità di eseguire script CGI, o in generale ospitare
siti dinamici.


## Siti dinamici, chiariamo una cosa!

Siamo felici di fare un'eccezione per il vostro progetto ma vorremmo chiarire preliminarmente alcune cose per evitare incomprensioni :)


* almeno 2 persone si dovranno occupare tecnicamente del sito (idealmente queste dovrebbero essere in contatto diretto con noi) per tutto il periodo di esistenza del progetto.


* dovrete autonomamente mantenervi informati sullo sviluppo di nuove versioni del CMS che avete scelto e gli aggiornamenti dovranno essere fatti tempestivamente dal rilascio di una nuova versione, in special modo per gli aggiornamenti riguardanti la sicurezza.


* non installate plugins che facciano statistiche, né che si colleghino a servizi commerciali che fanno tracking (quindi no social media, servizi google, o altri similari).


* non permettiamo la presenza di sponsors o di pubblicità di alcun genere, o di collegamenti ad attività commerciali, partiti politici, organizzazioni sindacali o altre entità istituzionali. Se avete dubbi scrivete a help@autistici.org.


* se il progetto dovesse essere abbandonato o non fosse più possibile seguirlo dovrete avvisarci, possibilmente in anticipo.


* ci aspettiamo che il sito non venga violato o compromesso per carenza di manutenzione o altre negligenze. Se vi accorgete di irregolarità, avete domande, dubbi o avete bisogno di consigli scrivete a help@autistici.org.


* al venir meno di una di queste condizioni il sito sarà chiuso e staticizzato.


Vi ricordiamo che:


* se usate un dominio vostro, sarete contattati direttamente da terze parti (siano esse entità private, avvocati o le autorità) se queste lo ritenessero necessario. Non figurando noi come proprietari del dominio, non siamo nemmeno nella lista dei relativi contatti.


* come per tutti gli altri servizi di A/I, voi siete gli unici responsabili dei contenuti che pubblicate.


* anche nel vostro caso vale sempre la nostra policy https://www.autistici.org/who/policy e chi non la rispetta rischia di essere cancellato definitivamente dai nostri server, senza preavviso.


* possiamo garantire la continuità dei nostri servizi online solo a livello di buone intenzioni, ma la nostra priorità è garantire la sicurezza e la privacy, non l'uptime o la performance.


* in generale non forniamo supporto tecnico di alcun genere a chi non usa i nostri servizi standard ma ovviamente rispondiamo alle domande e cerchiamo di indirizzare il vostro lavoro quando abbiamo tempo. Il fatto che al vostro progetto sia stato consentito di poter utilizzare i nostri server in modo "non standard" non significa che esista anche un'accordo formale o informale di collaborazione o che il vostro progetto goda di condizioni privilegiate rispetto agli altri utenti di A/I (policy, manifesto, trattamento dei dati personali, (in)esistenza di copie di backup, altre condizioni del servizio, gestione delle credenziali di accesso, tempi di risposta alle mail o altro), e questo indipendentemente dal vostro eventuale supporto, sia che venga fatto tramite donazioni in denaro o di altro genere. 

