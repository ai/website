title: Web hosting
----

# Documentation for users of the A/I web hosting service

* [Technical notes about the service](/docs/web/tech_web)
* [Domain registration and DNS setup instructions](/docs/web/domains)
* [Using WebDAV to access your website](/docs/web/webdav)
* [Some notes about users' privacy](/docs/web/privacyweb)

