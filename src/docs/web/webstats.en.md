title: Statistics for websites and blogs
----

Statistics for websites and blogs
=====================================

A/I runs a statistics service for subsites, domains and blogs hosted on our servers. This service is based on [Matomo](http://matomo.org).

To activate statistics for your website:

1.  If you have a static website, log into your [user panel](https://accounts.autistici.org) and click on the wheel next to your website name,
    then click on **Analytics**
2.  In the the footer of any page in your site (immediately before &lt;/body&gt;), paste the code you see in the new window, starting from:

        <!-- Piwik -->

    up to

        <!-- /Piwik -->

3.  On the same page you will see an excerpt of your website actual statistics. If you click on the **"SHOW COMPLETE STATS"** link you'll
    enter the statistics page updated in real time; consider that by default the date is anticipated by one day, though you can modify it.
4.  If you have installed a CMS like Wordpress, you'll have to hack around or to beg us: perhaps you'll move us to charity and we'll try to
    find the solution you need. :)

**Noblogs** blogs do not need to activate statistics in the above mentioned way: you'll only need to click on the **"WP-PIWIK"** link on
your blog dashboard.


