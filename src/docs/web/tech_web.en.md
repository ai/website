title: Technical features of Website hosting
----

Technical features of website hosting
======================================

In this page you'll find the main technical features of A/I website
hosting.

Please check out also our [FAQ](/services/website) pages for more
docs.  If you still haven't found what you are looking for, try our
[General FAQ](/docs/faq/) or [write to us](mailto:info@autistici.org).

## Site URL

Unless you have your own domain, the main URL where your site can
always be reached is:

> `https://www.autistici.org/site/`

(or www.inventati.org). A/I uses a proxy to redirect automatically
your visitors to the server where your site actually is hosted.

## Space usage

The web space offered by A/I has no strict limits. This doesn't mean
that you can drop any responsibility while using your space: to keep
update you will always be reminded in your user panel about the total
space you are using in our servers. The less space you use, the
smaller will be the load on our structures!

## PHP / MySQL

We do not offer PHP support for new websites anymore.

## CGI scripts

Due to the way services are organized, at the moment A/I **does not**
offer the possibility of running CGI scripts.
