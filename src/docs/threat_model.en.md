title: Your Threat Model
----

Your Threat Model
===================

Only you can know what you want to protect and from whom
----------

Often, in requests for new services, we find sentences like: "I'm asking for a mailbox on A/I because I want to protect my privacy", or "I need it to be anonymous".

The truth is we can't promise anything like that - while a mailbox (or other services) on our servers can certainly be a step to safeguard your privacy or anonymity, it is by no means the first.

The first step you need to take, even before you switch on your computer, is asking yourself a couple of questions:

- What do you want to protect by using our services?
- From whom do you want to protect it?
- What to you know about these people/entities?
- Why would they want to surveil you or know who you are?
- What means have they used so far to do similar thing with others?
- What resources do they have? Do they have strong allies?
- Would they use these resources or these allies to get directly at you?

Here's a couple of examples:

### Would you like a service with A/I to protect your privacy?

We don't analyze your contact network or your tastes, and we don't try to sell you to a marketing company, but we can't protect you from someone whose business model is the violation of your privacy. If you use an Autistici/Inventati mailbox to create an account on a commercial service you regularly access with your usual browser, and if, even worse, you are always connected to this service and haven't installed plugins that can protect your privacy, then having a mailbox on our servers won't help much in attaining your goal.

If you want to protect your privacy, you should first review the online services you use, and possibly switch to less invasive alternative services. It is also a good idea to install an anti-tracking plugin in your browser, like [Privacy Badger](https://www.eff.org/privacybadger), together with an extension to block ads like [uBlock Origin](https://github.com/gorhill/uBlock)
([Chrome](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm)
- [Firefox](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)).


### Would you like to stop unauthorized persons from reading your communications?

If you are writing from your mailbox to another mailbox on A/I, you can generally rely on the fact that nobody will read your emails apart from the person you've sent them to.

But since some of the people you write to will certainly use commercial services that read their users' emails for targeted ads, a mailbox with A/I is not enough to protect your conversations. Besides, if you are discussing particularly sensitive topics, other kinds of people (often wearing a uniform) might be interested in reading your email. If you want to be user that nobody but you and the people you write to reads your mail, we recommend you use GPG at least to encrypt the most delicate messages.

You can find clear explanations on email encryption on [Security in a Box](https://securityinabox.org/en/guide/secure-communication/).


### What you need is anonymity

It's true: we don't keep track of your IP address and cannot know who you are and where you connect from. But if you want to protect your anonymity, a mailbox with A/I is only one of the steps you need to take.

First of all, you should have a clear sense of what you mean by anonymity and who you want to hide your official identity from. If you just want your friends to call you by a different name than the one on your documents, what you need is a pseudonym. For this purpose, a mailbox on A/I can be enough.

But if you want to hide your official identity from the police or other powers, you'd better install the [Tor Browser](https://www.torproject.org/) and carefully read the [recommendations to use it correctly](https://www.torproject.org/download/download.html.en#warning).

If a disclosure of your identity could have unbearable consequences for you, we recommend you get a better protection by using 
[Tails](https://tails.boum.org/index.en.html) for the most sensitive activities.

If you just need to send completely anonymous messages, you can use 
anonymous remailers. You can learn more about them starting from [this page](https://www.autistici.org/services/anon).


### Threat modeling for the most delicate situations

Before you start using your mail account or other services on our services for sensitive activities, **we strongly recommend** you assess your risks and plan how to reduce them through a *threat modeling* exercise.

To learn how to do it, you can start from this [guide by the EFF](https://ssd.eff.org/en/module/assessing-your-risks).
