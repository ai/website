title: Statistiche per siti e blog ospitati
----

Statistiche per siti e blog ospitati
===========================================

Dopo anni di richieste, preghiere e minacce, oggi anche gli appassionati utenti di Autistici/Inventati possono finalmente usufruire di un
servizio di statistiche degli accessi ai loro contenuti.

Il nuovo servizio di statistiche web di A/I copre sia i siti ospitati sui nostri server, sia i blog di [noblogs.org](http://noblogs.org) ed
è basato su [matomo](http://matomo.org), che è software libero.

Attivare uno strumento di questo tipo all'interno di una realtà come A/I non è una scelta facile, perché in genere preferiamo astenerci dal
raccogliere i dati degli utenti, ma in questo caso abbiamo valutato i pro e i contro e hanno vinto i pro.

Il fatto è che raccogliere statistiche significa controllare tutte le visite alle pagine web dei siti in questione e verificarne la
provenienza e questa attività si può spesso associare a una profilazione degli utenti, profilazione che spesso si associa a indagini di
mercato e alla conservazione dei log. Ma a differenza dei servizi commerciali come googleads, nel nostro caso prendiamo in esame soltanto
alcuni particolari, come i link da cui provengono le visite, mentre non teniamo in alcun modo gli ip, gli indirizzi che associano le visite
registrate a persone in carne e ossa, e abbiamo anche eliminato dal software delle statistiche plugin più intrusivi come quelli che
verificano il paese da cui provengono le visite e le dimensioni dello schermo del visitatore.

Inoltre, utilizzando plugin che ostacolano la profilazione come Adblock per Firefox, i visitatori possono evitare di essere rilevati da
piwik. Questo ha per il gestore del sito lo svantaggio di non poter conteggiare tutti i visitatori, ma ha il vantaggio di garantire a chi
naviga nel sito la scelta di non essere rilevato. Potete comunque avvertire i vostri amici di disabilitare Adblock per il vostro particolare
sito/blog e il problema sarà in parte aggirato.

Se invece è a voi che non piace l'idea di profilare i vostri lettori, non temete: piwik non viene attivato automaticamente bensì solo per
scelta del singolo amministratore del sito/blog.

Le nostre statistiche sono attive automaticamente sui blog di [Noblogs.org](http://noblogs.org), ma dovete **scegliere di attivarle sui siti
che ospitate da noi, usando il nostro simpaticissimo [howto](/docs/web/webstats)**

Considerate comunque che le statistiche sono pubbliche e che quindi chiunque può visualizzarle. Questo ha per risvolto che potete anche
pubblicarle voi sul vostro sito, usando tutta una serie di appositi widget.

In ogni caso vi raccomandiamo caldamente di usare piwik se in questo momento state usando un contatore commerciale, che come avrete già
letto nella nostra [policy](/who/policy) va assolutamente evitato negli spazi di A/I in quanto contrasta con la tutela
dell’anonimato che tentiamo il più possibile di garantire.


