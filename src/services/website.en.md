title: Website hosting
----

Web Site Hosting
================

A/I Collective gives you the possibility to publish your own (static) web site.

The [criteria](/who/policy) for participating and enjoying the services offered by our servers are sharing our
**principles of anti-fascism, anti-racism, anti-sexism, anti-militarism** that foster this project.
Your website must share the non-commercial approach we support.

Your (static) site will generally be visible as a **subdirectory** of autistici.org or inventati.org (e.g.: www.autistici.org/mysite or
www.inventati.org/yoursite) and you'll be given a login name and password to upload via [WebDAV](/docs/web/webdav).
your files.

If your prefer, you can host a [**domain**](/docs/web/domains) of your own choosing (but you will have to purchase
it from a commercial provider).

You will find a lot of useful information on how to run a website on A/I servers in our howtos and manuals: <a name="howto"></a>

- [How to host your own domain on A/I servers](/docs/web/domains "how to host your own domain on A/I servers")
- [How to use webdav to update and upload your site](/docs/web/webdav "how to use webdav to update and upload your site")
- [A very short reminder about webmasters' privacy and webusers' data](/docs/web/privacyweb "a very short reminder about webmasters' privacy and webusers' data")
- [Some very frequently asked questions on sites hosted on A/I](/docs/faq/)
- [How to activate statistics on your site](/docs/web/webstats "Howto activate statistics on your site")
- [Technical notes on A/I hosted websites](/docs/web/tech_web "technical notes on A/I hosted websites")
- [Howto add A/I banners to your A/I hosted website](/docs/web/banner "Howto add A/I banner on your website")

An email account on our servers has to be connected to each site for technical communications and news on the web hosting services. So, if
you don't have one, you will have to open a mailbox account as well as request website hosting.

Now that you have read all about our static web hosting service, go on and **[ask us to open your website](/get_service)** but
remember that this project lives on exclusively thanks to our users **[donations!](/donate)**

Please be sure to check out our **[help page](/get_help)** and **the above mentioned manuals** for any problem you
might encounter (webdav access, web statistics, domain registration included especially).
