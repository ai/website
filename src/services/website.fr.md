title: Hébergement de Site Web
----

Hébergement de Site Web
=======================

Le collectif A / I vous donne la possibilité de publier votre propre site web.

Les [critères](/who/policy) pour participer et profiter des services offerts par nos serveurs sont de partager nos
**valeurs sur l'anti-fascisme, l'anti-racisme, l'anti-sexisme, l'anti-militarisme**, qui sont portées par ce projet.

Votre site web doit partager l'approche non-commerciale que nous soutenons.

Votre site sera généralement visible comme un sous-répertoire de autistici.org ou inventati.org (par exemple : www.autistici.org/monsite ou
www.inventati.org/votresite) et il vous sera donné un nom d'utilisateur et un mot de passe pour télécharger via
[WebDAV](/docs/web/webdav).

Si vous préférez, vous pouvez héberger un [domaine](/docs/web/domains) de votre choix (mais vous devrez l'acheter
auprès d'un fournisseur commercial).

Vous trouverez une foule d'informations utiles sur la façon de lancer un site web sur les serveurs A / I dans nos howtos et manuels :

<a name="howto"></a>

- [Comment héberger votre propre nom de domaine sur les serveurs A / I](/docs/web/domains "how to host your own domain on A/I servers")
- [Comment utiliser WebDAV pour mettre à jour et télécharger votre site](/docs/web/webdav "how to use webdav to update and upload your site")
- [Un très court rappel sur la vie privée et les données des WebUsers et webmasters](/docs/web/privacyweb "a very short reminder about webmasters' privacy and webusers' data")
- [Quelques questions très fréquemment posées sur les sites hébergés sur A / I](/docs/faq/)
- [Comment activer les statistiques sur votre site](/docs/web/webstats "Howto activate statistics on your site")
- [Notes techniques sur les sites web hébergés sur A / I](/docs/web/tech_web "technical notes on A/I hosted websites")
- [Comment ajouter des bannières A / I à votre site hébergé sur A / I](/docs/web/banner "Howto add A/I banner on your website")

Un compte de messagerie sur nos serveurs doit être connecté à chaque site pour les communications techniques et les nouvelles sur les
services d'hébergement Web. Donc, si vous n'en avez pas, vous devrez ouvrir un compte de messagerie en même temps que la demande
d'hébergement de site Web.

Maintenant que vous avez tout lu sur notre service d'hébergement Web, allez-y et **[demandez-nous d'ouvrir votre site Web](/get_service)**
mais n'oubliez pas que ce projet vit exclusivement grâce aux **[dons](/donate)** de nos utilisateurs !

S'il vous plaît, n'oubliez pas de consulter notre **[page d'aide](/get_help)** et **les manuels mentionnés ci-dessus**
pour tout problème que vous pourriez rencontrer (accès WebDAV, base de données, les statistiques web, enregistrement de domaine inclus en
particulier).


