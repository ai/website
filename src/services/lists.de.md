title: Mailinglistem und Newsletter
----

Mailinglistem und Newsletter
==============================================

## Viele Wege zum Führen einer On-line Diskussion

Es gibt viele Arten, Diskussionen und Debatten im Internet zu führen: unter den weit verbreitetsten Werkzeugen dafür bietet das A/I
Kollektiv **Mailinglistem** und **Newsletter**.

Bevor du weiterliest, versichere dich, dass du unsere [Policy](/who/policy) gelesen hast und denke daran, dass unser Projekt auf
freiwilliger Basis der Kollektivmitglieder realisiert und zudem ausschließlich mit [Spenden](/donate) der User finanziert wird!

Mailinglistem
-------------

Eine Mailingliste ist im Grunde genommen eine Liste von E-Mail-Adressen, zu dem ein Mensch Nachrichten schicken möchte: eine Nachricht wird
an eine spezifische Adresse unserer Server gesandt und ein Programm versendet sie weiter zu den E-Mail-Adressen der Liste.

Eine Mailingliste kann für jede\_n offen sein (**offene** Liste) oder kann sich auf die eingetragenen E-Mail-Adressen beschränken. Es gibt
sogar die Möglichkeit einer **geschlossenen** Liste.

Weiterhin können die Archive der Listennachrichten öffentlich im Web zugänglich sein (**öffentliche** Liste) oder nur für jene zugänglich,
die ein Passwort dazu haben (**private** Listen). In jedem Fall werden die Nachrichten allen auf der Liste eingetragenen E-Mail-Adressenen
direkt in die Mailbox gesandt.

Eine wichtige Warnung zur Privatsphäre: **Nachrichten, die über eine öffentliche Liste geschickt werden, tauchen früher oder später bei
Google und anderen Suchmaschinen auf**; also sei sorgsam wohin du deine Nachrichten verschickst.

Mailinglistem sind eine sehr nützliche Kommunikations- und Koordinationshilfe, besonders für Projekte, deren Mitglieder sehr weit
voneinander weg sind. Es gibt keine festgeschriebenen Regeln, wie auf einer Liste geschrieben wird, aber informier dich ruhig über die
lokalen Angewohnheiten und die Nettiquette.

Abhängig davon, wie die Liste konfiguriert ist, wirst du gebeten, deine Eintragung per E-Mail zu bestätigen oder auf eine Erlaubnis des
Listenadministrators zu warten.

Solltest du **eine neue öffentliche oder private Mailingliste öffnen** wollen, geh bitte zu unserer
[Request-Seite](/get_service).

## Newsletter

Bitte hab im Hinterkopf, dass du einen Newsletter und keine Mailingliste beantragst, wenn du **periodisch Neuigkeiten über dich oder deine
Organisation verschicken** möchtest. Bitte klick im Formular an die richtige Stelle.
