title: Списки листування й розсилання
----

# Списки листування й розсилання

## Обговорювати щось у мережі можна по-різному

Існує чимало шляхів проводити обговорення й дебати в інтернеті. A/I надає одні з найпоширеніших засобів для такої діяльності: **списки листування** й **розсилання.**

Перш ніж продовжити, обов'язково перечитайте [нашу політику](/who/policy) й пам'ятайте, що наш проєкт живе далі винятково завдяки користувацьким [пожертвам](/donate) і волонтерській роботі учасниць і учасників колективу!

Списки листування
-----------------

Список листування — це по суті перелік адрес е-пошти, кому хтось бажає доставити повідомлення: пошта надсилається на певну адресу на наших серверах — і програмне забезпечення доставляє її всім переліченим обліковим записам.

Список листування може приймати дописи як від будь-кого (**відкриті** списки), так і лише від підписаних чи навіть наперед визначених адрес (**закриті** списки). До того ж, архіви листування бувають як доступні всьому інтернету для читання (**загальнодоступні** списки), так і обмежені власницями й власниками паролів підписки (**приватні** списки). Хай там як, листи буде доставлено прямо в поштові скриньки всіх підписаних на список чи доданих у нього.

**Важливе застереження про приватність:** якщо список загальнодоступний, надіслані через нього повідомлення рано чи пізно опиняться в Google та інших пошукових системах, тож уважно стежте за тим, куди надсилаєте листи.

Списки листування — дуже корисні засоби для спілкування й координації, особливо для проєктів, у яких беруть участь багато людей на великій відстані одне від одного. Строгих загальних настанов про дописування в список немає, але слід дотримуватись місцевих звичаїв та етикету.

Деякі списки мають **загальнодоступні архіви:** [архів загальнодоступних списків](https://lists.autistici.org/splash/index.en.html).

Якщо **бажаєте відкрити новий загальнодоступний чи приватний список,** просимо [перейти до сторінки запиту на послуги](/get_service).

## Розсилання

Пам'ятайте: якщо **бажаєте засіб для розсилання періодичних новин** про свою діяльність чи діяльність своєї організації, **вам слід попрохати саме про розсилання,** а не про листування. Оберіть у формі запиту на список правильну опцію!
