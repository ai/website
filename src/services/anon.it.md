title: Servizi per l'anonimato
----

# Servizi per l'anonimato

In questa sezione proponiamo i nostri servizi specificamente pensati per tutelare l'anonimato de\* nostr\* utenti.

Probabilmente se sei entrat\* in questa parte del sito già hai riflettuto su quanto sia prezioso il tuo anonimato in rete, ma visto che
queste cose è sempre meglio ripeterle, vale la pena di mettere l'accento sui motivi per cui noi incoraggiamo caldamente a salvaguardare il
proprio anonimato in tutti i modi possibili.

C'è ad esempio chi pensa che certi servizi siano un po' eccessivi, che sia paranoico pensare che qualcuno ci controlli mentre navighiamo in
rete o quando scriviamo messaggi di posta. In realtà le intrusioni sono molto più frequenti di quanto si pensa normalmente, e chi pensa di
"non aver niente da nascondere" non sarebbe poi tanto content\* di sapere che le sue mail spedite non crittate possono essere lette da
curios\* di ogni genere e che la sua navigazione in Internet viene seguita per scoprire a che nicchia di mercato appartenga.

Per questo vi invitiamo a informarvi sui nostri servizi di anonimato e a leggere il nostro [blog](https://cavallette.noblogs.org), che
spesso dà notizie su nuovi programmi di difesa della privacy e sugli attacchi più mirabolanti al nostro anonimato.

Potete anche consultare i manuali che abbiamo scritto a proposito di privacy e servizi di anonimato nella nostra [apposita sezione di howto](/docs/anon/).

Anonymous Remailer
------------------

Un anonymous remailer è sostanzialmente un modo per nascondere la propria identità quando si invia un messaggio di posta elettronica. Un
messaggio inviato tramite un anonymous remailer non può essere ricondotto al suo autore e per questo non può ricevere risposta.

Per maggiori informazioni potete consultare [la pagina web del nostro remailer](https://remailer.paranoici.org).

Nym Server
----------

Un nymserver offre identità sotto pseudonimo di lunga durata. E' basato su una rete di anonymous remailer ma permette la creazione di
indirizzi di posta che possono inviare e ricevere posta in forma anonima.

Per maggiori informazioni potete consultare [la pagina web del nostro remailer](https://remailer.paranoici.org/nym.php).
