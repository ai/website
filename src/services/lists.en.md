title: Mailing lists and newsletters
----

# Mailing lists and newsletters

## Many ways to carry on an on-line discussion

There are many ways to carry on discussions and debates on the internet: among the most widespread tools for such an activity A/I Collective
provides **mailing lists** and **newsletters**.

Before proceeding further be sure to have read [our policy](/who/policy) and remember our project survives solely thanks
to our users [donations](/donate) and to the collective members voluntary work!

Mailing lists
-------------

A mailing list is basically a list of email addresses to which one wants to deliver messages: a mail is sent to a specific address on our
servers and a software delivers it to all the accounts included in the list.

A mailing list can be open to anyone to write (**open** lists) or limited to subscribed mail addresses, or even to some specific ones
(**closed** lists). Furthermore archives of the list messages can be available on the web for anyone to read (**public** lists) or limited
to those wielding a subscription password (**private** lists). In any case the messages will be delivered to all those subscribed (ie:
included) in the list directly in their mailboxes.

**Important warning on privacy**: mind that if a list is public, the messages sent through it will end up sooner or later on Google and
other search engines, so think carefully where are you sending your emails.

Mailing list are very useful communication and coordination tools especially for projects involving people very far away one from the
others. There are no fixed guidelines on how to write on a list but be sure to check out local customs and etiquette.

Some lists have **public archives**: [public list archive](https://lists.autistici.org/splash/index.en.html).

If you **want to open a new public or private list** please [proceed to our request page](/get_service).

## Newsletters

Keep in mind that if you **want a tool to send periodic news** around about your or your organization activities **you'll want to ask for a newsletter** and not
simply for a mailing list. Be sure to check the right option in our form!
