title: Video Conference and Streaming
----

Video Conference and Streaming
===============================

In the spring of 2020 Autistici/Inventati started offering a videoconference and a streaming service.

<a name="VC"></a>

Videoconference: Meet Jitsi
----------------------

A/I offers VoIP and video conferences on [vc.autistici.org](https://vc.autistici.org), a videochat platform based on open source software [Jitsi Meet](https://github.com/jitsi).

With Jitsi Meet, you can start a videoconference by connecting to the service with a common browser, without having to install anything in your computer. What you need to do is just open the website [vc.autistici.org](https://vc.autistici.org) (preferably with [Chromium](https://www.chromium.org/getting-involved/download-chromium)), and click the "Go" button to start a videoconference. Once the videoconference has started, you will just need to share the URL with the people you want to talk to.

You can find detailed instructions on Autistici/Inventati's videoconferencing service in this [guide](/docs/vc).

<a name="streaming"></a>

Streaming
--------------------------

Our streaming service can be found at [live.autistici.org](https://live.autistici.org).

In the home page, you will find basic instructions on how to use the service, and you can find out what is being currently streamed in [the stats page](https://live.autistici.org/stats).

You can also find a basic tutorial on how to start a stream on live.autistici.org [in our tech documentation](/docs/stream-obs).
