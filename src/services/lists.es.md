title: Listas de correo y Newsletter
----

# Listas de correo y Newsletter

## Diferentes formas de mantener un debate on line

Aquí existen varias formas de llevar a cabo discusiones y debates en internet: las herramientas más extendidas que provée el colectivo A/I
son las **listas de correo** y los boletínes o **newsletter**.

Antes de proceder, aseguraté de leer [nuestras políticas](/who/policy) y recuerda que nuestro proyecto subrevive
exclusivamente gracias a las [donaciones](/donate) de nuestr\*s usuari\*s y al trabajo colectivo de
nuestr\*s miembr\*s!

Listas de correo
----------------

Una lista de correo es básicamente una lista de direcciones de correo a la cual quieres enviar mensajes: tú envías un correo a una dirección
específica en nuestros servidores y el software lo entrega a cada una de las cuentas incluídas en esa lista de correo.

Una lista de correo puede estar abierta a la participación de cualquiera (lista **abierta**) o con suscripción limitada, o sólo para
direcciones específicas (listas **cerradas**). De todas formas, el archivo histórico de la lista puede estar disponible para la lectura de
cualquiera (listas **públicas**) o límitadas para aquellos que tienen la contraseña (listas **privadas**). En cualquier caso, los mensajes
son entregados a tod\*s l\*s suscriptor\*s (incluíd\*s) directamente en sus buzones de correo.

**Aviso importante de privacidad**: piensa que si una lista es pública, los mensajes enviados pueden, tarde o temprano, acabar en Google y
otros motores de búsqueda, así que piensa cuidadosamente lo que envías en esos correos.

Las listas de correo son una herramienta muy útil de comunicación y coordinación, especialmente para aquellos proyectos que involucran
gentes que se encuentran muy distante entre sí geográficamente. No existen unas pautas fijas sobre cómo escribir en una lista pero asegurate
de echar un vistazo a las normas locales y a la "netiquette".

Si **quieres abrir una lista nueva, pública o privada**, por favor [procede desde esta página](/get_service).

## Newsletters (boletínes)

Piensa que si lo que tú **quieres es una herramienta para enviar noticias periódicas** sobre tus actividades o las de tu organización **deberías
preguntar simplemente por la newsletter / boletín** y no por una lista de correo. Aseguraté de escoger la opción correcta en nuestro
formulario!

