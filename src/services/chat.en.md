title: Instant Messaging and Chat
----

Instant Messaging and Chat
==========================

Instant messaging and chat rooms are increasingly used all over the world, but the most widespread services are commercial and do not offer
any privacy measure.

The IRC network and Jabber service provided by A/I allow instead a free chatting time and at the same time a better level of anonymity
protection.

<a name="irc"></a>

Irc - Chat service
------------------

A/I offers its users the possibility to chat via IRC (Internet Relay Chat), a widespread chat system.

IRC is fairly easy to use and a lot of people get wound up in it very fast: that's why it's one of the most widespread tools of electronic
communication (more than e-mail by now).

The IRC network we offer is called **mufhd0** and is administered by A/I, [ECN](http://www.ecn.org) and [indivia](http://indivia.net)
projects together. You can access the network from each one of these project nodes via an IRC client as [X-Chat](/docs/irc/xchat), 
or [Irssi](/docs/irc/irssi) (Linux command line).

Among these tools, we strongly encourage X-Chat, since it allows for **encrypted connections via SSL** and also allows to connect to IRC
via [Tor](/docs/anon/tor).

To know how to configure a IRC client and what channels are available on our network, for more info on various IRC commands and services,
check out our **[howtos](/docs/irc/)**.

<a name="jabber"></a>

Jabber - instant messaging
--------------------------

Jabber (also known as XMPP) is a faster communication system for people
connected to the Internet. It's very similar both to chat and instant
messaging (like MSN, Google Talk, Yahoo IM, and so on), but it's actually
much more flexible.

Furthermore Jabber natively supports SSL connections and in some cases
it can be configured to manage communication through cryptographic keys
administered directly by users (GPG or OTR for example).  We suggest
[Pidgin and its OTR plugin as a client for this kind of usage](/docs/jabber/pidgin).

Every A/I user with a mailbox account has automatically a Jabber account with
the same username *user@domain.org* as its main email address.

The best way to chat via Jabber is to install a client in your system and
then configure it so as to encrypt your conversations with OTR.

To learn how to configure a Jabber client, to information on commands and
usage, as well as Jabber services, please refer to our
[Jabber manuals](/docs/jabber/).

