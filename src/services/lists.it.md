title: Servizi - Mailing list e newsletter
----

# Mailing list e newsletter

## Molti modi per discutere online

Ci sono molti modi per discutere e dibattere online su internet: tra gli strumenti più diffusi per questo tipo di attività A/I offre la
possibilità di aprire **mailing list** e **newsletter**.

Prima di procedere oltre, assicuratevi di aver letto la nostra [policy](/who/policy) e ricordatevi che il progetto A/I vive delle
sole [donazioni](/donate) dei propri utenti e del lavoro volontario dei membri del collettivo.

Mailing List
------------

Una mailing list è sostanzialmente una lista di indirizzi a cui automaticamente vengono inviati dei messaggi da un programma presente sul
server (la macchina che fornisce il servizio).

A seconda di come viene gestita la mailing list, vi possono scrivere soltanto gli iscritti (lista cosiddetta **chiusa**) o anche altri
(lista **aperta**), mentre in ogni caso i messaggi saranno ricevuti dai soli iscritti.

Una mailing list può essere **pubblica** o **riservata**, a seconda che i messaggi che transitano per essa siano anche pubblicati su web
(come nel nostro [archivio pubblico](http://lists.autistici.org).

Tenete sempre presente che se una lista è pubblica, **i messaggi che scrivete presto o tardi compariranno su Google e altri motori di
ricerca**, è importante avere sempre questa consapevolezza quando si scrive.

Le mailing list sono degli ottimi strumenti di comunicazione, di coordinamento su progetti che si svolgono con la partecipazione di soggetti
anche molto distanti tra loro, di approfondimento di alcune tematiche, se usate correttamente. Di fatto non esistono regole generali, ed
ogni lista ha i suoi usi e costumi che fanno parte della storia della comunità di quella lista.

Alcine liste hanno degli **archivi pubblici**: [archivi delle liste pubbliche su A/I](https://lists.autistici.org/splash/index.en.html).

Se volete **aprire una nuova lista pubblica o privata** [passate alla pagina di richiesta servizi](/get_service).

## Newsletter

Ricordatevi che se **avete bisogno di uno strumento per mandare informazioni periodiche a una serie di contatti dovreste richiedere una newsletter** e non una
normale mailing list. Controllate di cliccare sull'opzione corretta nel form per la richiesta dei servizi su A/I!
