title: Hosting di siti web
----

Hosting di siti web
==================

Il collettivo A/I vi dà la possibilità di pubblicare il vostro sito.

Le pregiudiziali per poter partecipare ai servizi offerti sui nostri server sono la condivisione dei **principi di anti-fascismo,
anti-razzismo, anti-sessismo, anti-militarismo e non commercialità** che animano questo progetto, oltre ovviamente a una buona dose di
volontà di condivisione e di relazione ;)))))

Il vostro sito sarà visibile come **sottodirectory del server autistici.org o inventati.org** (es: www.autistici.org/vostrosito oppure
www.inventati.org/vostrosito) e vi sarà attivato un [account WebDAV](/docs/web/webdav) per poter uploadare e gestire il vostro
materiale via [WebDAV](/docs/web/webdav).

Se preferite è anche possibile ospitare uno spazio web con un suo proprio [**dominio**](/docs/web/domains) (ma dovrete
acquistarlo da un provider commerciale).

Nei manuali e nella documentazione qui sotto potete leggere tutto ciò che vi serve per ospitare un sito su A/I: <a name="howto"></a>

- [Come ospitare il vostro dominio sui server di A/I](/docs/web/domains "come ospitare il vostro dominio sui server di A/I")
- [Come usare un account WebDAV per aggiornare il vostro sito](/docs/web/webdav "come usare un account dav per aggiornare il vostro sito")
- [Alcuni appunti sulla vostra privacy e su quella dei vostri visitatori](/docs/web/privacyweb "alcune brevi note sulla vostra privacy e su quella dei vostri visitatori")
- [Alcune FAQ (Domande molto frequenti) sui siti ospitati sui server di A/I](/docs/faq/)
- [Come attivare le statistiche per il vostro sito](/docs/web/webstats "come attivare le statistiche per il vostro sito")
- [Note tecniche sull'hosting di siti su A/I](/docs/web/tech_web "Note tecniche sull'hosting di siti su A/I")
- [Come aggiungere i banner di A/I al vostro sito](/docs/web/banner "come aggiungere i banner di A/I al vostro sito")

Ad ogni sito web sarà associata una casella e-mail sui nostri server, dove invieremo le comunicazioni tecniche e le novità relative al
servizio.

Ora che avete letto tutto quello che potevate sul nostro servizio di hosting di siti web, potete **[chiederci di aprirvene uno](/get_service)**,
ma ricordate che questo progetto è completamente **[autofinanziato](/donate)** e vive solo grazie al
supporto di tutti e al lavoro volontario dei membri del collettivo.

Per qualsiasi problema doveste riscontrare (accesso webdav, accesso ai database, statistiche, registrazione del vostro dominio soprattutto)
assicuratevi di aver consultato la nostra **[pagina di supporto](/get_help)** e **i manuali menzionati in questa pagina** prima di
disperarvi.
