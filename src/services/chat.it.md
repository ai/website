title: Instant Messaging e Chat
----

Instant Messaging e Chat su A/I
===============================

L'instant messaging e la chat sono sempre più diffusi e usati, ma quasi tutti i servizi a disposizione sono commerciali e non danno garanzia
di privacy.

La rete IRC e il servizio Jabber messi a disposizione da A/I consentono invece di chattare liberamente e, al contempo, permettono di
tutelare molto di più il proprio anonimato.

<a name="irc"></a>

Irc - Servizio di chat
----------------------

A/I offre ai suoi utenti la possibilità di chattare con IRC (Internet Relay Chat), uno dei sistemi di chat più utilizzati.

IRC è facile da usare e abbastanza coinvolgente e per questo è ormai diffuso anche più della mail come strumento di comunicazione
elettronica.

La rete IRC che vi proponiamo si chiama **mufhd0** ed è gestita assieme da A/I, [ECN](http://www.ecn.org) e [indivia](http://indivia.net).
Vi potete accedere tramite appositi client come [X-Chat](/docs/irc/xchat) (per Linux, Windows e OSX),
o [Irssi](/docs/irc/irssi) (un client a riga di comando per Linux).

Di questi, noi vi consigliamo caldamente X-Chat, che consente connessioni crittate tramite SSL e permette anche di collegarsi tramite
[Tor](/docs/anon/tor).

Per sapere come configurare un client e quali canali sono a disposizione sulla rete mufhd0 e per informazioni sui vari comandi IRC e sui
servizi offerti dalla rete, consultate i nostri [howto](/docs/irc/).

<a name="jabber"></a>

Jabber - instant messaging
--------------------------

Jabber (altrimenti noto come XMPP) è un sistema di comunicazione rapido tra persone in rete. È molto simile sia alla chat che all’instant messaging (MSN, o google Talk,
o Yahoo IM, ad esempio), ma in realtà è più versatile.

Inoltre Jabber ha nativamente un supporto per le connessioni in SSL e in alcuni casi può essere configurato per
gestire la comunicazione usando chiavi crittografiche gestite direttamente dagli utenti (il famoso GPG o OTR per esempio). Per questo
suggeriamo l'utilizzo di [Pidgin con il suo plugin per OTR](/docs/jabber/pidgin).

Ogni utente dei server di [A/I](http://autistici.org) ha automaticamente un account Jabber, accessibile usando
l'indirizzo email come username e la password dell'account email.

Per usare Jabber, la soluzione che ti consigliamo consiste nell'installare un client nel tuo sistema e configurarlo in modo da crittare le tue conversazioni con OTR.
Per sapere come configurare un client, per informazioni sui vari comandi e sui servizi offerti consulta la
[sezione dedicata a Jabber](/docs/jabber/) dei nostri manuali per utenti.

