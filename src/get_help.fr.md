title: Pas de panique!
----

Pas de panique!
===============

Page d'aide d'Autistici/Inventati
---------------------------------

Sur cette page nous allons essayer de vous dire ce que vous pouvez faire quand vous pensez avoir besoin de notre aide. Nous avons mis en
place quelques instruments pour tenter de réduire le nombre d'e-mails de demande d'aide à traiter. Avant de nous écrire **vous êtes
fortement incités à essayer de trouver la solution à votre problème ici.**

- [Cavallette](https://cavallette.noblogs.org/) - News?
- [FAQ - une série de questions fréquemment posées](/docs/faq/) (et leurs réponses, bien sûr!)
- [Manuels](/docs/) - une collection de manuels pour configurer différents logiciels pour utiliser nos services.
- [Helpdesk](http://helpdesk.autistici.org/) - ici vous pouvez nous laisser un message spécifique concernant votre problème et attendre
    une réponse qui viendra lorsque nous aurons le temps (il vous faudra bien sûr laisser un e-mail de contact pour recevoir la réponse!

Il est également possible de trouver certains d'entre nous en allant sur le chan **\#ai** du [serveur IRC d'autistici.org](/docs/irc/).
Là bas les gens pourront vous aider pour des petits problèmes techniques et
autre choses rapides.

<a name="gpgkey"></a>

Si vous êtes vraiment désespérés - mais avant de faire ça vous devez être sur le point de mourir d'une mort horrible - vous pouvez [nous écrire](mailto:info@autistici.org),
éventuellement en utilisant notre [clé GPG](gpg_key) (disponible sur les serveur de clés mondiaux).
