title: +KAOS - Il libro sui 10 anni di A/I
----

+KAOS - 10 Years of Hacking and Media Activism
==============================================

10 years after the creation of our [collective](/who/collective), we finally
managed to tell at least a part of our story, and what it meant to us.

After another few years, the English translation is finally available! :)

![+KAOS Book Cover Low Resolution](/static/img/book/ai-book-cover-en-web.jpg)

## About the book

At the end of the 20th century, hacking was bleeding edge. When the ideas, practices and pranks of this experimental niche of technophiles attracted the attention of a handful of activists in Italy, they understood that information and communication were what would give shape and voice to social, political, and cultural processes in the near future.

+KAOS is a cut and paste of interviews, like a documentary film transposed on paper. It describes the peculiar relationship between hacktivism and activism, in Italy and beyond, highlighting the importance of maintaining digital infrastructures. While this may not sound as glamorous as sneaking into a server and leaking data, it is a fundamental topic: not even the most emblematic group of hacktivists can operate without the services of radical server collectives.

## Download

* [DOWNLOAD THE BOOK HERE](https://networkcultures.org/blog/publication/kaos-ten-years-of-hacking-and-media-activism/)

The English translation of the book is published by the
[Institute of Network Cultures](https://networkcultures.org) in Amsterdam.

