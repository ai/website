title: Un breve racconto sul perché siamo chi siamo e perché facciamo quel che facciamo
----

Un breve racconto sul perché siamo chi siamo e perché facciamo quel che facciamo
================================================================================

Già quando scrivemmo il [nostro primo manifesto](/who/manifesto) il mondo non era un posto semplice in cui passare le giornate. Le
nostre prospettive erano più limitate, il nostro futuro iscrivibile nello stretto cerchio di un serpente che divora la propria coda. In quei
tempi A/I era un piccolo attracco di resistenza, di sopravvivenza. Volevamo contribuire con quanto sapevamo fare alla proliferazione della
piccola, ma tenace e testarda comunità disadatta al potere. Ci eravamo chiamate autistici, autistiche, paranoici perché eravamo consapevoli
di quanto il nostro atteggiamento rispetto all'uso della tecnologia, di quanto la nostra naturale diffidenza verso la retorica libertaria
della democrazia rappresentativa e alle sue inconsistenze repressive fosse radicale per quei tempi, tanto ottimisti quanto proni
all'accettazione del compromesso con le logiche di prevaricazione del capitalismo globale.

E, come quando abbiamo cominciato, il nostro approccio è prima di tutto politico, radicale e ostinatamente antiautoritario. Allora come
oggi siamo spesso costrette a definirci in maniera negativa, perchè per prima cosa occorre prendere le distanze: **antifascismo,
antirazzismo, antisessismo, antimilitarismo.** Nella nostra rada la protezione è data solo a chi osa ripararsi da quel vento, che purtroppo
soffia in tutta Europa.

**No business**, perché il tempo che vogliamo sia rappresentato in A/I sono i bei momenti rubati al lavoro affannoso e precario, le gioie,
le fatiche, il sudore e le lacrime delle lotte quotidiane per affermare le proprie idee nel mondo plasmato a immagine e somiglianza di una
moneta.
 In questi tempi flessibilmente dolorosi l'umanità è sostituita dal capitale umano in cui, si noti, la parola capitale sta per prima e umano
è soltanto un aggettivo. Dovendo recitare a forza su questo palcoscenico, la nostra esigenza primaria è unirsi, e contribuire con i mezzi
che possiamo mettere in campo alla sopravvivenza delle intelligenze critiche, in primis le nostre.

La tecnica nasce dalla capacità dell'essere umano di usare il proprio pollice opponibile, ma il capitalismo ha completato la trasmutazione
della tecnologia in credenza religiosa, in settore strategico di una linea di crescita e di controllo infinita, tanto illusoria, quanto
suicida.

Si consumano risorse per produrre oggetti, in breve trasformati in rifiuti. Si prelevano informazioni per produrre desideri, profitto,
potere e per creare paura, autocensura, rassegnazione. Bella esperienza vivere nel terrore, vero? In questo consiste la schiavitù.

Noi abbiamo soltanto voluto dire alle genti di allora come a voi: non fatevi intimorire, dignità, perseveranza, coraggio,
auto-organizzazione. E queste poche parole sono forse le sole importanti del nostro manifesto.
