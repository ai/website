title: Costes de la infrastructura
----

Costed de la infrasctructura
============================

Pensamos que es justo para todas las personas que cada año contribuyen y soportan nuestro trabajo con sus donaciones sepan cuánto cuesta la
infraestructura de autistici.org/inventati.org

En escencia, los costes se pueden dividir en **costes de conectividad** para cada máquina de la red A/I y **costes de mantenimiento** (que
incluyen costes de hardware, reemplazo de servidores, y sólo excepcionalmente costes de viajes para aquell<span class="red">\*</span>s que
vuelan a arreglar los desastres que ocurren con regularidad :)

**Costes de conectividad (por año)**

Alrededor de 10000 euros

**Costes bancarios (por año)**

Alrededor de 1000 euros

**Consultoría jurídica y fiscal (por año)**

Alrededor de 3400 euros

**Administración (por año)** \[ registro de dominios, contabilidad, etc.\]

Alrededor de 1500 euros

**Mantenimiento (por año)** \[ hardware, servidores, coste de viajes, impuestos legales,emergencias\]

Alrededor de 4400 euros

**TOTAL circa 20300 euros por año**

Esto significa que todos los años gastamos más o menos **20.300** euros para la supervivencia de la infraestructura.

Todo el trabajo que el colectivo hace para mantener la infraestructura es voluntario. **Sobrevivimos gracias a las donaciones que realizan las personas que usan nuestros servicios**.

Así que [dona](/donate) ahora!
