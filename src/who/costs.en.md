title: Infrastructure Costs
----

Infrastructure costs
=====================

We thought it fair for the people who support our work with their donations every year to know how much the autistici.org/inventati.org
infrastructure costs.

Costs can be split up in two main chapters: **connectivity costs** for each node of the a/i infrastructure and **maintenance costs**
(covering hardware costs, server replacements, and only exceptionally travel costs for those who wind up fixing the impeding disaster :)

**Connectivity (per year)**

about 10000E

**Banking costs (per year)**

about 1000E

**Legal and Fiscal consulence (per year)**

about 3400E

**Administration (per year)** \[ domain name registration, accounting, etc \]

about 1500E

**Maintanance (per year)** \[ hardware, server, travel costs, legal expenses, emergencies \]

about 4400E

**TOTAL circa 20300 euro per year**

This means that every year we spend more or less **20.300** euros for the survival of the infrastructure.

All the work the collective does for maintaining the infrastructure is voluntary work. **We survive thanks to the donations of people using our services**.

So [donate](/donate) now!
