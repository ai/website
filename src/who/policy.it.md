﻿title: Policy
----

Prefazione
==========

Le pregiudiziali per poter partecipare ai servizi offerti su questo server sono
la condivisione dei princìpi di **antifascismo, antirazzismo, antisessismo,
antiomofobia, antitransfobia, antimilitarismo e non commercialità** che animano
questo progetto, oltre ovviamente a una buona dose di volontà di condivisione e
di relazione e a un bel po' di pazienza.

Spazi e servizi di questo server non vengono destinati ad attività
(direttamente o indirettamente) commerciali, al clero, ai partiti politici
istituzionali o comunque, in sintesi, a qualunque realtà che disponga di altri
potenti mezzi per veicolare i propri contenuti, o che utilizzi il concetto di
delega (esplicita o implicita) per la gestione di rapporti e progetti.

Il server conserva solo i log strettamente necessari a operazioni di debugging,
che comunque non sono associabili in alcun modo ai dati identificativi degli
utenti dei nostri servizi. Per saperne di più su come gestiamo i tuoi dati,
ricordati di leggere la [privacy policy](/who/privacy-policy.it)

Termini di Servizio
================

13 aprile 2020

Per favore, leggi attentamente questo documento.

Questi Termini di Servizio (anche "TdS", "TOS" oppure "Termini" da qui in avanti)
costituiscono un accordo vincolante tra te (da qui in avanti l'"utente") e
l'associazione A/I-ODV (anche "A/I" o "Noi" di seguito). Accedendo alla nostra
piattaforma e utilizzando i nostri Servizi, l'utente accetta automaticamente i
presenti Termini. Tra i servizi di A/I si annoverano - tra le altre cose -
email, siti web, blog, mailing list, newsletter, irc, jabber, anonymous
remailer e nym server. Se stai agendo a nome di un'organizzazione, dichiari di
avere l'autorità di sottoscrivere questi Termini per conto di tale
organizzazione. Se non rispetti questi Termini, ti è precluso l'utilizzo dei
nostri Servizi.

A/I è una ODV, organizzazione di volontari per la diffusione della conoscenza e
la promozione di una coscienza sociale sulle tecnologie informatiche.

A/I è un associazione costituita unicamente da volontari, che non ricevono
nessuna forma di pagamento per il proprio impegno o di rimborso per le proprie
spese. Il progetto A/I è finanziato esclusivamente tramite donazioni spontanee.
I servizi sono completamente gratuiti e le donazioni che li rendono possibili
non sono né necessarie né sufficienti al fine di ottenere un'utenza gratuita.
Il nostro lavoro mira a mettere in pratica i princìpi della solidarietà e
dell'autorganizzazione.

Assicurati di leggere anche la nostra [Privacy Policy](/who/privacy-policy.it), che descrive il modo in
cui raccogliamo e processiamo i dati e le informazioni degli Utenti, in quanto
tale Policy costituisce parte integrante di questi Termini di Servizio.

Ti incoraggiamo a scriverci all'indirizzo e-mail
[associazione@ai-odv.org](mailto:associazione@ai-odv.org) nel caso tu abbia
dubbi o perplessità riguardo a questi Termini, o per avvisarci di eventuali
violazioni dei Termini da parte di altri Utenti di A/I.

### Richiesta di servizi

Per avere accesso ai nostri Servizi è di fondamentale importanza che tu legga e
comprenda il nostro [Manifesto](/who/manifesto) e le nostre policy, poiché spiegano nei dettagli
le nostre intenzioni. Per ottenere un'utenza su A/I devi valutare, prima di
iniziare la procedura di richiesta, se ti trovi d'accordo o meno con i princìpi
alla base di questo progetto.

Al momento di richiedere una nuova utenza viene richiesto di spiegare per
iscritto in che modo le intenzioni di utilizzo del richiedente siano in armonia
con i nostri princìpi. Nei casi in cui questo non sia possibile l'utenza non
viene erogata. A/I sceglie a quali individui concedere l'uso dei propri servizi
al fine di ridurre il rischio di violazione delle proprie policy. Richiedendo
di inviare una previa dichiarazione di intenti per iscritto, A/I è in grado di
valutare il grado di compatibilità di ogni nuova richiesta prima della
creazione della relativa utenza; ogni dichiarazione d'intenti viene valutata da
un membro di A/I in persona e viene cancellata al termine del processo.

Ogni dichiarazione scritta viene sottoposta a un processo di valutazione
completamente manuale, con esiti differenti a seconda dei casi. Nel corso della
valutazione il referente di A/I può fare domande specifiche relative al testo
della richiesta, interagendo direttamente con la persona richiedente su una
pagina web dedicata. Alla fine del processo, la richiesta può essere dichiarata
compatibile con i princìpi di A/I, con conseguente creazione di una nuova
utenza, oppure dichiarata incompatibile, nel qual caso la richiesta viene
respinta.  Tutta la corrispondenza relativa a questo processo di selezione
viene sempre cancellata al termine dello stesso, sia nel caso di esito positivo
che di esito negativo.  Scoraggiamo i e le richiedenti dal fornire informazioni
false durante questo processo, in quanto ogni utenza trovata in violazione di
questi Termini o delle altre policy sarà sospesa e cancellata senza preavviso.

### Utenti dei Servizi

Nel rispetto del GDPR e della Legge Italiana sulla Privacy, per richiedere una
utenza per i nostri Servizi devi aver compiuto i 14 anni di età. I Servizi sono
forniti esclusivamente a esseri umani. La registrazione di utenze da parte di
bot o di altri sistemi automatizzati è vietata e nel caso dovessimo rilevarne
l'esistenza, tali utenze saranno cancellate senza preavviso.

### Responsabilità sui contenuti

Gli utenti di A/I sono autorizzati a trasmettere contenuti online,
pubblicandoli o inviandoli privatamente attraverso i nostri Servizi. La maggior
parte di detti Servizi consiste in software che, operando in modo automatico e
senza supervisione, permette la trasmissione cifrata e non monitorata di
informazioni attraverso i nostri server. L'Utente è l'unica persona
responsabile dei dati memorizzati sulla nostra piattaforma e dei contenuti resi
disponibili attraverso i nostri Servizi, così come della loro appropriatezza,
attendibilità e aderenza ai dettami di legge. A/I non opera nessuna forma di
esame preventivo, filtro o verifica dei contenuti inviati dagli Utenti, né ha
la possibilità di modificare detti contenuti.

Ogni Utente è responsabile del proprio uso dei Servizi e per ogni conseguenza
derivante da tale uso.

In ragione della natura priva di finalità lucrative della nostra associazione,
possiamo fare affidamento solo sul lavoro delle e dei nostri volontari, 
quando questo sia possibile, in base alla loro disponibilità. 
Pertanto non siamo in grado di garantire l'appropriatezza o legalità dei contenuti 
e dei materiali distribuiti attraverso i nostri Servizi in qualsiasi momento. 
Ogni violazione o rimostranza può essere sottoposta all'attenzione di A/I 
via mail oppure attraverso il nostro sistema di ticketing, [helpdesk](https://helpdesk.autistici.org/).

### Norme di condotta e restrizioni

L'autorizzazione a usare i Servizi di A/I è soggetta alle restrizioni elencate
in questi Termini. All'Utente non è permesso l'uso improprio della propria
utenza, delle infrastrutture o dei Servizi per finalità incompatibili con i
nostri princìpi e le nostre policy, né è ammesso assistere, incoraggiare o
aiutare altri in tale uso improprio. A/I in particolare considera uso improprio
le seguenti attività:

1. promuovere il fascismo, il nazismo, il militarismo o il nazionalismo;
   contribuire alla discriminazione, alla persecuzione o al danneggiamento di
   qualsiasi individuo o gruppo di individui tramite forme di discriminazione
   basate sul genere, la razza, la religione o l'orientamento sessuale, come
   anche pubblicare dichiarazioni razziste, xenofobe, omofobe o discriminatorie
   per qualunque altro motivo;

2. inviare pubblicità o messaggi non richiesti, spam, mail spazzatura, mail di
   massa o mail a liste di persone che non abbiano esplicitamente acconsentito
   alla ricezione di tali mail da parte dei mittenti; rendere pubblico
   l'indirizzo mail del destinatario come indirizzo di risposta nell'uso di
   servizi di spam di terze parti; usare il proprio indirizzo mail come
   indirizzo di tipo "no-reply" per scopi commerciali;

3. pubblicare informazioni personali di terzi che non siano già pubblicamente
   disponibili, in particolar modo quando la pubblicazione avviene tramite
   violazione della sfera personale di un individuo o non costituisce legittimo
   diritto di delazione;

4. interferire con le configurazioni, il comportamento previsto o il
   funzionamento appropriato dei Servizi in maniera tale da danneggiare,
   impedire il funzionamento, sovraccaricare o pregiudicare i Servizi o da
   compromettere, incidere negativamente su o impedire ad altri utenti di
   usufruire pienamente dell'esatta funzionalità che i Servizi sono stati
   originariamente progettati per fornire; così come intercettare qualsiasi
   contenuto o informazione che Noi non abbiamo intenzionalmente reso
   disponibile, aggirare o invalidare qualsiasi controllo d'accesso che abbiamo
   implementato; tentare di fare "scraping", esfiltrare o raccogliere
   contenuti;

5. rivelare, vendere o acquistare le credenziali per l'uso dei Servizi, e
   commercializzare o ridistribuire qualsiasi Servizio A/I a terze parti
   (persona diversa da quella che ha effettuato la richiesta di utenza);

6. usare i Servizi allo scopo di procurarsi un profitto, come ad esempio, tra
   le altre cose, commerciare e gestire vendite, altri progetti a fini
   commerciali, imprese e qualsiasi altre attività a fine di lucro. Eccezioni
   possono essere fatte per quei progetti che antepongono il concetto di
   autoproduzione o il mutuo supporto e la solidarietà locale alla logica del
   capitale e del guadagno personale, quando ciò sia stato preventivamente
   documentato e discusso all'atto della richiesta del Servizio;

7. usare i Servizi per attività collegate alle criptovalute;

8. usare i Servizi per promuovere partiti politici istituzionali o qualsiasi
   altra organizzazione che abbia già le risorse finanziarie per diffondere i
   propri contenuti e le proprie idee, o che promuova (esplicitamente o
   implicitamente) il concetto di delega e rappresentanza nei rapporti
   quotidiani e nei progetti;

9. usare i Servizi per qualsiasi scopo di tipo militare, inclusa la diffusione
   di informazioni o materiale didattico riguardo all'uso di armi da fuoco e
   relative tecniche di combattimento, guerra cibernetica, sviluppo e
   produzione di armi;

10. violare o contravvenire a questi Termini in qualsiasi modo;

Ogni tipo di uso improprio o abuso può risultare nella cancellazione
dell'utenza senza preavviso.

Chiunque sia trovato in violazione di questi Termini sarà obbligato a risarcire
A/I in ragione di reclami di terze parti a causa del proprio comportamento,
incluso il rimborso delle spese che l'associazione debba sostenere per condurre
tutte le indagini adeguate alla ricerca di fatti o materiale probatorio e per
procurarsi un'assistenza legale nella propria difesa. Anche azioni frivole o
vessatorie ci danno il diritto di esigere un indennizzo, poiché ci impongono un
impegno lavorativo / economico irragionevole e uno spreco di tempo e risorse.

Inoltre, il tuo accesso e utilizzo dei Servizi di A/I è soggetto a questi
Termini e a tutte le leggi e i regolamenti applicabili. Ti informeremo
prontamente nel caso contese di natura legale dovessero presentarsi.

### Principi specifici ai servizi


#### ** Caselle di posta elettronica **

Non ci sono copie di backup dei tuoi dati, quindi tu solo sei responsabile di
scaricare la tua casella di posta elettronica localmente usando un client
email. Noi ci impegniamo solo a garantire la continuità del servizio di
consegna email (invio e ricezione).  Anche se non imponiamo limitazioni sulla
dimensione della casella email, devi mantenere un livello di archiviazione
ragionevole; per questo motivo ti invitiamo a limitare l'uso che fai dello
spazio sui nostri dischi, scaricando localmente o cancellando le tue email
quanto più spesso possibile.

Per usare la tua email devi scegliere una semplice domanda di sicurezza, in
modo da poter recuperare l'accesso alla tua casella di posta nel caso in cui tu
perda o dimentichi la password. Se dimentichi anche la domanda, o la risposta
alla domanda di sicurezza, non saremo in grado di rinviarti i dati di accesso
alla tua utenza. L'unica cosa che possiamo fare è creare una nuova utenza, ma
in nessun caso possiamo recuperare i dati di quella vecchia. Di conseguenza,
devi ricordarti di impostare la domanda di sicurezza, cliccando il link che
trovi nel tuo pannello utente. Nel creare un account, accetti di tenere la tua
password al sicuro e accetti anche tutti i rischi connessi all'accesso non
autorizzato al tuo account.


#### ** Siti web e blog **

Forniamo spazio web solo per contenuti statici. In alternativa puoi usare la
nostra piattaforma di blogging, [Noblogs](https://noblogs.org).

Lo spazio su server lasciato inutilizzato per più di 120 giorni
dall'attivazione sarà disabilitato e cancellato. Lo spazio su disco è limitato:
se hai bisogno di caricare grandi quantità di dati, devi contattarci
preventivamente.

Tra i nostri Servizi, forniamo uno strumento per le statistiche web, quindi
ogni altro strumento che funga a tale scopo è proibito.

Non è permesso l'uso di spazio a te assegnato come redirect ad altri siti.

La responsabilità per i contenuti dei siti è dei gestori dei siti stessi. I
gestori sono legalmente responsabili per i propri contenuti web. Ogni qualvolta
riceviamo un avviso legale da una fonte ufficiale, ci riserviamo il diritto di
condurre un'indagine riguardo ai termini dell'avviso, che può anche portare
all'oscuramento della pagina; in ogni caso possiamo considerare di discutere la
questione con le persone direttamente interessate, al fine di trovare la
soluzione più adeguata per le e i nostri Utenti nel rispetto dei nostri
princìpi e delle nostre policy.


### Violazione di diritto d'autore e proprietà intellettuale

Nel caso in cui fossimo avvisati di violazioni della proprietà intellettuale
sulla nostra piattaforma, quando le parti interessate ci avranno provvisto di
informazioni sufficienti e specifiche per identificare e individuare il
presunto materiale in violazione, provvederemo a rimuovere dalla nostra
piattaforma qualsiasi contenuto che violi brevetti, marchi, segreti
commerciali, diritto d'autore, diritti intellettuali o diritti simili di terze
parti. Dopo attenta revisione, A/I può rifiutarsi di rimuovere il presunto
materiale in violazione - ad esempio se riteniamo che il materiale non sia
effettivamente in violazione, se non disponiamo di informazioni sufficienti a
dimostrare che tale materiale sia in violazione, se non siamo in grado di
individuare detto materiale o se tale materiale è protetto dalla dottrina
dell'uso equo. Se sei detentore di diritti d'autore e ritieni che qualche
specifico contenuto su A/I violi i tuoi diritti (riguardo ai tuoi contenuti,
non quelli di altri), ti invitiamo a contattarci per email a
[associazione@ai-odv.org](mailto:associazione@ai-odv.org), descrivendo il problema.

### Chiusura dell'utenza

Se violi o non rispetti questi Termini, A/I ha il diritto di sospendere o
disabilitare il tuo accesso ai Servizi, senza preavviso. Qualsiasi abuso può
condurre a un'immediata chiusura dell'utenza.  Nel caso di un periodo di
inattività maggiore o uguale a 12 mesi, ci riserviamo il diritto di cancellare
la tua utenza A/I e i dati a essa associati.

### Limitazione di responsabilità

In quanto utente dei nostri servizi, e indipendentemente dal nostro impegno,
accetti che A/I (AI-ODV) non sia responsabile nei tuoi confronti o nei riguardi
di terze parti per qualsiasi danno di tipo diretto, indiretto,
preterintenzionale, specifico, conseguente o esemplare, inclusi, fra l'altro,
danni dovuti all'interruzione dell'attività, perdita di profitti, buona fede,
uso, dati e qualsiasi altra perdita di tipo immateriale, dovuta a o connessa
con l'utilizzo o l'impossibilità di utilizzo dei Servizi, anche nei casi in cui
la possibilità che tale danno avvenga ci venga notificata oralmente o in forma
scritta. In caso la legge vigente non permetta la limitazione o l'esclusione
della nostra responsabilità a danni imprevisti o consequenziali, la limitazione
o esclusione di cui sopra può non essere applicabile a te. Ciononostante la
nostra responsabilità sarà limitata nella maniera più ampia permessa dalla
legge in vigore.

Se hai una controversia con uno o più dei nostri utenti, accetti di sollevare
A/I da qualsiasi responsabilità e richiesta di danni (diretti e consequenziali)
di qualsiasi tipo e natura, noti e ignoti, che derivino o siano in qualsiasi
modo legati a tale controversia.

È responsabilità dell'utente tenere la propria password al sicuro e di
prevenire l'accesso da parte di terze parti al proprio account. Se
ciononostante una terza parte non autorizzata dovesse ottenere accesso
dell'utenza e utilizzare i Servizi in modo improprio, solo l'utente che ha
registrato l'account sarà ritenuto responsabile.

Inoltre, non riterrai responsabile A/I, né richiederai indennizzo, se materiali
riservati venissero diffusi non intenzionalmente come risultato di una falla di
sicurezza o di una vulnerabilità dei Servizi.

Noi non siamo responsabili per qualsivoglia malfunzionamento o ritardo che non
sia sotto il nostro ragionevole controllo.

### Dichiarazione di non responsabilità

Indipendentemente da quello che è il nostro impegno, dichiari di aver compreso
e accettato che A/I offre servizi via internet, che sono di conseguenza
soggetti a limitazioni nell'ambito della possibilità di accedervi, senza che
esista alcuna forma di garanzia da parte nostra nei tuoi confronti in merito. I
servizi vengono forniti "così come sono" e senza nessuna garanzia esplicita,
implicita o prescritta dalla legge. In particolare neghiamo qualsiasi garanzia
di titolarità, commerciabilità, adeguatezza per un particolare scopo o uso
conforme a termine legale. Non diamo alcuna garanzia di completezza,
accessibilità, accuratezza, adeguatezza, funzionalità o qualità dei nostri
Servizi.

A/I non garantisce in alcun modo che i Servizi soddisferanno le tue esigenze o
che saranno ininterrotti, performanti, sicuri o privi di errori, né che le
informazioni riportate siano accurate, affidabili o corrette e che qualsiasi
problema o errore sarà corretto. A/I non può assicurare che i Servizi saranno
disponibili in qualsiasi specifico momento o locazione geografica.

Come utente accetti che noi non abbiamo nessun controllo sul tuo uso dei
Servizi e che non garantiamo prestazioni o risultati che possano essere
ottenuti attraverso il tuo uso di tali Servizi, né garantiamo o sottoscriviamo
che il tuo uso dei Servizi non violerà i diritti di alcuna terza parte.

Tu accetti che il tuo uso dei Servizi è a tuo solo ed esclusivo rischio.

### Legge vigente

Questi Termini e il tuo uso dei Servizi di A/I sono governati a tutti gli
effetti dalle leggi e dai regolamenti italiani, senza prendere in
considerazione possibili conflitti con altre prescrizioni di legge ad eccezione
della misura in cui è previsto dalla legge vigente. Qualsiasi diritto, obbligo
e relazione tra le parti in questi Termini sarà interpretato e determinato in
accordo alla legge italiana. In quanto utente ti sottoponi irrevocabilmente
all'esclusiva giurisdizione e sede delle corti dello Stato Italiano in
qualsiasi controversia che dovesse sorgere o in qualsiasi modo relazionata a
questi Termini o ai Servizi di A/I. Concordi anche che qualsiasi reclamo o
controversia è governato dalla [versione in lingua Inglese di questi
Termini](/who/policy.en)

### Modifiche a questi Termini di Servizio

Ci riserviamo il diritto di cambiare, modificare o aggiornare questi Termini a
nostra sola discrezione in qualsiasi momento, per qualsiasi ragione o senza
nessuna ragione, senza responsabilità da parte nostra. Se apporteremo
cambiamenti di rilievo a questi Termini, avvertiremo i nostri utenti in maniera
chiara ed evidente. Cambiamenti minori possono essere evidenziati con una nota
alla fine del nostro sito.

Questi Termini costituiscono per intero l'accordo tra te e A/I riguardo all'uso
dei Servizi, sostituendo qualsiasi accordo precedente. In quanto utente sei
responsabile di rivedere regolarmente questi Termini di Servizio, e di
conseguenza continuando a utilizzare i Servizi accetterai automaticamente tali
Termini. Se non accetti i cambiamenti, avrai come unico ed esclusivo rimedio
l'interruzione dell'uso dei Servizi.

### Miscellanea

In questo accordo, ogni sezione include dei titoli. Questi titoli non sono
legalmente vincolanti.

Se vieni a conoscenza o sospetti l'esistenza di una falla nella sicurezza dei
Servizi, ti invitiamo a rendercelo noto il prima possibile. Se vieni a
conoscenza di un uso o accesso non autorizzato ai Servizi attraverso il tuo
account, compreso qualsiasi uso non autorizzato della tua password o del tuo
account, sei tenuto a notificarcelo immediatamente.

A/I [offre supporto](https://helpdesk.autistici.org/) esclusivamente via email, o per mezzo di altre forme di
comunicazione tramite i propri servizi interni. Non offriamo supporto
telefonico.

Comunicazioni che avvengono via email non costituiscono avviso legale per A/I
né per nessuno dei suoi volontari e delle sue volontarie in qualsiasi
situazione in cui la notifica sia necessaria per rispetto di qualsivoglia
contratto, legge o regolamento. Ogni avviso legale ad A/I deve essere messo per
iscritto e fornito in una lingua a noi comprensibile.

Per ulteriori chiarimenti e per approfondire i nostri princìpi e la nostra
missione, ti invitiamo a leggere il nostro [Manifesto](manifesto).

(Versione originale: [EN](/who/policy.en) / Tradotto in: IT)
