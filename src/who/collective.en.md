title: A short history of the A/I Collective
----

A short history of the A/I Collective
=====================================

The A/I Collective (meaning [autistici/inventati](/who/manifesto)) was born in March 2001 when individuals and
collectives dealing in technology, privacy, cyber rights and political activism met in Italy. Our fundamental aim is to provide free
communication tools on a wide basis, while pushing people to choose free rather than commercial communication modes.

Nowadays, more than twenty years after our first server was activated, A/I hosts nearly 12,000 mailboxes, over 1000 websites, more than 3000
blogs and around 3000 mailing lists. Apart from these services, we offer a video conferencing and a streaming service.
At the moment, the community surfing in the obscure sea of the A/I services amounts to several thousands of people.

It's not that many, especially if you consider the grim global scenary we are struggling against, but it's enough to attract unwanted
attention, censorship and overt repression: as early as 2003 we received the official request of handing over some mail users' personal
data, but as everybody knows, we follow a determinate policy of not keeping any logs or any information about the identity of people using
our services. So the inquirers could gain nothing from us, and it's been like that ever since our birth. In 2004 one of the websites we
hosted (Zenmai)\[[1](#1)\] was shut since Trenitalia (the Italian railways company) thought the joke our user was making about their support
for army vehicle transportation was not really funny, but after legal mishappenings we managed to win the case. \[Now that website is again
online and Google has a new story to tell about Trenitalia.\]

That same year, finally, the police seized and copied the content of all disks in our server with the help of the provider which hosted it
(Aruba) and without us knowing anything about it (we discovered it by chance one year later\[[2](#2)\] while reviewing the acts of the case
where we were asked to shut down a single mailbox \[[3](#3)\]). We were thus forced to find a strategy allowing us to make a significant
step forward in our defence of people's privacy and rights. That's why we have developed the [R\* Plan](/who/rplan/index "The R* Plan")
and spent a significant amount of time [traveling around Italy](http://www.autistici.org/ai/kaostour/) and Europe to explain what
had happened and what would happen next.

After 2005 we have been constantly pestered by prosecutors and security forces (and even by the Vatican! \[[4](#4)\]) asking us to hand over
users' data and identities and we are proud to say we were always able to answer: we are sorry, but we do not have them. Recently (2010)
some very smart policeman managed to convince a judge to order the full seizing of three servers in three different countries to find out if
we REALLY did not have any data about a user's activity on our servers \[[5](#5)\]. After spending a lot of public money (for a couple of
graffiti on a wall), the judge ended up with a lot of encrypted files with no useful information inside, and maybe he'll think twice about
giving out other investigations to the cunning policeman.

Right now, we go on surfing the dark ocean of the Internet with the very same ideas we began with: everyone should be able to use freely and
without fear the communication tools offered by the digital world, exploting them to struggle against injustice and for people's rights and
dignity. And someone has to be able to provide people struggling with appropriate tools. Knowledge and power should be shared. Control and
repression should be fought against. We have been nought, we shall be all.

- - -

**Note**
  
* <a name="1"></a>\[1\] See the Trenitalia vs. zenmai23 and A/I case: <http://www.autistici.org/ai/trenitalia/index.en.php>
* <a name="2"></a>\[2\] See the A/I Crackdown case: <http://www.autistici.org/ai/crackdown/#en>
* <a name="3"></a>\[3\] See the case about Crocenera Anarchica: <http://www.autistici.org/ai/crocenera/index.en.php>
* <a name="4"></a>\[4\] See the Pretofilia case: <http://cavallette.noblogs.org/2007/07/611>
* <a name="5"></a>\[5\] See the Norwegian Crackdown case: [special website](http://www.autistici.org/ai/crackdown-2010/#english-crackdown) - <http://cavallette.noblogs.org/2010/11/7029>

10 years nerdcore: a book on A/I Collective through the words of the A/I Collective
===================================================================================

[Download the book here](/who/book) 
