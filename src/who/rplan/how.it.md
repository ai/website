title: Il Piano R* - Come funziona
----

Il Piano R\*: un network di comunicazione resistente
=============================================================================

Implementazione tecnica
-----------------------

Documentazione Tecnica 2019: [Orange Book2](http://www.autistici.org/orangebook2/ "A/I Orange Book2") 2022

Prima implementazione del 2005: [Orange Book](http://www.autistici.org/orangebook/ "A/I Orange Book2")

[Questa pagina del sito](https://git.autistici.org/ai/website/-/blob/7f80c66fbd2c6b4fef510e5fa843a4a410bca57d/src/who/rplan/how.it.md) nella prima versione del Piano R*

Questa sezione avrebbe la pretesa di spiegare a chi volesse conoscerlo il meccanismo con il quale abbiamo realizzato la rete di server di
autistici.org/inventati.org (progetto meglio noto con il nome di "Piano R\*" :)

Il tentativo è di rendere fruibile questo passaggio a chi si diletta di tecnicaglie senza necessariamente essere un esperto.

**Layer zero : l'hardware**

Il piano R* prevede la dislocazione di ’n’ server in ’n’ luoghi. L’assunto di base è che non esiste nessun modo per impedire con certezza un accesso fisico non desiderato alla macchina. Se la cifratura da garanzie sulla confidenzialità dei dati all'interno non si può dire altrettando riguardo la reperibilità della macchina in caso di prelievo fisico o distruzione.

Quindi resta inteso che questo non ci mette al riparo da eventuali intromissione remote, dalle quali evidentemente non si e' mai protetti
abbastanza.

**Layer uno : la rete**

I server sono collegati tra loro attraverso una VPN realizzata mediante il software tinc. Tutte le comunicazioni tra i server, dalla sincronizzazione all’indirizzamento della posta passano, crittate, attraverso la VPN.
Abbiamo scelto di non implementare un layer di coordinamento globale, dividendo invece i servizi in componenti stateless (replicabili identicamente per ottenere high availability) e stateful (dove invece adottiamo il partizionamento come strategia di limitazione dei problemi), combinato con algoritmi di load balancing semplici e fondamentalmente client-side (round-robin DNS)
La struttura multi-livello discende direttamente dalla necessità di separare il “piano legale” (la superficie dell’architettura che è pubblicamente visibile e dunque soggetta a processi di legal discovery) dai dati delle utenti. Tale separazione è un aspetto che non desideriamo abbandonare, abbiamo invece deprecato nella nuova implementazione la divisione in ring differenti.

**Layer due : la sincronizzazione dei servizi**

Uno degli obiettivi fondamentali del Piano R* è quello di garantire che, nel caso in cui si venga costretti a mettere off-line uno o più nodi (perché, per esempio, un nodo è risultato compromesso), i servizi offerti non vengano interrotti.
Per consentire questo è stato necessario strutturare un meccanismo di sincronizzazione dei materiali e di facile redirezione di tutte le richieste fatte al server eventualmente compromesso verso un nuovo server.
Per sincronizzare le configurazioni dei vari servizi, il Piano R* prevede l’uso di diversi meccanismi:
LDAP per gli attributi /servizi/account degli utenti, database unico ma replicato delle utenze.
Per i dettagli consultare appunto l'Orangebook2 nel capitolo: "Macchine a stati e riconciliazione"
Noblogs segue la stessa logica di risorse allocate in macchine diverse e spostabili solo all'occorrenza, ma è totalmente indipendente da LDAP.

**Layer tre : i contenuti degli utenti**

I dati che è necessario sincronizzare su più nodi della rete (porzioni condivise di contenuti dei servizi, dati di alcuni utenti, chiavi e certificati, ecc.) vengono trasferiti via rsync, come pagine html presenti in più copie, i servizi di backup e altre cosine.
Ogni casella di posta è fisicamente localizzata su un server, scelto in modo da bilanciare il carico della rete. È possibile, in qualsiasi momento, spostare una determinata casella di posta da un server a un altro, modificando un parametro di LDAP. 
Questi spostamenti risultano completamente trasparenti/invisibili all’utente. 
Così come le caselle di posta, i siti web sono fisicamente disponibili su uno dei webserver del network, con possiblità di essere spostati in breve tempo recuperando i dati dalle copie di backup, presenti su altre macchine della rete. 
Usiamo a questo proposito uno strumento fattoapposta: Tabacco: https://git.autistici.org/ai3/tools/tabacco

**Layer quattro : gli utenti**

Gli utenti sono contenuti in un solo database LDAP (un database pensato per rendere al massimo in situazioni in cui è necessario leggere molte volte dal database e scrivervi raramente).
Nel database LDAP sono contenute tutte le informazioni degli utenti, nonché le informazioni relative ai vari servizi collegate a ogni utente (dove risiede la sua casella di posta o il suo sito, la sua password, ecc. ecc.).

**Layer cinque : i servizi**

Per comprendere la parte tecnica di realizzazione del piano R* vi manca solo un’idea di come i vari servizi siano organizzati tra i vari nodi.
Il server web è configurato per rispondere sui nodi di frontend. I siti degli utenti e la posta sono divisi sui vari nodi e la redirezione viene effettuata automaticamente dal server di frontend una volta che si cerca di raggiungere la risorsa. 
Ovvero: il dominio www.autistici.org risolve in round robin su tutti i nodi della rete di frontend, e ognuno dei nodi redirige la richiesta sul nodo che effettivamente ospita il sito.
In sostanza, ogni singolo nodo della rete serve parte dei siti o parte delle mail. In un qualsiasi momento se uno dei nodi viene manomesso, tutte le sue configurazioni e la parte di siti e caselle che gestiva vengono trasferite a un altro nodo della rete, impedendo un breakdown della comunicazione.
I servizi ora sono su macchine diverse e sono isolati orizzontalmente con container. Inoltre ogni volta che li aggiorniamo vengono distrutti e ricreati, per avere una "sanificazione" automatica.
Le configurazioni dei servizi sono versionate in un unico luogo, ma replicate e la loro messa in atto viene orchestrata da un piccolo, ma funzionale software scritto da noi: [FLOAT](https://git.autistici.org/ai3/float)
Per fare ciò la nostra infrastruttura si è arricchita di un Gitlab dedicato ed anche di una macchina apposita per buildare i container.

