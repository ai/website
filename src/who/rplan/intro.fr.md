title: Le Plan R* - Introduction
----

Le Plan R\* : un réseau de communication résistante
============================================================================

L'autre côté de la spirale
--------------------------

De nouvelles formes d'expression et d'organisation émergent, de manière désordonnée, en créant des relations, des façons de produire et des
moyens d’imaginer. Jour après jour, sans plan préétabli, mais avec beaucoup d'idées en commun, nous voyons de nouveaux modes de vie
apparaître dans ce monde en constante mutation -- modes de vie que les multinationales et les fondations de milliardaires tentent
d'exploiter à leur avantage et à celui de leurs actionnaires.

Des tentatives maladroites pour combler le «fossé réglementaire dangereux» des lois inspirées par le choc des civilisations, par le
terrorisme international, par la nécessité de tout savoir sur tout le monde, par la vitesse du vent et les pâquerettes sauvages, ont créé un
contexte dans lequel les règles et leur application sont d’un « certain point de vue », adéquates pour toute situation, pour toute et chaque
partie du jeu et pour les tabloïdes les plus populaires.

Incapable de séparer le bon du mauvais, les institutions tentent de fermer la barrière, procédant d'une manière confuse, contradictoire et
hâtive. Elles s’évertuent à faire apparaître une culture terroriste à l’esprit fermé, et à construire les dangers nécessaires à la
prévention d’une utilisation «abusive» du Web, tentant ainsi de créer le Centre Commercial Mondial, un endroit sûr (sécuritaire) et bien
éclairé.

Nous longeons une voie qui est celle de la répression, une répression pas si molle envers quiconque contesterait cette mentalité pas si
nouvelle, et son parrainage rusé.

Tout cela, et bien plus encore, a conduit au cours des dernières années, pas seulement dans les Etats que l’on appelle voyous, mais aussi
dans les pays démocratiques auto-proclamés, à une violation plus ou moins évidente de ce que l'on appelle généralement les droits civils et
la liberté d'expression : la mise sur écoute automatique de tout canal de communication, une tendance croissante à la saisie de sites web et
de boîtes mail, des tentatives plus ou moins fructueuses pour censurer une information embarrassante et indépendante, et la criminalisation
de l'échange et du partage.

Depuis son image originelle de monde des merveilles, Internet se transforme, selon les besoins actuels, en un enfer de pédophiles, de
tricheurs et de terroristes.

Involontairement jetés dans ce paysage, et se basant sur notre expérience, nous avons essayé d'organiser nos services d’une façon
différente, réaffirmant ainsi notre volonté à R\*ésister.

Nous avons donc conçu le Plan R\* \[[#1](#1)\] : si vous souhaitez en savoir plus à ce sujet, il vous suffit de
naviguer dans cette section du site.

<a name="1"></a>**\[1\]** Pour développer notre plan, nous avons lu de nombreux livres d’histoire, pour s’imprégner de l'histoire de vainqueurs et trouver
la meilleure stratégie pour un grand bond en avant. Après avoir lu les classiques, d'Adam Smith à Kipling, nous sommes tombés sur un texte
en italien, un plan écrit pas plus tard qu’en 1982 par un ancien Grand Maître maçonnique et visant à subordonner l'Italie à des
multinationales, et de transformer les citoyens en une main-d’œuvre amorphe et impuissante.
 Cette tentative a été appelé Plan R, et depuis lors, tout le monde nous a affirmé qu'elle a été contrecarrée. Mais toutes les preuves
autour de nous montrent que ce n'est pas le cas, et comme il a même si bien réussi, encore après 30 ans, nous avons décidé de nous en
inspirer, en l'appelant Plan R\*.
