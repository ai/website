title: Breve storia del collettivo A/I
----

Breve storia del collettivo A/I 
===============================

Il Collettivo A/I (che sta per [autistici.org / inventati.org](/who/manifesto)) nasce nel marzo 2001 dall'incontro di individualità
e collettivi che si occupano di tecnologie, privacy, diritti digitali e attivismo politico. L'idea di base è quella di fornire strumenti di
comunicazione liberi e gratuiti su vasta scala, spingendo le persone a scegliere modalità comunicative libere anziché commerciali.

Oggi, a più di vent'anni dall'attivazione del primo server A/I, le caselle di posta ospitate sono quasi 12000, i siti più di 1000, i blog
più di 3000 e le mailing circa 3000. A questi servizi vanno aggiunti un servizio di video conferenza, e uno di streaming. 
Inoltre, A/I gira per l'Italia e per l'Europa per cercare di spiegare e diffondere le sue idee sulla comunicazione digitale. 
Al momento la comunità che naviga nelle acque scure dei servizi di A/I conta decine di migliaia di persone.

Non sono molte, considerato soprattutto il cupo scenario globale su cui ci affacciamo, ma abbastanza perché incappassimo in casi di censura
o di aperta repressione: già nel 2003 abbiamo ricevuto richieste ufficiali di dati anagrafici di utenti di determinate caselle di posta, ma
noi quei dati, che neanche molti server commerciali conservano, non li abbiamo mai tenuti, per nostra scelta politica consapevole. Per
questo gli inquirenti non hanno potuto ottenere niente: è sempre andata così fin dalla nostra nascita. Nel 2004 abbiamo poi subito un
sequestro di un sito di satira (Zenmai)\[[1](#1)\] causato dall'ipersensibilità di Trenitalia sul tema delle guerra (vicenda risolta
decisamente a nostro favore). \[ Ora Il sito è nuovamente online e Google ha una nuova storia da raccontare su Trenitalia.\]

Nello stesso anno, infine, la polizia ha sequestrato e copiato tutto il contenuto dei dischi del server con la collaborazione del provider
presso il quale risiedeva (Aruba), completamente a nostra insaputa (noi siamo venuti a saperlo solo a un anno di distanza, e solo per
caso\[[2](#2)\], mentre esaminavamo gli atti del caso in cui ci avevano chiesto di chiudere una singola casella di posta\[[3](#3)\]). Di
fronte a tutto questo, era necessario inventarsi una strategia che costituisse un valido passo avanti. Per questo abbiamo dato vita al
[Piano R\*](/who/rplan/ "The R* Plan") e abbiamo [viaggiato](http://www.autistici.org/ai/kaostour/) per molto tempo tra
l'Italia e l'Europa per spiegare che cosa era successo e come sarebbe andata in seguito.

Dopo il 2005 abbiamo continuato a essere tormentati da pubblici ministeri e forze dell'ordine (e persino dal Vaticano!\[[4](#4)\]), che ci
chiedevano di consegnargli dati anagrafici di utenti, e siamo fieri di aver sempre potuto dire: ci spiace, ma non li abbiamo. Di recente
(nel 2010) dei brillanti poliziotti sono riusciti a convincere un giudice a sequestrare tre server in tre paesi diversi per sapere se
*davvero* non avevamo dati sull'attività online di un nostro utente\[[5](#5)\]. Dopo aver speso un bel po' di soldi pubblici (per
un'indagine su un paio di scritte su un muro), il giudice ha ottenuto un sacco di file crittati senza alcuna informazione utile, e magari
ora ci penserà due volte prima di autorizzare altre indagini per quegli astuti agenti.

Oggi continuiamo a navigare l'oscuro oceano di Internet con le stesse idee di partenza: tutti dovrebbero poter usare liberamente e senza
paura gli strumenti di comunicazione offerti dal mondo digitale, sfruttandoli per lottare contro le ingiustizie e per i diritti e la dignità
di ognuno. E qualcuno deve essere in grado di fornire gli strumenti appropriati a chi lotta. I sapere e il potere vanno condivisi. Il
controllo e la repressione contrastati. Noi saremo tutto.

*Abbiamo raccontato la nostra storia in un libro. Scaricalo [qui](/who/book) o scrivi a info (at) autistici . org per chiederne una copia

- - -
  
**Note**

* <a name="1"></a>\[1\] V. il caso Trenitalia vs. zenmai23 e A/I: <http://www.autistici.org/ai/trenitalia/index.php>
* <a name="2"></a>\[2\] V. crackdown A/I: <http://www.autistici.org/ai/crackdown>
* <a name="3"></a>\[3\] V. il caso Crocenera Anarchica: <http://www.autistici.org/ai/crocenera/index.php>
* <a name="4"></a>\[4\] V. il caso Pretofilia: <http://cavallette.noblogs.org/2007/07/611>
* <a name="5"></a>\[5\] V. Norwegian Crackdown: [sito speciale](http://www.autistici.org/ai/crackdown-2010) - <http://cavallette.noblogs.org/2010/11/7029>
