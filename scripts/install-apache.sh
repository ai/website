#!/bin/sh
#
# Installs apache2 on a temporary chroot on a Debian system.
#
# Used in combination with run-test-server.sh to avoid
# polluting the local system installation with server-side
# packages, which is annoying.
#

# Arch linux is missing /bin, apt-get will fail badly.
PATH=/bin:/usr/bin:/sbin:/usr/sbin
export PATH

chroot_dir=./build/apache

if [ ! -x /usr/sbin/debootstrap ]; then
    echo "ERROR: the debootstrap package is not installed" >&2
    exit 1
fi
if [ `whoami` != root ]; then
    echo "ERROR: you must run this script with 'sudo'" >&2
    exit 1
fi

set -e

# Randomly checks a chrooted binary to see if we need
# to run debootstrap or not.
mkdir -p ./build
if [ ! -e ${chroot_dir}/bin/ls ]; then
    debootstrap jessie ${chroot_dir} http://httpredir.debian.org/debian
fi

if [ ! -e ${chroot_dir}/usr/sbin/policy-rc.d ]; then
    cat > ${chroot_dir}/usr/sbin/policy-rc.d <<EOF
#!/bin/sh
exit 101
EOF
    chmod a+x ${chroot_dir}/usr/sbin/policy-rc.d
fi

chroot ${chroot_dir} \
  env DEBIAN_FRONTEND=noninteractive LANG= LANGUAGE= \
  apt-get install -y --force-yes apache2-mpm-worker

# Current user must be resolved by NSS.
cp /etc/passwd ${chroot_dir}/etc/passwd

# Ensure the 'test' dir is writable by the user.
mkdir -p ${chroot_dir}/test
chmod 777 ${chroot_dir}/test

exit 0
