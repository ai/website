#!/bin/sh -x

root="$1"

find_all_files() {
    find ${root} -type f -regextype egrep -regex '.*\.(css|js|svg|html)' -print
}

find_all_files | xargs brotli -9k
find_all_files | xargs gzip -9k

# Letting Apache handle encodings via content negotiation
# requires us to use double-extensions for the original files.
# See:
# https://kevinlocke.name/bits/2016/01/20/serving-pre-compressed-files-with-apache-multiviews/

find_all_files | (while read f ; do
         dst=$(echo $f | sed -e 's/\(\.[a-z]*\)$/\1\1/')
         mv "$f" "$dst"
       done)

