#!/bin/sh

if ! curl -sf -o /dev/null http://localhost:8080 ; then
    echo "Could not connect to Apache (8080)"
    exit 1
fi
echo "Successfully connected to http://localhost:8080"

exit 0
